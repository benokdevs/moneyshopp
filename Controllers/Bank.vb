﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class Bank
    Inherits ControllerBase
    <HttpPost>
    <Route("/admin/addbank")>
    Public Function AddBank(<FromBody()> ByVal values As Banks) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim Dr As DataRow
            Dim sSQL As String
            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.BankName.Length = 0 Then
                        Message.Message = "Bank Name Required"
                        Message.Payload = "No Bank Name Entered"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Generate the FI Code ie the ID Number
                    values.BankNo = GenerateBankID()

                    If values.BankNo = "" Then
                        Message.Message = "Internal Error"
                        Message.Payload = "Unable to Generate Bank ID"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    ' Check for Duplicate Entry

                    sSQL = "SELECT COUNT(BankNo)AS NAME from Banks WHERE BankNo = '" + values.BankNo + "'"
                    Dr = DBConnection.GetRowInfo(sSQL)
                    If Dr Is Nothing Then
                        Message.Message = "Internal Error"
                        Message.Payload = "Cannot Get Bank Count"
                        Return BadRequest(ReturnJSONResults(Message))
                    Else

                        Dim fint As Integer
                        fint = CInt(Dr("Name").ToString)

                        If fint > 0 Then

                            Message.Message = "Bank Already Exist"
                            Message.Payload = "Bank Already Exist"

                            Return BadRequest(ReturnJSONResults(Message))
                        Else
                            ' get the username from  the token
                            Dim username As String = GetJWTUsername(myToken)
                            If username = "" Or username Is Nothing Then
                                Message.Message = "Unable to Retrieve Username from token"
                                Message.Payload = "Unable to Retrieve Username from token"
                                Return BadRequest(ReturnJSONResults(Message))
                            End If

                            ' Go ahead to save Record

                            Message = DBConnection.SaveBank(values, username)
                            If Message.Message = "" Or Message.Message Is Nothing Then

                                Message.Message = "Unable to Save Records"
                                Message.Payload = "Internal Error"

                                Return BadRequest(ReturnJSONResults(Message))

                            Else


                                Dim rval As String
                                rval = ReturnJSONResults(Message)

                                Return Ok(rval)
                            End If

                        End If

                    End If



                End If
            Catch ex As Exception

            End Try



        End If

    End Function
    <HttpPost>
    <Route("/admin/updatebank")>
    Public Function UpdateBank(<FromBody()> ByVal values As Banks) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.BankNo.Length = 0 Then
                        Message.Message = "Bank Number Required"
                        Message.Payload = "Bank Number Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Update Record
                    Message = DBConnection.UpdateBank(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Updaate Records"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/deletebank")>
    Public Function DeleteBank(<FromBody()> ByVal values As Banks) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.BankNo.Length = 0 Then
                        Message.Message = "Company Number Required"
                        Message.Payload = "Company Number Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Delete Record
                    Message = DBConnection.DeleteBank(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Delete Records"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpGet>
    <Route("/admin/banks")>
    Public Function GetBanks(<FromQuery()> ByVal values As BankFilter) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds



            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            Else

                Try
                    'validate token





                    Dim sSQL As String
                    Dim Dt As DataTable

                    sSQL = "Select *, count(*) OVER() AS full_count From Banks"


                    Dim hasmore As Boolean = False

                    '                 Public Property BankNo As String
                    'Public Property BankName As String
                    'Public Property AccountNo As String
                    'Public Property AccountName As String
                    'Public Property Status As Boolean

                    If values.BankNo IsNot Nothing Then
                        If values.BankNo.Length > 0 Then
                            ' '%\_%'
                            sSQL = sSQL & " Where BankNo ilike '%" & values.BankNo & "%' "
                            hasmore = True
                        End If

                    End If

                    If values.BankName IsNot Nothing Then
                        If values.BankName.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "OR BankName ilike '%" & values.BankName & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " Where BankName ilike '%" & values.BankName & "%' "
                                hasmore = True
                            End If

                        End If
                    End If


                    If values.AccountNo IsNot Nothing Then
                        If values.AccountNo.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "Or AccountNo ilike '%" & values.AccountNo & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " Where AccountNo ilike '%" & values.AccountNo & "%' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.AccountName IsNot Nothing Then
                        If values.AccountName.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "Or AccountName ilike '%" & values.AccountName & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " Where AccountName ilike '%" & values.AccountName & "%' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.Status IsNot Nothing Then
                        If values.Status.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "or  Status = '" & values.Status & "' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " Where Status = '" & values.Status & "' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.pagination = True Then

                        Dim pagenum As Integer
                        pagenum = values.pagesize * (values.pagenumber - 1)
                        sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    End If


                    '  sSQL = sSQL & " ORDER BY BankNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dt = DBConnection.GetTableData(sSQL)


                    If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                        Message.Message = "No records Found "
                        Message.Payload = "No Records Found "
                        Return BadRequest(ReturnJSONResults(Message))

                    Else

                        Dim glob As New Chilkat.Global
                        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                        'Log("Chilkat License Status : " & succes)


                        Message.Payload = JsonForDatatable(Dt)
                        'Message.Payload = Message.Payload.Replace("""[", "[")
                        'Message.Payload = Message.Payload.Replace("]""", "]")
                        Message.Message = "Bank Details"
                        Dim jsonconvert As New JsonResult(Message)
                        ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If
                Catch ex As Exception

                End Try


                ' Return jsonconvert
            End If

        End If

    End Function



End Class
