﻿Public Class FIModel

    Public Property CompanyNo As String
    Public Property CompanyName As String
    Public Property Location As String
    Public Property Telephone As String
    Public Property PhoneNo As String
    Public Property Address As String
    Public Property LoanLimit As Double
    Public Property DateRegistered As DateTime
    Public Property RegisteredBy As String
    Public Property Status As Boolean
    Public Property Token As String
    Public Property Emailaddress As String
End Class
