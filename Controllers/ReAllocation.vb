﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO

Public Class ReAllocation
    Inherits ControllerBase

    <HttpPost>
    <Route("/admin/loans/reallocation")>
    Public Function Reallocation(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'process body json

                    ' Go ahead to Update Record

                    Message = ProcessReAllocation(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function
    Public Function ProcessReAllocation(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token


            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessReAllocation : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the institution_id 

            Dim FID As String = json.StringOf("institution_id")


            Dim instArray As Chilkat.JsonArray = json.ArrayOf("applicants_id")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessReAllocation : " & "No applicants Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message


            End If

            Dim arraySize As Integer = instArray.Size
            Dim i As Integer
            Dim ssql As String
            For i = 0 To arraySize - 1

                ' Dim sValue As String = instArray.StringAt(i)

                '  Debug.WriteLine("[" & i & "] = " & sValue)

                Dim iValue As Integer = instArray.IntAt(i)

                ssql = "update SalaryLoan set Status='3000', FI=@FI, DateProcessed = @DateProcessed WHERE ID= " & iValue

                Message.mError = Not DBConnection.LoanAllocations(ssql, username, iValue.ToString, FID)

                ' Debug.WriteLine("[" & i & "] as integer = " & iValue)

            Next


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessReAllocation : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function


    <HttpGet>
    <Route("/admin/loans/reallocations")>
    Public Function GetReallocation(<FromQuery()> ByVal qval As InstLoanApprFilters) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)

                'Get the SQL parameter values

                Dim pTime As Integer
                Dim hTime As Integer


                pTime = CInt(GetConfigurator(3))
                hTime = CInt(GetConfigurator(4))
                ' sSQL = "SELECT id, StaffID, Institution, Amount, FI, Status, DateApplied , count(*) OVER() AS full_count  FROM SalaryLoan where Status =  '2000'"
                '  sSQL = "SELECT id, StaffID, Institution, Amount, FI, Status, DateApplied, DATEDIFF(minute, DateProcessed, getdate()) AS TimeElapsed , count(*) OVER() AS full_count FROM SalaryLoan where (Status =  '3000' OR Status='4000') AND (DATEDIFF(minute, DateProcessed, getdate()) >= @Time OR DATEDIFF(hour, DateConfirmed, getdate()) >= @HTime) "
                sSQL = "SELECT id, StaffID, Institution, Amount, FI, Status, DateApplied, (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 60) + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint AS TimeElapsed , count(*) OVER() AS full_count FROM SalaryLoan where (Status =  '3000' OR Status='4000') AND cast(((DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 60) + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)) as bigint) >= " & pTime & " OR cast((DATE_PART('day', DateConfirmed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateConfirmed::timestamp - CURRENT_DATE::timestamp)* 60) as bigint) >= " & hTime

                If qval.pagination = False Then
                    sSQL = sSQL & " ORDER BY id"
                Else
                    Dim pagenum As Integer
                    pagenum = qval.pagesize * (qval.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qval.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetReAllocationRecords(sSQL, pTime, hTime)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "ReAllocation  Records Completed"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

End Class
