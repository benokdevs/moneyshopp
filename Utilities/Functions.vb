﻿Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data
Imports System.IO
Imports System.Net
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http
Imports System.DateTime
Imports Npgsql

Public Module Functions
    Public AppPath As String = AppDomain.CurrentDomain.BaseDirectory

    Public Function GetSettings(key As String) As String
        'getting the config
        Try
            Dim rval As String
            Dim glob As New Chilkat.Global

            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)s


            'Dim status As Integer = glob.UnlockStatus


            Dim json As New Chilkat.JsonObject

            '  Load the JSON into the object.
            '  Call json.Load to load from a string rather than a file...
            'Dim p As String = Path.Join(AppPath, "config.json")
            Dim success As Boolean = json.LoadFile(AppPath & "config.json")

            ' SavetoLog("Config Path : " & AppPath & "config.json")

            '  We are assuming success..

            '  Get the easy stuff:
            'Dim Luno As String
            'Dim HostIP As String
            'Dim Hostport As Integer

            'HostIP = json.StringOf("General.host")

            'Hostport = CInt(json.StringOf("General.port"))
            'Luno = json.StringOf("General.LUNO")
            rval = json.StringOf(key)
            Return rval
        Catch ex As Exception
            SavetoLog("Error From GetSettings : " & ex.Message)
        End Try


    End Function
    Public Function FormatSento(ByVal st As String) As String
        FormatSento = ""
        Try
            ' Dim i As Integer = 0
            Dim r As Integer
            Dim t As String = ""
            Dim ap As String = "233"

            r = st.Length

            If st.Length <= 10 Then

                t = st.Substring(0, 1)
                If t = "0" Then

                    t = st.Substring(1, r - 1)
                    t = ap & t
                Else
                    t = st.Substring(0, r)
                    t = ap & t
                    'For i = 0 To r
                    '    t = t & Mid(st, 1, 1)
                    'Next
                End If
                Return t
            End If
        Catch ex As Exception

        End Try

    End Function

    Public Function GetNetwork(no As String) As String
        Try

            'Globacom(23)
            'MTN(24), (054), (055)
            'AirtelTigo(27), (057), (026), (056)
            'Expresso(28)
            'Vodafone(20), (050)

            '          "MTN",
            '"VODAFONE",
            '"TIGO",
            '"AIRTEL"

            Dim sortcode As String
            sortcode = no.Substring(0, 3)

            If sortcode = "024" Or sortcode = "054" Or sortcode = "055" Then
                Return "MTN"
            ElseIf sortcode = "027" Or sortcode = "057" Then
                Return "TIGO"
            ElseIf sortcode = "026" Or sortcode = "056" Then
                Return "AIRTEL"
            ElseIf sortcode = "020" Or sortcode = "050" Then
                Return "VODAFONE"
            End If

        Catch ex As Exception

        End Try
    End Function

    Public Function PostTransaction(msg As String, url As String) As GeneralResponds
        Try
            ' This requires the Chilkat API to have been previously unlocked.
            ' See Global Unlock Sample for sample code
            Dim mess As New GeneralResponds
            Dim req As New Chilkat.HttpRequest
            Dim http As New Chilkat.Http


            Dim success As Boolean

            ' This example duplicates the HTTP POST shown at
            ' http://json.org/JSONRequest.html

            ' Specifically, the request to be sent looks like this:

            ' POST /request HTTP/1.1
            ' Accept: application/jsonrequest
            ' Content-Encoding: identity
            ' Content-Length: 72
            ' Content-Type: application/jsonrequest
            ' Host: json.penzance.org
            ' 
            ' {"user":"doctoravatar@penzance.com","forecast":7,"t":"vlIj","zip":94089}


            ' First, remove default header fields that would be automatically
            ' sent.  (These headers are harmless, and shouldn't need to 
            ' be suppressed, but just in case...)
            http.AcceptCharset = ""
            http.UserAgent = ""
            http.AcceptLanguage = ""
            ' Suppress the Accept-Encoding header by disallowing 
            ' a gzip response:
            http.AllowGzip = False

            ' If a Cookie needs to be added, it may be added by calling
            ' AddQuickHeader:
            '  success = http.AddQuickHeader("Cookie", "JSESSIONID=1234")

            '' Dim jsonText As String = "{""user"":""doctoravatar@penzance.com"",""forecast"":7,""t"":""vlIj"",""zip"":94089}"
            Dim jsonText As String = msg

            ' To use SSL/TLS, simply use "https://" in the URL.

            ' IMPORTANT: Make sure to change the URL, JSON text,
            ' and other data items to your own values.  The URL used
            ' in this example will not actually work.

            Dim resp As Chilkat.HttpResponse
            resp = http.PostJson(url, jsonText)
            If (http.LastMethodSuccess <> True) Then
                SavetoLog(http.LastErrorText)
                mess.Message = http.LastErrorText
                mess.Payload = http.LastErrorText
                mess.mError = True
            Else
                ' Display the JSON response.
                SavetoLog(resp.BodyStr)
                Dim respcode As String
                Dim responseMessage As String

                respcode = ProcessJsonText(resp.BodyStr, "responseCode")
                responseMessage = ProcessJsonText(resp.BodyStr, "responseMessage")
                mess.Message = respcode
                mess.Payload = responseMessage
                If respcode = "01" Or respcode = "03" Then
                    mess.mError = False

                Else
                    SavetoLog("Posting of Transaction : " & msg & vbNewLine & " to : " & url & vbNewLine & "Failed with Responds : " & resp.BodyStr)
                    mess.mError = True
                End If

            End If

            Return mess
        Catch ex As Exception

        End Try
    End Function

    Public Function ProcessJsonText(jsontxt As String, key As String) As String
        'getting the config
        Try
            Dim rval As String
            Dim glob As New Chilkat.Global

            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)s


            'Dim status As Integer = glob.UnlockStatus


            Dim json As New Chilkat.JsonObject

            '  Load the JSON into the object.
            '  Call json.Load to load from a string rather than a file...
            'Dim p As String = Path.Join(AppPath, "config.json")
            Dim success As Boolean = json.Load(jsontxt)


            rval = json.StringOf(key)
            Return rval
        Catch ex As Exception
            SavetoLog("Error From ProcessJsonText : " & ex.Message)
        End Try


    End Function
    Public Function VerifyJWT(token As String) As Boolean
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        'Log("Chilkat License Status : " & succes)

        Dim jwt As New Chilkat.Jwt



        Dim bb As Boolean = jwt.VerifyJwt(token, GetSettings("General.secret"))
        Dim cc As Boolean = jwt.IsTimeValid(token, 60)

        If bb = True Then
            If cc = True Then
                Return True
            Else
                Return False
            End If

        Else
            Return False
        End If


    End Function

    Public Function GetJWTUsername(token As String) As String
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        Dim rval As String
        'Log("Chilkat License Status : " & succes)

        Dim jwt As New Chilkat.Jwt


        '  Now let's recover the original claims JSON (the payload).
        Dim payload As String = jwt.GetPayload(token)
        '  The payload will likely be in compact form:
        ' Console.WriteLine(payload)

        '  We can format for human viewing by loading it into Chilkat's JSON object
        '  and emit.
        Dim json As New Chilkat.JsonObject
        Dim success As Boolean = json.Load(payload)
        json.EmitCompact = False
        ' Console.WriteLine(json.Emit())
        rval = json.StringOf("username")

        Return rval


    End Function

    Public Function GetJWTValues(token As String) As StructJWTValues
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        Dim rval As New StructJWTValues
        'Log("Chilkat License Status : " & succes)

        Dim jwt As New Chilkat.Jwt


        '  Now let's recover the original claims JSON (the payload).
        Dim payload As String = jwt.GetPayload(token)
        '  The payload will likely be in compact form:
        ' Console.WriteLine(payload)

        '  We can format for human viewing by loading it into Chilkat's JSON object
        '  and emit.
        Dim json As New Chilkat.JsonObject
        Dim success As Boolean = json.Load(payload)
        json.EmitCompact = False
        ' Console.WriteLine(json.Emit())
        rval.username = json.StringOf("username")
        rval.status = json.StringOf("status")
        rval.appversion = json.StringOf("appversion")

        Return rval


    End Function

    Public Function GenerateJWT(uname As String) As String
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        'Log("Chilkat License Status : " & succes)

        Dim jwt As New Chilkat.Jwt

        '  Build the JOSE header
        Dim jose As New Chilkat.JsonObject
        '  Use HS256.  Pass the string "HS384" or "HS512" to use a different algorithm.
        Dim success As Boolean = jose.AppendString("alg", "HS256")
        success = jose.AppendString("typ", "JWT")

        '  Now build the JWT claims (also known as the payload)
        Dim claims As New Chilkat.JsonObject
        success = claims.AppendString("iss", "http://moneyshopp.com")
        success = claims.AppendString("username", uname)
        success = claims.AppendString("aud", "http://moneyshopp.com")

        '  Set the timestamp of when the JWT was created to now.
        Dim curDateTime As Integer = jwt.GenNumericDate(0)
        success = claims.AddIntAt(-1, "iat", curDateTime)

        '  Set the "not process before" timestamp to now.
        success = claims.AddIntAt(-1, "nbf", curDateTime)

        '  Set the timestamp defining an expiration time (end time) for the token
        '  to be now + 1 hour (3600 seconds)

        '--------FOR PRODUCTION
        '  success = claims.AddIntAt(-1, "exp", curDateTime + (3600))
        '--------

        '---FOR DEV MODE
        success = claims.AddIntAt(-1, "exp", curDateTime + (3600 * 24 * 365))
        '------
        '  Produce the smallest possible JWT:
        jwt.AutoCompact = True

        Dim strJwt As String = jwt.CreateJwt(jose.Emit(), claims.Emit(), GetSettings("General.secret"))
        ' Dim bb As Boolean = jwt.VerifyJwt("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwOi8vd2VrZmxvdy5jb20iLCJ1c2VybmFtZSI6ImFkbWluIiwiYXVkIjoiaHR0cDovL2V4YW1wbGUuY29tIiwiaWF0IjoxNTY2NDkzNjc5LCJuYmYiOjE1NjY0OTM2NzksImV4cCI6MTU2NjQ5NzI3OX0.xR250JwWlR5N_G5YeFKq1n4KkK7y_SHJkI1ERNA-yx0", "24A539C69B76B84B8AE289CDD08F783E4B52EF9ADA68A8809304B60D0735E30C0F2F67E68BF2D6FD0EE70D29DBA97487C7A04DD1C9E682FF78801029DF8828FB")

        Return strJwt

    End Function
    Public Function GenerateMobileJWT(uname As String) As String
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        'Log("Chilkat License Status : " & succes)

        Dim jwt As New Chilkat.Jwt

        '  Build the JOSE header
        Dim jose As New Chilkat.JsonObject
        '  Use HS256.  Pass the string "HS384" or "HS512" to use a different algorithm.
        Dim success As Boolean = jose.AppendString("alg", "HS256")
        success = jose.AppendString("typ", "JWT")

        '  Now build the JWT claims (also known as the payload)
        Dim claims As New Chilkat.JsonObject
        success = claims.AppendString("iss", "http://moneyshopp.com")
        success = claims.AppendString("username", uname)
        success = claims.AppendString("status", "0")
        succes = claims.AppendString("appversion", GetAppVersion)
        success = claims.AppendString("aud", "http://moneyshopp.com")

        '  Set the timestamp of when the JWT was created to now.
        Dim curDateTime As Integer = jwt.GenNumericDate(0)
        ' currentTime.AddMinutes(GetTokenDuration())
        success = claims.AddIntAt(-1, "iat", curDateTime)

        '  Set the "not process before" timestamp to now.
        success = claims.AddIntAt(-1, "nbf", curDateTime)

        '  Set the timestamp defining an expiration time (end time) for the token
        '  to be now + 1 hour (3600 seconds)

        '--------FOR PRODUCTION
        '  success = claims.AddIntAt(-1, "exp", curDateTime + (3600))
        '--------

        '---FOR DEV MODE
        success = claims.AddIntAt(-1, "exp", curDateTime + (60 * Convert.ToInt32(GetTokenDuration)))
        '------
        '  Produce the smallest possible JWT:
        jwt.AutoCompact = True

        Dim strJwt As String = jwt.CreateJwt(jose.Emit(), claims.Emit(), GetSettings("General.secret"))
        ' Dim bb As Boolean = jwt.VerifyJwt("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwOi8vd2VrZmxvdy5jb20iLCJ1c2VybmFtZSI6ImFkbWluIiwiYXVkIjoiaHR0cDovL2V4YW1wbGUuY29tIiwiaWF0IjoxNTY2NDkzNjc5LCJuYmYiOjE1NjY0OTM2NzksImV4cCI6MTU2NjQ5NzI3OX0.xR250JwWlR5N_G5YeFKq1n4KkK7y_SHJkI1ERNA-yx0", "24A539C69B76B84B8AE289CDD08F783E4B52EF9ADA68A8809304B60D0735E30C0F2F67E68BF2D6FD0EE70D29DBA97487C7A04DD1C9E682FF78801029DF8828FB")

        Return strJwt

    End Function
    Public Function BuildReturnErrorMessage(Errmsg As String) As String
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        'Log("Chilkat License Status : " & succes)


        'Dim status As Integer = glob.UnlockStatus


        Dim json As New Chilkat.JsonObject

        json.AddStringAt(-1, "ErrorMessage", Errmsg)

        Return json.Emit
    End Function

    Public Function ReturnErrorMessage(Errmsg As String) As GeneralResponds
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
        'Log("Chilkat License Status : " & succes)


        Dim Message As New GeneralResponds


        Dim json As New Chilkat.JsonObject

        json.AddStringAt(-1, "Payload", Errmsg)


        Message.Message = Errmsg
        Message.Payload = json.Emit()

        Return Message
    End Function

    Public Sub SavetoLog2(txt As String)


        Try


            Dim sw As StreamWriter
            ' // s = s.Substring(s.LastIndexOf("\\", s.Length - 2) + 1)
            '//string logfile = (Environment.CommandLine.Substring(0, (String.LastIndexOf("\\") - 1)) + "\\log.txt").Replace('\"', "");

            '  Dim logfile As String = Environment.CurrentDirectory & "\Log.txt"
            Dim logfile As String = AppPath & GETLogfilename() & ".txt"
            ' //string logfile = System.AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            If Not File.Exists(logfile) Then
                File.Create(logfile)


                sw = File.CreateText(logfile)

            Else

                sw = File.AppendText(logfile)
            End If

            sw.WriteLine((DateTime.Now.ToShortDateString() + (" " + (DateTime.Now.ToShortTimeString() + (": " + txt)))))
            sw.Flush()
            sw.Close()

            GC.Collect()
        Catch ex As Exception

        End Try



    End Sub

    Public Function SavetoLog(ByVal strData As String, Optional ByVal ErrInfo As String = "") As Boolean

        Dim Contents As String = ""
        Dim bAns As Boolean = False
        Dim FullPath As String
        ' Dim logConf As New structRegistery

        'logConf = GetConfig()
        '  FullPath = System.AppDomain.CurrentDomain.BaseDirectory & "\" & DateTime.Today.ToString("MMM-dd-yyyy") & ".log"
        FullPath = AppPath & DateTime.Today.ToString("MMM-dd-yyyy") & ".log"



        'Dim strFile As String = "C:\ErrorLog_" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        Dim sw As StreamWriter
        Try
            If (Not File.Exists(FullPath)) Then
                sw = File.CreateText(FullPath)
                sw.WriteLine("Start Log for today")
            Else
                sw = File.AppendText(FullPath)
            End If
            sw.WriteLine(Now.ToString & " :: " & strData)
            sw.Close()
        Catch ex As IOException
            ' SaveLogToFile("Saving Log File : &Error writing to log file.")
            'Debug.Print(ex.Message)
        End Try

        '----------------------------------
        Return True
    End Function
    Public Function GETLogfilename() As String
        '  GETFilename = ""
        Dim l As String
        Dim dayx As String
        Dim monthx As String
        Dim yr As String
        Dim hh As String
        Dim min As String
        Dim ss As String
        Dim rValue As String

        ' l = ConfData.LUNO
        dayx = CStr(DateTime.Now.Day)
        monthx = CStr(Now.Month)
        yr = CStr(Now.Year)

        hh = CStr(Now.Hour)
        min = CStr(Now.Minute)

        ss = CStr(Now.Second)
        If monthx.Length < 2 Then
            monthx = "0" & monthx.ToString
        End If
        If dayx.Length < 2 Then
            dayx = "0" & dayx.ToString
        End If
        If ss.Length < 2 Then
            ss = "0" & ss.ToString
        End If
        If hh.Length < 2 Then
            hh = "0" & hh.ToString
        End If
        If min.Length < 2 Then
            min = "0" & min.ToString
        End If
        rValue = monthx & dayx
        'If camVal = 1 Then
        '    rValue = yr & monthx & dayx & hh & min & ss
        'Else
        '    rValue = yr & monthx & dayx & hh & min & ss & "CAMERA 2"
        'End If

        Return rValue
    End Function
    'Public Function Readvalue(ByVal obj As Object, Optional ByVal xpectedType As String = "") As String
    '    Readvalue = ""
    '    Try

    '        If (obj) Or DBNull.Value Is obj Or obj Is Nothing Then
    '            If xpectedType = "int" Then
    '                Readvalue = 0
    '            Else
    '                Readvalue = ""
    '            End If

    '        ElseIf xpectedType = "int" Then
    '            If IsNumeric(obj) = False Then
    '                Readvalue = 0
    '            Else
    '                Readvalue = obj
    '            End If
    '        Else

    '            Readvalue = obj
    '        End If
    '        Return Readvalue
    '    Catch ex As Exception

    '    End Try
    'End Function
    Public Function ReturnJSONResults(Msg As GeneralResponds) As String
        Dim glob As New Chilkat.Global
        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")

        Dim rval As String
        'Log("Chilkat License Status : " & succes)


        'Dim status As Integer = glob.UnlockStatus


        Dim json As New Chilkat.JsonObject

        json.AddStringAt(-1, "Message", Msg.Message)
        json.AddStringAt(-1, "PayLoad", Msg.Payload)
        rval = json.Emit

        rval = rval.Replace("\", "")
        rval = rval.Replace(vbNewLine, "")
        rval = rval.Replace("""[", "[")
        rval = rval.Replace("]""", "]")


        Return rval
    End Function
    Public Function JsonForNewMandateResponse(udata As structMandate) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)


            'cmd.Parameters.AddWithValue("@creditacctname", myMandate.creditacctname)
            'cmd.Parameters.AddWithValue("@creditacctno", myMandate.creditacctno)
            'cmd.Parameters.AddWithValue("@creditbank", myMandate.creditbank)
            'cmd.Parameters.AddWithValue("@creditbankbranch", myMandate.creditbankbranch)
            'cmd.Parameters.AddWithValue("@approvalstatus", False)

            arr.AddObjectAt(0)
            Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            With udata


                obj.UpdateString("accountname", .Accountname)
                obj.UpdateString("bank", .Bank)
                obj.UpdateString("amount", .Amount)
                obj.UpdateString("customername", .CustomerName)
                obj.UpdateString("customerbankbranch", .CustomerBankBranch)
                obj.UpdateString("amounttorepay", .Amounttorepay)
                obj.UpdateString("signature", .signature)
                obj.UpdateString("effectdate", .effectdate)
                obj.UpdateString("transdate", .transdate)
                obj.UpdateString("mandaterefno", .mandaterefno)
                obj.UpdateString("accountnumber", .Accountnumber)
                obj.UpdateString("creditacctname", .creditacctname)
                obj.UpdateString("creditacctno", .creditacctno)
                obj.UpdateString("creditbank", .creditbank)
                obj.UpdateString("creditbankbranch", .creditbankbranch)

            End With
            ' Console.WriteLine("Chilkat Json Results : = " & arr.Emit)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForNewMandateResponse: " & ex.Message))
            SavetoLog("Error From JsonForNewMandateResponse: " & ex.Message)

        End Try

    End Function

    Public Function JsonForMobileloginResponse(Msg As MobileLoginResponds) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            arr.AddObjectAt(0)
            Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            With Msg

                '  obj.UpdateString("respondsmessage", Msg.respondsmessage)
                obj.UpdateString("role", Msg.role)
                obj.UpdateString("branch", Msg.branch)
                obj.UpdateString("role", Msg.role)
                obj.UpdateString("status", Msg.status)
                obj.UpdateString("isaccountvalid", Msg.isaccountvalid)
                obj.UpdateString("token", Msg.token)
                obj.UpdateString("ispassphotopresent", Msg.ispassphotopresent)
                obj.UpdateString("isidcardpresent", Msg.isidcardpresent)
                obj.UpdateString("issignaturepresent", Msg.issignaturepresent)
                obj.UpdateString("netsalary", Msg.netsalary)
            End With


            ' Console.WriteLine("Chilkat Json Results : = " & arr.Emit)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForMobileloginResponse: " & ex.Message))
            SavetoLog("Error From JsonForMobileloginResponse: " & ex.Message)

        End Try



        '-----------------------------------

        'Dim arr As New Chilkat.JsonArray

        'Dim success As Boolean

        ''  Add an empty object at the 1st JSON array position.
        'arr.AddObjectAt(0)
        ''  Get the object we just created.
        'Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
        'obj.UpdateString("Name", "Otto")
        'obj.UpdateInt("Age", 29)
        'obj.UpdateBool("Married", False)


        ''  Add an empty object at the 2nd JSON array position.
        'arr.AddObjectAt(1)
        'obj = arr.ObjectAt(1)
        'obj.UpdateString("Name", "Connor")
        'obj.UpdateInt("Age", 43)
        'obj.UpdateBool("Married", True)


        ''  Add an empty object at the 3rd JSON array position.
        'arr.AddObjectAt(2)
        'obj = arr.ObjectAt(2)
        'obj.UpdateString("Name", "Ramona")
        'obj.UpdateInt("Age", 34)
        'obj.UpdateBool("Married", True)


        ''  Examine what we have:
        'arr.EmitCompact = False
        'Debug.WriteLine(arr.Emit())


        ''  The output is:

        ''  [
        ''    {
        ''      "Name": "Otto",
        ''      "Age": 29,
        ''      "Married": false
        ''    },
        ''    {
        ''      "Name": "Connor",
        ''      "Age": 43,
        ''      "Married": true
        ''    },
        ''    {
        ''      "Name": "Ramona",
        ''      "Age": 34,
        ''      "Married": true
        ''    }
        ''  ]



        '----------------------------------



    End Function
    Public Function JsonForGeneralResponse(fld As String, fvalue As String) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            arr.AddObjectAt(0)
            Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            '   With Msg

            '  obj.UpdateString("respondsmessage", Msg.respondsmessage)
            obj.UpdateString(fld, fvalue)
            '    obj.UpdateString("branch", Msg.branch)
            '    obj.UpdateString("role", Msg.role)
            '    obj.UpdateString("status", Msg.status)
            '    obj.UpdateString("isaccountvalid", Msg.isaccountvalid)
            '    obj.UpdateString("token", Msg.token)
            '    obj.UpdateString("ispassphotopresent", Msg.ispassphotopresent)
            '    obj.UpdateString("isidcardpresent", Msg.isidcardpresent)
            '    obj.UpdateString("issignaturepresent", Msg.issignaturepresent)
            '    obj.UpdateString("netsalary", Msg.netsalary)
            'End With


            ' Console.WriteLine("Chilkat Json Results : = " & arr.Emit)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForGeneralResponse: " & ex.Message))
            SavetoLog("Error From JsonForGeneralResponse: " & ex.Message)

        End Try
    End Function
    Public Function JsonForAffordabilityResponse(msg As clsAffordabilty) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            arr.AddObjectAt(0)
            Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            '   With Msg

            '  obj.UpdateString("respondsmessage", Msg.respondsmessage)
            obj.UpdateString("affordabilityamount", msg.affordabilityamount)
            obj.UpdateString("isdefaulted", msg.defaulted)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForAffordabilityResponse: " & ex.Message))
            SavetoLog("Error From JsonForAffordabilityResponse: " & ex.Message)

        End Try
    End Function
    Public Function JsonForValidteOTPResponse(msg As StructValidateOTPResults) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            arr.AddObjectAt(0)
            Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            '   With Msg

            '  obj.UpdateString("respondsmessage", Msg.respondsmessage)
            obj.UpdateString("newpassword", msg.NEWPASSWORD)
            obj.UpdateString("resetusername", msg.RESETUSERNAME)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForValidteOTPResponse: " & ex.Message))
            SavetoLog("Error From JsonForValidteOTPResponse: " & ex.Message)

        End Try
    End Function
    Public Function JsonForDatatable(Dt As DataTable) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)


            For Each drr As DataRow In Dt.Rows
                ' row = New Dictionary(Of String, Object)()

                arr.AddObjectAt(jint)


                For Each col As DataColumn In Dt.Columns
                    ' row.Add(col.ColumnName, drr(col))

                    '  Dim priceObj As Chilkat.JsonObject = aPrice.ObjectAt(aPrice.Size - 1)
                    Dim obj As Chilkat.JsonObject = arr.ObjectAt(jint)

                    If col.DataType = System.Type.GetType("System.Byte[]") Then

                        If Not DBNull.Value.Equals(drr(col)) Then

                            '//Not null  
                            Dim colval As Byte() = drr(col)


                            obj.UpdateString(col.ColumnName.ToString, Convert.ToBase64String(colval))

                        Else

                            obj.UpdateString(col.ColumnName.ToString, "")
                        End If


                        ' success = priceObj.AddStringAt(-1, col.ColumnName.ToString, drr(col).ToString)
                    Else
                        obj.UpdateString(col.ColumnName.ToString, drr(col).ToString)
                    End If
                Next
                jint = jint + 1
                ' rows.Add(row)
            Next

            ' Console.WriteLine("Chilkat Json Results : = " & arr.Emit)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForDatatable: " & ex.Message))
            SavetoLog("Error From JsonForDatatable: " & ex.Message)

        End Try

        '--possible datacolumns data types

        '      // Create DataColumn objects of data types.
        'DataColumn colString = New DataColumn("StringCol");
        'colString.DataType = System.Type.GetType("System.String");
        'myTable.Columns.Add(colString); 

        'DataColumn colInt32 = New DataColumn("Int32Col");
        'colInt32.DataType = System.Type.GetType("System.Int32");
        'myTable.Columns.Add(colInt32);

        'DataColumn colBoolean = New DataColumn("BooleanCol");
        'colBoolean.DataType = System.Type.GetType("System.Boolean");
        'myTable.Columns.Add(colBoolean);

        'DataColumn colTimeSpan = New DataColumn("TimeSpanCol");
        'colTimeSpan.DataType = System.Type.GetType("System.TimeSpan");
        'myTable.Columns.Add(colTimeSpan);

        'DataColumn colDateTime = New DataColumn("DateTimeCol");
        'colDateTime.DataType = System.Type.GetType("System.DateTime");
        'myTable.Columns.Add(colDateTime);

        'DataColumn colDecimal = New DataColumn("DecimalCol");
        'colDecimal.DataType = System.Type.GetType("System.Decimal");
        'myTable.Columns.Add(colDecimal);

        'DataColumn colByteArray = New DataColumn("ByteArrayCol");
        'colByteArray.DataType = System.Type.GetType("System.Byte[]");
        'myTable.Columns.Add(colByteArray);



        '-----------------------------------

        'Dim arr As New Chilkat.JsonArray

        'Dim success As Boolean

        ''  Add an empty object at the 1st JSON array position.
        'arr.AddObjectAt(0)
        ''  Get the object we just created.
        'Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
        'obj.UpdateString("Name", "Otto")
        'obj.UpdateInt("Age", 29)
        'obj.UpdateBool("Married", False)


        ''  Add an empty object at the 2nd JSON array position.
        'arr.AddObjectAt(1)
        'obj = arr.ObjectAt(1)
        'obj.UpdateString("Name", "Connor")
        'obj.UpdateInt("Age", 43)
        'obj.UpdateBool("Married", True)


        ''  Add an empty object at the 3rd JSON array position.
        'arr.AddObjectAt(2)
        'obj = arr.ObjectAt(2)
        'obj.UpdateString("Name", "Ramona")
        'obj.UpdateInt("Age", 34)
        'obj.UpdateBool("Married", True)


        ''  Examine what we have:
        'arr.EmitCompact = False
        'Debug.WriteLine(arr.Emit())


        ''  The output is:

        ''  [
        ''    {
        ''      "Name": "Otto",
        ''      "Age": 29,
        ''      "Married": false
        ''    },
        ''    {
        ''      "Name": "Connor",
        ''      "Age": 43,
        ''      "Married": true
        ''    },
        ''    {
        ''      "Name": "Ramona",
        ''      "Age": 34,
        ''      "Married": true
        ''    }
        ''  ]



        '----------------------------------



    End Function
    Public Function JsonForDataReader(Drr As NpgsqlDataReader) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)

            If Drr.HasRows = False Then

                Return ""
            Else

                While Drr.Read
                    arr.AddObjectAt(jint)

                    For i As Integer = 0 To Drr.FieldCount - 1
                        Dim obj As Chilkat.JsonObject = arr.ObjectAt(jint)

                        If (Drr(i)) IsNot Nothing Then

                            ' .Parameters.AddWithValue("@IDCard", sVal.IDCard)
                            obj.UpdateString(Drr.GetName(i).ToString, Drr(i).ToString)

                        Else
                            obj.UpdateString(Drr.GetName(i).ToString, "")
                        End If




                    Next
                    jint = jint + 1
                End While

            End If



            Console.WriteLine("Chilkat Json Results : = " & arr.Emit)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForDataReader: " & ex.Message))
            SavetoLog("Error From JsonForDataReader: " & ex.Message)

        End Try




        '----------------------------------



    End Function

    Public Function JsonForUserResponse(udata As Users) As String
        '  Dim serializer As New 

        Try
            Dim glob As New Chilkat.Global
            Dim success As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            'Log("Chilkat License Status : " & succes)

            'Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            'Dim row As Dictionary(Of String, Object)

            Dim arr As New Chilkat.JsonArray
            Dim jint As Integer = 0

            '  Add an empty object at the 1st JSON array position.

            '  Get the object we just created.
            ' Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            arr.AddObjectAt(0)
            Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
            With udata

                obj.UpdateString("FirstName", .FirstName)
                obj.UpdateString("LastName", .LastName)
                obj.UpdateString("UserName", .UserName)
                obj.UpdateString("usertype", .usertype)
                obj.UpdateString("Email", .Email)
                obj.UpdateString("Role", .Role)
                obj.UpdateString("EnteredBy", .EnteredBy)
                obj.UpdateString("DateEntered", .DateEntered)
                obj.UpdateString("Status", .Status)
                obj.UpdateString("LastLogin", .LastLogin)
                obj.UpdateString("Branch", .Branch)
                obj.UpdateString("Locked", .Locked)
                obj.UpdateString("ExpiringDate", .ExpiringDate)
                obj.UpdateString("FirstTimeLogin", .FirstTimeLogin)
                obj.UpdateString("MobileNo", .MobileNo)
                obj.UpdateString("Token", .Token)

            End With


            Console.WriteLine("Chilkat Json Results : = " & arr.Emit)

            arr.EmitCompact = True

            Return arr.Emit
        Catch ex As Exception
            Console.WriteLine(("Error From JsonForUserResponse: " & ex.Message))
            SavetoLog("Error From JsonForUserResponse: " & ex.Message)

        End Try



        '-----------------------------------

        'Dim arr As New Chilkat.JsonArray

        'Dim success As Boolean

        ''  Add an empty object at the 1st JSON array position.
        'arr.AddObjectAt(0)
        ''  Get the object we just created.
        'Dim obj As Chilkat.JsonObject = arr.ObjectAt(0)
        'obj.UpdateString("Name", "Otto")
        'obj.UpdateInt("Age", 29)
        'obj.UpdateBool("Married", False)


        ''  Add an empty object at the 2nd JSON array position.
        'arr.AddObjectAt(1)
        'obj = arr.ObjectAt(1)
        'obj.UpdateString("Name", "Connor")
        'obj.UpdateInt("Age", 43)
        'obj.UpdateBool("Married", True)


        ''  Add an empty object at the 3rd JSON array position.
        'arr.AddObjectAt(2)
        'obj = arr.ObjectAt(2)
        'obj.UpdateString("Name", "Ramona")
        'obj.UpdateInt("Age", 34)
        'obj.UpdateBool("Married", True)


        ''  Examine what we have:
        'arr.EmitCompact = False
        'Debug.WriteLine(arr.Emit())


        ''  The output is:

        ''  [
        ''    {
        ''      "Name": "Otto",
        ''      "Age": 29,
        ''      "Married": false
        ''    },
        ''    {
        ''      "Name": "Connor",
        ''      "Age": 43,
        ''      "Married": true
        ''    },
        ''    {
        ''      "Name": "Ramona",
        ''      "Age": 34,
        ''      "Married": true
        ''    }
        ''  ]



        '----------------------------------



    End Function

    Public Function GenerateFIID() As String
        Try
            ' Dim number As String = "FI" + String.Format("{0:d9}", (DateTime.Now.Ticks / 10) Mod 1000000000)
            Dim number As String = "FI" & String.Format("{0:000000000}", (DateTime.Now.Ticks / 10) Mod 1000000000)
            Return number
        Catch ex As Exception
            SavetoLog("GenerateFIID = " & ex.Message)
            Return ""
        End Try

    End Function
    Public Function GenerateBankID() As String
        Try
            ' Dim number As String = "FI" + String.Format("{0:d9}", (DateTime.Now.Ticks / 10) Mod 1000000000)
            Dim number As String = "BNK" & String.Format("{0:000000000}", (DateTime.Now.Ticks / 10) Mod 1000000000)
            Return number
        Catch ex As Exception
            SavetoLog("GenerateBankID = " & ex.Message)
            Return ""
        End Try

    End Function

    Public Function GenerateInstID() As String
        Try
            ' Dim number As String = "FI" + String.Format("{0:d9}", (DateTime.Now.Ticks / 10) Mod 1000000000)
            Dim number As String = "INS" & String.Format("{0:000000000}", (DateTime.Now.Ticks / 10) Mod 1000000000)
            Return number
        Catch ex As Exception
            SavetoLog("GenerateInstID = " & ex.Message)
            Return ""
        End Try

    End Function

    Public Function AuditLogs(ByVal UserName As String, ByVal Activity As String, ByVal Connection As String, ByVal Action As String) As String

        Try
            Dim ip As String = (Environment.MachineName & ("(" _
                    + (Dns.GetHostEntry(Environment.MachineName).AddressList(0).ToString & ")")))
            Dim conn As SqlConnection = New SqlConnection(Connection)
            conn.Open()
            Dim cmd As SqlCommand = New SqlCommand("INSERT INTO AuditTrail(DateEntered, Activity, UserName, Action) VALUES (@DateEntered, @Activity, @Use" &
            "rName, @Action)", conn)
            'passing parameters to query
            cmd.Parameters.AddWithValue("@DateEntered", DateTime.Now)
            cmd.Parameters.AddWithValue("@Activity", Activity.ToString)
            cmd.Parameters.AddWithValue("@UserName", UserName.ToString)
            cmd.Parameters.AddWithValue("@Action", Action.ToString)
            '   cmd.Parameters.AddWithValue("@IPAddress", ip);
            cmd.ExecuteNonQuery()
            conn.Close()
            Return Activity
            'Return UserName
            'Return Connection
        Catch ex As Exception
            SavetoLog("Error From Audit Trail " & ex.Message)
        End Try

    End Function

    Public Function ReadDBValuesStrings(ByRef dr As DataRow, ByRef field As String) As String
        Try
            If dr(field) Is DBNull.Value Then
                Return ""
            Else

                Return Convert.ToString(dr(field))
            End If
        Catch ex As Exception

            SavetoLog("Error Unable to read DbField Value: " & ex.Message)
            Return ""
        End Try
    End Function


    Public Function ReadDBValuesBoolean(ByRef dr As DataRow, ByRef field As String) As Boolean
        Try
            If dr(field) Is DBNull.Value Then
                Return False
            Else

                Return Convert.ToBoolean(dr(field))
            End If
        Catch ex As Exception

            SavetoLog("Error Unable to read DbField Value: " & ex.Message)
            Return ""
        End Try
    End Function

    Public Function ReadDBValuesDate(ByRef dr As DataRow, ByRef field As String) As String
        Try
            If dr(field) Is DBNull.Value Then
                Return "1.1.0001 0:00:00"
            Else

                Return Convert.ToDateTime(dr(field))
            End If
        Catch ex As Exception

            SavetoLog("Error Unable to read DbField Value: " & ex.Message)
            Return ""
        End Try
    End Function

    Public Function GetConfigurator(confid As Integer) As String

        Try
            Dim ssql As String
            Dim Dr As DataRow
            ssql = "SELECT Value from Configurators WHERE id =  " & confid

            Dr = DBConnection.GetRowInfo(ssql)
            If Dr Is Nothing Then
                Return ""
            Else

                Dim rval As String
                rval = Dr("Value").ToString

                Return rval
            End If
        Catch ex As Exception
            Return ""
            SavetoLog("Error from GetConfigurator : " & ex.Message)
        End Try


    End Function

End Module
