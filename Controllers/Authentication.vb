﻿Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class Authentication
    Inherits ControllerBase

    <HttpPost("application/json")>
    <Route("/admin/auth/login")>
    Public Function Login(<FromBody()> ByVal values As AuthModel) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            Try
                'validate returned values
                Dim Message As New GeneralResponds
                Dim UserResponds As New Users

                If values.username = "" Or values.username.Length <= 0 Or values.password = "" Or values.password.Length <= 0 Then
                    Message.Message = "Wrong Credentials"
                    Message.Payload = "Username Or Password Required"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    ' Go to dabase and validate record
                    Dim myCheckStatus As New StructCheckStatus
                    Dim myUserTypeContent As New StructUserTypeContent
                    Dim myCheckInst As New StructCheckInstitutionStatus
                    myCheckStatus = DBConnection.CheckStatus(values)
                    myUserTypeContent = DBConnection.UserTypeContent(values)
                    myCheckInst = DBConnection.CheckInstStatus(myUserTypeContent.UserInstID)

                    ' Begin validations

                    If myCheckStatus.UserStatus = "False" Then
                        Message.Message = "Unable to Login .Please Contact Administrator if you are not registered."
                        Message.Payload = "Registration Not Complete with User Status is False"
                        Return BadRequest(ReturnJSONResults(Message))
                        '  LblEnterCredentials.Visible = true;
                    ElseIf myCheckInst.InstStatus = "False" Then
                        Message.Message = "Your Institution has been disabled. Contact your Institution Administrator......."
                        Message.Payload = "Institution Is False "
                        Return BadRequest(ReturnJSONResults(Message))

                    ElseIf myCheckStatus.UserLocked = "True" Then
                        Message.Message = "Your Account Has Been Locked Please Contact Administrator....."
                        Message.Payload = "Account Locked "
                        Return BadRequest(ReturnJSONResults(Message))

                        '      iderror.Visible = true;
                    End If

                    'Now Begin Authentications
                    Dim sec As New Security
                    Dim Dt As DataTable
                    Dim sql As String
                    ' sql = "SELECT (UserName)AS Name, Password, UserGuid from Users where UserName = @UName UNION select(StaffID) AS Name, Password, UserGuid from Staffs where StaffID =@UName"
                    sql = "SELECT * from Users where UserName = @UName"
                    Dt = DBConnection.GetUserTableData(sql, values)

                    Dim dbPassword As String = ""
                    Dim dbUserGuid As String = ""
                    Dim bdusername As String = ""
                    Dim hashedPassword As String = ""

                    For Each dr As DataRow In Dt.Rows
                        dbPassword = Convert.ToString(dr("password"))
                        dbUserGuid = Convert.ToString(dr("userguid"))
                        bdusername = Convert.ToString(dr("UserName")).ToLower

                        hashedPassword = sec.HashSHA1((values.password.ToString & dbUserGuid))

                        With UserResponds

                            .FirstName = ReadDBValuesStrings(dr, "FirstName")
                            .LastName = ReadDBValuesStrings(dr, "LastName")
                            .UserName = ReadDBValuesStrings(dr, "UserName")
                            .Password = ReadDBValuesStrings(dr, "password")
                            ' .UserGuid = Convert.ToString(dr("password")
                            .Email = ReadDBValuesStrings(dr, "Email")
                            .Role = ReadDBValuesStrings(dr, "Role")
                            .EnteredBy = ReadDBValuesStrings(dr, "EnteredBy")
                            .DateEntered = ReadDBValuesDate(dr, "DateEntered")
                            .Status = ReadDBValuesBoolean(dr, "Status")
                            .LastLogin = ReadDBValuesDate(dr, "LastLogin")
                            .Branch = ReadDBValuesStrings(dr, "Branch")
                            .Locked = ReadDBValuesBoolean(dr, "Locked")
                            .ExpiringDate = ReadDBValuesStrings(dr, "ExpiringDate")
                            .FirstTimeLogin = ReadDBValuesStrings(dr, "FirstTimeLogin")
                            .MobileNo = ReadDBValuesStrings(dr, "MobileNo")
                            .usertype = ReadDBValuesStrings(dr, "usertype")
                            '.Code = Convert.ToString(dr("Code"))
                            '.CodeTime = Convert.ToString(dr("CodeTime"))

                        End With
                    Next

                    If (dbPassword = hashedPassword) And (values.username.ToLower = bdusername) Then
                        ''    SystemLogContent();
                        'Session.Add("UserName", Convert.ToString(dr("Name")))
                        ''     UpdateLocked();
                        Dim mesage As String = (bdusername + " Successfully logged onto the system")
                        Dim log As String = "Login"


                        AuditLogs(values.username, mesage, DBConnection.DbConString, log)
                        ''   Session.Add("permission", Convert.ToString(dr["permission"]));
                        ''    FormsAuthentication.RedirectFromLoginPage(Convert.ToString(dr["username"]), false);
                        'Response.Redirect("~/Admin/Dashboard.aspx")

                        'Bright Comments
                        'Generate Token and add to Users model to populate  as responds payload

                        Dim mytoken As String
                        mytoken = GenerateJWT(values.username)

                        UserResponds.Token = mytoken

                        Dim rval As String

                        Message.Message = "Login Successful"
                        Message.Payload = JsonForUserResponse(UserResponds)
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)


                    Else
                        Message.Message = "Wrong Credentials"
                        Message.Payload = "Username or Password Wrong "
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    '     Details();

                End If


            Catch ex As Exception
                SavetoLog("Error From Login : " & ex.Message)
            End Try

        End If


    End Function
End Class
