﻿Public Class Institutions
    Public Property InsNo As String
    Public Property Name As String
    Public Property Address As String
    Public Property Phone As String
    Public Property Location As String
    Public Property BusinessType As String
    Public Property DateRegistered As Date
    Public Property Status As Boolean
    Public Property RegisteredBy As String

    Public Property TIN As String
    Public Property GPS As String
    Public Property CertOfInco As Byte()
    Public Property CertOfBusiness As Byte()
    Public Property OfficePicture As Byte()


    Public Property ContactPerson As String
    Public Property ContactEmail As String

End Class
