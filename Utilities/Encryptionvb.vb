﻿Imports System.Text
Imports System.Collections.Specialized
Imports System.Security.Cryptography


'QhF?{3:8e+_)95)z-87Lavpqg+,QnZ{jTqU!:D$JDwq'tDc6kRXEy4^*);Gu8NnJVY6Yc}Gaw2*7\umBZZ)Y,GkMa`7Y`T5)z5zLDzJSeQcd(?B\:tU<9v;))H23Y9~[

Public Class Security
    Private lbtVector() As Byte = {240, 3, 45, 29, 0, 76, 173, 59}
    Private lscryptoKey As String = "QhF?{3:8e+_)95)z-87Lavpqg+,QnZ{jTqU!:D$JDwq'tDc6kRXEy4^*);Gu8NnJVY6Yc}Gaw2*7\umBZZ)Y,GkMa`7Y`T5)z5zLDzJSeQcd(?B\:tU<9v;))H23Y9~[!"

    Public Function HashSHA1(ByVal value As String) As String
        Dim sha1 = System.Security.Cryptography.SHA1.Create
        Dim inputBytes = Encoding.ASCII.GetBytes(value)
        Dim hash = sha1.ComputeHash(inputBytes)
        Dim sb = New StringBuilder
        Dim i = 0
        Do While (i < hash.Length)
            sb.Append(hash(i).ToString("X2"))
            i = (i + 1)
        Loop

        Return sb.ToString
    End Function

    Public Function psDecrypt(ByVal sQueryString As String) As String

        Dim buffer() As Byte
        Dim loCryptoClass As New TripleDESCryptoServiceProvider
        Dim loCryptoProvider As New MD5CryptoServiceProvider

        Try

            buffer = Convert.FromBase64String(sQueryString)
            loCryptoClass.Key = loCryptoProvider.ComputeHash(ASCIIEncoding.ASCII.GetBytes(lscryptoKey))
            loCryptoClass.IV = lbtVector
            Return Encoding.ASCII.GetString(loCryptoClass.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
        Catch ex As Exception
            Throw ex
        Finally
            loCryptoClass.Clear()
            loCryptoProvider.Clear()
            loCryptoClass = Nothing
            loCryptoProvider = Nothing
        End Try


    End Function
    'Author      :       Nikhil Gupta
    'Description :       This function encrypts a given string
    'Parameters  :       String
    'Return Values:      Encrypted String
    'Called From :       Business Layer
    Public Function psEncrypt(ByVal sInputVal As String) As String

        Dim loCryptoClass As New TripleDESCryptoServiceProvider
        Dim loCryptoProvider As New MD5CryptoServiceProvider
        Dim lbtBuffer() As Byte

        Try
            lbtBuffer = System.Text.Encoding.ASCII.GetBytes(sInputVal)
            loCryptoClass.Key = loCryptoProvider.ComputeHash(ASCIIEncoding.ASCII.GetBytes(lscryptoKey))
            loCryptoClass.IV = lbtVector
            sInputVal = Convert.ToBase64String(loCryptoClass.CreateEncryptor().TransformFinalBlock(lbtBuffer, 0, lbtBuffer.Length()))
            psEncrypt = sInputVal
        Catch ex As CryptographicException
            Throw ex
        Catch ex As FormatException
            Throw ex
        Catch ex As Exception
            Throw ex
        Finally
            loCryptoClass.Clear()
            loCryptoProvider.Clear()
            loCryptoClass = Nothing
            loCryptoProvider = Nothing
        End Try
    End Function

End Class