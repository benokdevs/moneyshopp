﻿

Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Imports Npgsql
Imports System.Globalization

Public Class MobileApi
    Inherits ControllerBase

    Dim perm, registeredstatus As String
    Dim locked As String
    Dim firsttimelogin As String
    Dim expiringdate As String
    Dim dbPassword, dbUserGuid, bdusername, dbimei, dbrole, dbbranch, dbregis, dbstatus, hashedPassword, response, ispassphotopresent, isidcardpresent, issignaturepresent, NettSalary As String
    Dim token As String
    Dim NEWPASSWORD As String
    Dim RESETUSERNAME As String

    <HttpPost("application/json")>
    <Route("/mobileapi/login")>
    Public Function mobilelogin(<FromBody()> ByVal values As AuthModel) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            Try
                'validate returned values
                Dim Message As New GeneralResponds
                Dim MobMessage As New MobileLoginResponds
                Dim UserResponds As New Users



                If values.username = "" Or values.username.Length <= 0 Or values.password = "" Or values.password.Length <= 0 Then
                    Message.Message = "Wrong Credentials"
                    Message.Payload = "Username Or Password Required"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    ' Go to dabase and validate record
                    'Dim myCheckStatus As New StructCheckStatus
                    'Dim myUserTypeContent As New StructUserTypeContent
                    'Dim myCheckInst As New StructCheckInstitutionStatus
                    'myCheckStatus = DBConnection.CheckStatus(values)
                    'myUserTypeContent = DBConnection.UserTypeContent(values)
                    'myCheckInst = DBConnection.CheckInstStatus(myUserTypeContent.UserInstID)

                    ' Begin validations

                    If values.appversion Is Nothing Then
                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    ElseIf values.appversion = "" Then

                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'If myCheckStatus.UserStatus = "False" Then
                    '    Message.Message = "Unable to Login .Please Contact Administrator if you are not registered."
                    '    Message.Payload = "Registration Not Complete with User Status is False"
                    '    Return BadRequest(ReturnJSONResults(Message))
                    '    '  LblEnterCredentials.Visible = true;
                    'ElseIf myCheckInst.InstStatus = "False" Then
                    '    Message.Message = "Your Institution has been disabled. Contact your Institution Administrator......."
                    '    Message.Payload = "Institution Is False "
                    '    Return BadRequest(ReturnJSONResults(Message))

                    'ElseIf myCheckStatus.UserLocked = "True" Then
                    '    Message.Message = "Your Account Has Been Locked Please Contact Administrator....."
                    '    Message.Payload = "Account Locked "
                    '    Return BadRequest(ReturnJSONResults(Message))



                    '      iderror.Visible = true;
                    '  End If

                    'Now Begin Authentications

                    If CheckAppVersion(values.appversion) = True Then

                        Dim sec As New Security
                        Dim Dt As DataTable
                        Dim sql As String
                        ' sql = "SELECT (UserName)AS Name, Password, UserGuid from Users where UserName = @UName UNION select(StaffID) AS Name, Password, UserGuid from Staffs where StaffID =@UName"
                        sql = "select staffid, password, userguid, role, insid, registeredstatus, status, username,picture,idcard,signature,netsalary from staffs where username=@uname"
                        Dt = DBConnection.GetUserTableData(sql, values)

                        Dim dbPassword As String = ""
                        Dim dbUserGuid As String = ""
                        Dim bdusername As String = ""
                        Dim hashedPassword As String = ""

                        For Each dr As DataRow In Dt.Rows
                            dbPassword = Convert.ToString(dr("password"))
                            dbUserGuid = Convert.ToString(dr("userguid"))
                            bdusername = Convert.ToString(dr("username")).ToLower

                            hashedPassword = sec.HashSHA1((values.password.ToString & dbUserGuid))

                            With UserResponds

                                dbrole = Convert.ToString(dr("role"))
                                dbbranch = Convert.ToString(dr("insid"))
                                dbstatus = Convert.ToString(dr("status"))
                                dbregis = Convert.ToString(dr("registeredstatus"))
                                NettSalary = Convert.ToString(dr("netsalary"))

                            End With


                            If Not String.IsNullOrWhiteSpace(dr("picture").ToString()) Then

                                'not null
                                Dim picData As Byte() = If(TryCast(dr("picture"), Byte()), Nothing)
                                Dim idl As Integer = picData.Length

                                If idl <= 0 Then
                                    ispassphotopresent = "false"
                                Else
                                    ispassphotopresent = "true"
                                End If
                            Else
                                'null

                                ispassphotopresent = "false"
                            End If

                            If Not String.IsNullOrWhiteSpace(dr("idcard").ToString()) Then
                                'not null
                                Dim picdata As Byte() = If(TryCast(dr("idcard"), Byte()), Nothing)
                                Dim idl As Integer = picdata.Length

                                If idl <= 0 Then
                                    isidcardpresent = "false"
                                Else
                                    isidcardpresent = "true"
                                End If
                            Else
                                isidcardpresent = "false"
                            End If

                            If Not String.IsNullOrWhiteSpace(dr("signature").ToString()) Then

                                'not null
                                Dim picdata As Byte() = If(TryCast(dr("signature"), Byte()), Nothing)
                                Dim idl As Integer = picdata.Length

                                If idl <= 0 Then
                                    issignaturepresent = "false"
                                Else
                                    issignaturepresent = "true"
                                End If
                            Else
                                'null
                                issignaturepresent = "false"
                            End If

                        Next

                        If (dbPassword = hashedPassword) And (values.username.ToLower = bdusername) Then
                            ''    SystemLogContent();
                            'Session.Add("UserName", Convert.ToString(dr("Name")))
                            ''     UpdateLocked();
                            Dim mesage As String = (bdusername + " Successfully logged onto the system")
                            Dim log As String = "Login"


                            AuditLogs(values.username, mesage, DBConnection.DbConString, log)
                            ''   Session.Add("permission", Convert.ToString(dr["permission"]));
                            ''    FormsAuthentication.RedirectFromLoginPage(Convert.ToString(dr["username"]), false);
                            'Response.Redirect("~/Admin/Dashboard.aspx")

                            'Bright Comments
                            'Generate Token and add to Users model to populate  as responds payload

                            Dim mytoken As String
                            mytoken = GenerateMobileJWT(values.username)

                            ' UserResponds.Token = mytoken

                            ' Dim rval As String

                            ' MobMessage.respondsmessage = "Login Successful"



                            With MobMessage
                                .role = dbrole
                                .branch = dbbranch
                                .status = dbstatus
                                .isaccountvalid = dbregis
                                .token = mytoken
                                .ispassphotopresent = ispassphotopresent
                                .isidcardpresent = isidcardpresent
                                .issignaturepresent = issignaturepresent
                                .netsalary = NettSalary
                            End With

                            Dim rval As String

                            Message.Message = "Login Successful"
                            Message.Payload = JsonForMobileloginResponse(MobMessage)
                            rval = ReturnJSONResults(Message)

                            Return Ok(rval)


                        Else
                            Message.Message = "Wrong Credentials"
                            Message.Payload = "Username or Password Wrong "
                            Return BadRequest(ReturnJSONResults(Message))
                        End If



                        '     Details();
                    Else
                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                End If


            Catch ex As Exception
                SavetoLog("Error From Login : " & ex.Message)
            End Try

        End If


    End Function

    <HttpPost("application/json")>
    <Route("/mobileapi/forgotpassword")>
    Public Function MobileForgorPassword(<FromBody()> ByVal values As MobileForgotPasswordRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            Try
                'validate returned values
                Dim Message As New GeneralResponds



                If values.appversion = "" Or values.appversion.Length <= 0 Or values.mobileno = "" Or values.mobileno.Length <= 0 Then
                    Message.Message = "Invalid App Version or Mobile Number"
                    Message.Payload = "App Version and Mobile Number Required"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.appversion Is Nothing Then
                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    ElseIf values.appversion = "" Then

                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    If CheckAppVersion(values.appversion) = True Then

                        Dim sec As New Security
                        Dim Dt As DataTable
                        Dim sql As String
                        ' sql = "SELECT (UserName)AS Name, Password, UserGuid from Users where UserName = @UName UNION select(StaffID) AS Name, Password, UserGuid from Staffs where StaffID =@UName"
                        sql = "SELECT MobileNo,UserName FROM Staffs where MobileNo=@val"
                        Dt = DBConnection.GetGeneralTableData(sql, values.mobileno.Trim)

                        Dim otpp As String = ""
                        Dim uname As String = ""

                        For Each dr As DataRow In Dt.Rows

                            uname = Convert.ToString(dr("username")).ToLower

                        Next

                        Do
                            otpp = GenerateOTP()
                        Loop While CheckDuplicateOTP(otpp) = True

                        Dim myFP As New MobForgotPassword


                        If DeleteOldOTPRecords(values.mobileno.Trim) = True Then
                            Dim currentTime As Date = Date.Now
                            Dim exptotptime As Date = currentTime.AddMinutes(GetOTPDuration())
                            myFP.mobileno = values.mobileno.Trim
                            myFP.username = uname
                            myFP.otp = otpp
                            myFP.expiry = exptotptime

                            If SaveOTP(myFP) = True Then

                                'Send SMS here

                                Dim msg As String = SMSForgotpasswordtemplate.Replace("<<OTP>>", otpp)
                                Dim desti As String = values.mobileno.Trim
                                Dim sendbyy As String = "MoneyShopp"
                                'Dim sendbyy As String = "GodlyFavour"
                                Dim email As String = "spyglassgh@gmail.com"

                                If SendSMS(msg, desti, sendbyy, email) = True Then
                                    Dim rval As String

                                    Message.Message = "Successful"
                                    Message.Payload = "true"
                                    rval = ReturnJSONResults(Message)

                                    Return Ok(rval)

                                    ' Return "0|Successful"
                                Else
                                    Message.Message = "Unable to Send OTP"
                                    Message.Payload = "false"
                                    Return BadRequest(ReturnJSONResults(Message))

                                    ' Return "0|Unable to Send OTP"
                                End If
                            Else
                                Message.Message = "Unable to Save OTP"
                                Message.Payload = "false"
                                Return BadRequest(ReturnJSONResults(Message))
                                ' Return "1|Unable to Save OTP"
                            End If
                        Else
                            Message.Message = "Unable to Remove OTP"
                            Message.Payload = "false"
                            Return BadRequest(ReturnJSONResults(Message))
                            ' Return "1|Unable to Remove OTP"
                        End If

                    Else
                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                End If


            Catch ex As Exception
                SavetoLog("Error From MobileForgorPassword : " & ex.Message)
            End Try

        End If


    End Function
    <HttpPost("application/json")>
    <Route("/mobileapi/changepassword")>
    Public Function MobileChangePassword(<FromBody()> ByVal values As MobileChangePasswordReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            Try
                'validate returned values
                Dim Message As New GeneralResponds
                Dim apversion
                If values.forgotpassword = False Then

                    Dim myToken As String
                    myToken = HttpContext.Request.Headers("token").ToString()


                    If VerifyJWT(myToken) = False Then
                        Message.Message = "Invalid Token/Expired Token"
                        Message.Payload = "Invalid Token/Expired Token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    Dim jwtv As New StructJWTValues
                    jwtv = GetJWTValues(myToken)
                    apversion = jwtv.appversion
                    If jwtv.appversion Is Nothing Then
                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    ElseIf jwtv.appversion = "" Then

                        Message.Message = "Invalid Mobile Version"
                        Message.Payload = "Invalid Mobile Version"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                Else
                    apversion = values.appversion


                End If
                If values.oldpassword Is Nothing Then
                    Message.Message = "Old Password Required Mobile Version"
                    Message.Payload = "Field Required"
                    Return BadRequest(ReturnJSONResults(Message))
                ElseIf values.oldpassword = "" Then
                    Message.Message = "Old Password Required Mobile Version"
                    Message.Payload = "Field Required"
                    Return BadRequest(ReturnJSONResults(Message))
                ElseIf values.newpassword Is Nothing Then
                    Message.Message = "New Password Required Mobile Version"
                    Message.Payload = "Field Required"
                    Return BadRequest(ReturnJSONResults(Message))
                ElseIf values.newpassword = "" Then
                    Message.Message = "New Password Required Mobile Version"
                    Message.Payload = "Field Required"
                    Return BadRequest(ReturnJSONResults(Message))
                End If


                If CheckAppVersion(apversion) = True Then

                    Dim sec As New Security
                    Dim Dt As DataTable
                    Dim sql As String
                    ' sql = "SELECT (UserName)AS Name, Password, UserGuid from Users where UserName = @UName UNION select(StaffID) AS Name, Password, UserGuid from Staffs where StaffID =@UName"
                    sql = "select staffid, password, userguid, role, insid, registeredstatus, status, username,picture,idcard,signature,netsalary from staffs where username=@val"
                    Dt = DBConnection.GetGeneralTableData(sql, values.username)

                    Dim otpp As String = ""
                    Dim uname As String = ""
                    Dim dbstatus As String = ""

                    Dim UserResponds As New MobileLogin


                    For Each dr As DataRow In Dt.Rows
                        With UserResponds
                            .dbpassword = Convert.ToString(dr("password"))
                            .dbuserguid = Convert.ToString(dr("userguid"))
                            .dbusername = Convert.ToString(dr("username")).ToLower()

                            .dbrole = Convert.ToString(dr("role"))
                            .dbbranch = Convert.ToString(dr("insid"))
                            .dbstatus = Convert.ToString(dr("status"))
                            .dbregis = Convert.ToString(dr("registeredstatus"))
                            .hashedPassword = sec.HashSHA1(values.oldpassword + .dbuserguid)
                        End With

                    Next

                    If ((UserResponds.dbpassword = UserResponds.hashedPassword) AndAlso (values.username.ToLower = UserResponds.dbusername.ToLower)) Then
                        Try
                            'Now set the new password and Update password
                            '   Guid userGuid = System.Guid.NewGuid();
                            ' Hash the password together with our unique userGuid
                            Dim hashedPassword As String = sec.HashSHA1((values.newpassword + UserResponds.dbuserguid.ToString))
                            Dim connn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                            connn.Open()
                            Dim cmmd As NpgsqlCommand = New NpgsqlCommand("update staffs set password=@password where username=@username", connn)
                            'passing parameters to query
                            cmmd.Parameters.AddWithValue("@username", UserResponds.dbusername)
                            cmmd.Parameters.AddWithValue("@password", hashedPassword)
                            cmmd.ExecuteNonQuery()
                            connn.Close()
                            ' response = "Successful"


                            Dim rval As String

                            Message.Message = "Successful"
                            Message.Payload = JsonForGeneralResponse("status", "true")
                            rval = ReturnJSONResults(Message)

                            Return Ok(rval)

                        Catch ex As Exception
                            Message.Message = "Unable to Change Password"
                            Message.Payload = "Internal Error"
                            Return BadRequest(ReturnJSONResults(Message))
                            ' response = "Unable to Change Password"
                            ' throw ex;
                        End Try

                    Else

                        Message.Message = "Failed Credentials for old Password"
                        Message.Payload = "Old Password Validation Failed"
                        Return BadRequest(ReturnJSONResults(Message))
                        'Message.Message = "Invalid Mobile Version"
                        'Message.Payload = "Invalid Mobile Version"
                        'Return BadRequest(ReturnJSONResults(Message))

                    End If
                Else
                    Message.Message = "Kindly Confirm you Credentials"
                    Message.Payload = "Failed Credentials"
                    Return BadRequest(ReturnJSONResults(Message))

                End If

            Catch ex As Exception
                SavetoLog("Error From MobileChangePassword : " & ex.Message)
            End Try

        End If



    End Function


    <HttpPost("application/json")>
    <Route("/mobileapi/resetpassword")>
    Public Function MobResetPassword(<FromBody()> ByVal values As MobileChangePasswordReq, Optional forgotpassword As Boolean = False) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            Dim Message As New GeneralResponds
            If values.appversion Is Nothing Then
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then

                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            ElseIf values.oldpassword Is Nothing Then
                Message.Message = "Old Password Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.oldpassword = "" Then
                Message.Message = "Old Password Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.newpassword Is Nothing Then
                Message.Message = "New Password Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.newpassword = "" Then
                Message.Message = "New Password Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            If CheckAppVersion(values.appversion) = True Then
                'confirm if oldpassword can log in


                Dim sec As New Security
                Dim Dt As DataTable
                Dim sql As String
                ' sql = "SELECT (UserName)AS Name, Password, UserGuid from Users where UserName = @UName UNION select(StaffID) AS Name, Password, UserGuid from Staffs where StaffID =@UName"
                sql = "select staffid, password, userguid, role, insid, registeredstatus, status, username,picture,idcard,signature,netsalary from staffs where username=@val"
                Dt = DBConnection.GetGeneralTableData(sql, values.username)

                Dim otpp As String = ""
                Dim uname As String = ""
                Dim dbstatus As String = ""

                Dim mylogin As New MobileLogin


                For Each dr As DataRow In Dt.Rows
                    With mylogin
                        .dbpassword = Convert.ToString(dr("password"))
                        .dbuserguid = Convert.ToString(dr("userguid"))
                        .dbusername = Convert.ToString(dr("username")).ToLower()

                        .dbrole = Convert.ToString(dr("role"))
                        .dbbranch = Convert.ToString(dr("insid"))
                        .dbstatus = Convert.ToString(dr("status"))
                        .dbregis = Convert.ToString(dr("registeredstatus"))
                        .hashedPassword = sec.HashSHA1(values.oldpassword + .dbuserguid)
                    End With

                Next


                If ((mylogin.dbpassword = mylogin.hashedPassword) AndAlso (values.username.ToLower = mylogin.dbusername.ToLower)) Then
                    Try
                        'Now set the new password and Update password
                        '   Guid userGuid = System.Guid.NewGuid();
                        ' Hash the password together with our unique userGuid
                        Dim hashedPassword As String = sec.HashSHA1((values.newpassword + mylogin.dbuserguid.ToString))
                        Dim connn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                        connn.Open()
                        Dim cmmd As NpgsqlCommand = New NpgsqlCommand("update staffs set password=@password where username=@username", connn)
                        'passing parameters to query
                        cmmd.Parameters.AddWithValue("@username", mylogin.dbusername)
                        cmmd.Parameters.AddWithValue("@password", hashedPassword)
                        cmmd.ExecuteNonQuery()
                        connn.Close()
                        ' response = "Successful"


                        Dim rval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("status", "true")
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)

                    Catch ex As Exception
                        Message.Message = "Unable to Change Password"
                        Message.Payload = "Internal Error"
                        Return BadRequest(ReturnJSONResults(Message))
                        ' response = "Unable to Change Password"
                        ' throw ex;
                    End Try

                Else

                    Message.Message = "Failed Credentials for old Password"
                    Message.Payload = "Old Password Validation Failed"
                    Return BadRequest(ReturnJSONResults(Message))

                End If
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function


    <HttpPost("application/json")>
    <Route("/mobileapi/loanapplication")>
    Public Function MobLoanApplication(<FromBody()> ByVal values As MobileLoanApplicationReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values
            Dim Message As New GeneralResponds



            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then
                Dim username As String

                username = GetJWTUsername(myToken)

                If username = "" Or username Is Nothing Then
                    Message.Message = "Unable to Retrieve Username from token"
                    Message.Payload = "Unable to Retrieve Username from token"
                    Return BadRequest(ReturnJSONResults(Message))
                End If

                Try

                    Dim availableloan As String
                    Dim interest As String = InterestRate()
                    Dim Limit As String = LoanLimit()
                    Dim owing As String = CheckOwing(username)
                    Dim check As String = CheckApplication(username)
                    Dim sd As StructStaffDetails = StaffDetails(username)

                    Dim ll As Decimal = (Decimal.Parse(Limit) * Decimal.Parse("0.01"))
                    If (check = "") Then
                        check = "0.00"
                    End If

                    Dim old As Decimal = Decimal.Parse(check)
                    Dim net As Decimal = Decimal.Parse(sd.netsalary)
                    availableloan = ((ll * net) _
                - old).ToString
                    '     string staffID, Name, Institution, AccountNo, Mobile, NetSalary;
                    If (Decimal.Parse(availableloan) < Decimal.Parse(values.amount)) Then
                        response = ("2000" + (";" _
                    + (Limit + (";" + sd.netsalary))))
                    Else
                        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                        conn.Open()
                        Dim cmd As NpgsqlCommand = New NpgsqlCommand("insert into salaryloan(username, institution, amount, fi, status, dateapplied, timeapplied, modde, ac" &
            "countno, mobileno, interest,refno,staffid) values (@username, @institution, @amount, @fi, @status, @" &
            "dateapplied, @timeapplied, @modde, @accountno, @mobileno, @interest,@refno,@staffid)", conn)
                        'NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO SalaryLoan(username, Institution, Amount, FI, Status,DateApplied,TimeApplied,StaffID) VALUES (@username, @Institution, @Amount, @FI, @Status,@DateApplied,@TimeApplied,@StaffID)", conn);
                        Dim refno As String = GenNewRefNo("salaryloan", "id").ToString
                        Dim intrate As Double = Convert.ToDouble((Decimal.Parse(interest) / (100 * Decimal.Parse(values.amount))))
                        'Double intrate = 0;
                        'passing parameters to query
                        cmd.Parameters.AddWithValue("@staffid", username)
                        cmd.Parameters.AddWithValue("@username", username)
                        cmd.Parameters.AddWithValue("@institution", sd.institution)
                        cmd.Parameters.AddWithValue("@amount", Convert.ToDouble(values.amount))
                        cmd.Parameters.AddWithValue("@fi", "unassigned")
                        cmd.Parameters.AddWithValue("@status", "1000")
                        cmd.Parameters.AddWithValue("@dateapplied", DateTime.Now)
                        cmd.Parameters.AddWithValue("@timeapplied", DateTime.Now)
                        cmd.Parameters.AddWithValue("@modde", Convert.ToInt16(values.mode))
                        cmd.Parameters.AddWithValue("@accountno", sd.accountno)
                        cmd.Parameters.AddWithValue("@mobileno", sd.mobile)
                        cmd.Parameters.AddWithValue("@interest", intrate)
                        cmd.Parameters.AddWithValue("@refno", refno)
                        cmd.ExecuteNonQuery()
                        conn.Close()
                        response = refno

                        Dim rval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("refno", refno)
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)

                    End If

                Catch ex As Exception

                    SavetoLog(("LoanApplication" + ex.Message))

                    Message.Message = "Internal Error"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))
                    ' response = "10000"

                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function

    <HttpGet("application/json")>
    <Route("/mobileapi/affordability")>
    Public Function MobAffordability(<FromBody()> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then
                Dim username As String

                username = GetJWTUsername(myToken)

                If username = "" Or username Is Nothing Then
                    Message.Message = "Unable to Retrieve Username from token"
                    Message.Payload = "Unable to Retrieve Username from token"
                    Return BadRequest(ReturnJSONResults(Message))
                End If


                Try

                    Dim availableloan As String
                    Dim interest As String = InterestRate()
                    Dim Limit As String = LoanLimit()
                    Dim owing As String = CheckOwing(username)
                    Dim check As String = CheckApplication(username)
                    Dim sd As StructStaffDetails = StaffDetails(username)
                    Dim ll As Decimal = (Decimal.Parse(Limit) * Decimal.Parse("0.01"))
                    If (check = "") Then
                        check = "0.00"
                    End If

                    Dim old As Decimal = Decimal.Parse(check)
                    Dim net As Decimal = Decimal.Parse(sd.netsalary)
                    availableloan = ((ll * net) _
                                - old).ToString
                    Dim afford As clsAffordabilty = New clsAffordabilty
                    '     string staffID, Name, Institution, AccountNo, Mobile, NetSalary;
                    afford.affordabilityamount = availableloan
                    afford.defaulted = CheckDefaults(username)

                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForAffordabilityResponse(afford)
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)


                Catch ex As Exception
                    SavetoLog(("Affordability Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get Affordability"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function

    <HttpGet("application/json")>
    <Route("/mobileapi/interestrate")>
    Public Function MobInterestrate(<FromBody()> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try

                    Dim interest As String = InterestRate()

                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("interestrate", interest)
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)


                Catch ex As Exception
                    SavetoLog(("MobInterestrate Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get MobInterestrate"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function

    <HttpGet("application/json")>
    <Route("/mobileapi/loanlimit")>
    Public Function MobLoanLimit(<FromBody()> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try

                    Dim ll As String = LoanLimit()

                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("loanlimit", ll)
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)


                Catch ex As Exception
                    SavetoLog(("MobLoanLimit Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get MobLoanLimit"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function

    <HttpGet("application/json")>
    <Route("/mobileapi/amountpayable")>
    Public Function MobAmountPayable(<FromQuery> ByVal values As MobileAmountpayableReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            If values.amount Is Nothing Then
                Message.Message = "Amount Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.amount = "" Then
                Message.Message = "Amount Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    Dim ap As String
                    Dim interest As Double = InterestRate()
                    ap = ((Decimal.Parse(values.amount) * (Decimal.Parse(interest) * Decimal.Parse("0.01"))) + Decimal.Parse(values.amount)).ToString()


                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("amountpayable", ap)
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)


                Catch ex As Exception
                    SavetoLog(("MobAmountPayable Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get MobAmountPayable"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function
    <HttpPost("application/json")>
    <Route("/mobileapi/paydayloansignup")>
    Public Function MobPayDayLoan(<FromBody()> ByVal values As MobilePaydayloanReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values
            Dim Message As New GeneralResponds

            If values.appversion Is Nothing Then
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then

                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            ElseIf values.mobileno Is Nothing Then
                Message.Message = "MobileNo Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.mobileno = "" Then
                Message.Message = "MobileNo Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.email Is Nothing Then
                Message.Message = "Email Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.email = "" Then
                Message.Message = "Email Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))

            ElseIf values.idnumber Is Nothing Then
                Message.Message = "IDNO Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.idnumber = "" Then
                Message.Message = "IDNO Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If


            If CheckAppVersion(values.appversion) = True Then

                If CheckPaydayDuplicate(values.mobileno, values.username, values.email, values.idnumber) = False Then
                    Try
                        Dim sec As New Security
                        Dim userGuid As Guid = System.Guid.NewGuid
                        ' Hash the password together with our unique userGuid
                        Dim hashedPassword As String = sec.HashSHA1((values.password + userGuid.ToString))
                        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                        conn.Open()
                        Dim cmd As NpgsqlCommand = New NpgsqlCommand("insert into staffs(name, gender, dob, mobileno, email, companyname, location, netsalary, ssnitno, sal" &
        "aryaccountno, bankname, branch, accountno, username, password, userguid, status, registeredstatus,id" &
        "card,picture,signature,idtype,idno,staffid) values (@name, @gender, @dob, @mobileno, @email, @compan" &
        "yname, @location, @netsalary, @ssnitno, @salaryaccountno, @bankname, @branch, @accountno, @username," &
        " @password, @userguid, @status, @registeredstatus,@idcard, @picture, @signature,@idtype,@idno,@staff" &
        "id)", conn)
                        'passing parameters to query
                        With values
                            cmd.Parameters.AddWithValue("@staffid", .username)
                            cmd.Parameters.AddWithValue("@name", .name)
                            cmd.Parameters.AddWithValue("@gender", .gender)
                            cmd.Parameters.AddWithValue("@dob", .dateofbirth)
                            cmd.Parameters.AddWithValue("@mobileno", .mobileno)
                            cmd.Parameters.AddWithValue("@email", .email)
                            cmd.Parameters.AddWithValue("@companyname", .companyname)
                            cmd.Parameters.AddWithValue("@location", .location)
                            cmd.Parameters.AddWithValue("@netsalary", .netsalary)
                            cmd.Parameters.AddWithValue("@idtype", .idtype)
                            cmd.Parameters.AddWithValue("@idno", .idnumber)
                            cmd.Parameters.AddWithValue("@ssnitno", .ssnitno)
                            cmd.Parameters.AddWithValue("@salaryaccountno", .salaryaccountno)
                            cmd.Parameters.AddWithValue("@bankname", .bankname)
                            cmd.Parameters.AddWithValue("@branch", .branch)
                            cmd.Parameters.AddWithValue("@accountno", .accountno)
                            cmd.Parameters.AddWithValue("@username", .username)
                            cmd.Parameters.AddWithValue("@password", hashedPassword)
                            cmd.Parameters.AddWithValue("@userguid", userGuid)
                            cmd.Parameters.AddWithValue("@status", False)
                            cmd.Parameters.AddWithValue("@registeredstatus", False)
                            If Not DBNull.Value.Equals(.idcard) Then
                                cmd.Parameters.AddWithValue("@idcard", .idcard)
                            Else
                                cmd.Parameters.AddWithValue("@idcard", DBNull.Value)
                            End If

                            If Not DBNull.Value.Equals(.picture) Then
                                cmd.Parameters.AddWithValue("@picture", .picture)
                            Else
                                cmd.Parameters.AddWithValue("@picture", DBNull.Value)
                            End If

                            If Not DBNull.Value.Equals(.signature) Then
                                cmd.Parameters.AddWithValue("@signature", .signature)
                            Else
                                cmd.Parameters.AddWithValue("@signature", DBNull.Value)
                            End If

                        End With         '  cmd.Parameters.AddWithValue("@refno",GenNewRefNo("Staffs", "id").ToString());
                        cmd.ExecuteNonQuery()
                        conn.Close()
                        ' response = "0"
                        'Generate the Verification token for Mobile number verfication
                        If (GenerateRegistrationOTP(values.mobileno, values.username) = True) Then

                            Dim rval As String

                            Message.Message = "Successful"
                            Message.Payload = JsonForGeneralResponse("status", "true")
                            rval = ReturnJSONResults(Message)

                            Return Ok(rval)

                        Else
                            Message.Message = "Account Created Unable to Send OTP"
                            Message.Payload = "Internal Error "
                            Return BadRequest(ReturnJSONResults(Message))

                        End If

                    Catch ex As Exception


                        'response = "10000"
                        'response = (response + ex.Message)
                        SavetoLog(("PaydayLoan Exception: " + ex.Message))

                        Message.Message = "Error Processing Request"
                        Message.Payload = "Internal Error "
                        Return BadRequest(ReturnJSONResults(Message))

                    End Try
                Else
                    Message.Message = "Duplicate Entry.User Already Exist"
                    Message.Payload = "User Already Exist"
                    Return BadRequest(ReturnJSONResults(Message))
                End If
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function

    <HttpGet("application/json")>
    <Route("/mobileapi/transactions")>
    Public Function MobTransactions(<FromQuery> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand("select left(dateapplied::varchar, 10) as dateapplied , sl.status, sl.amount, sl.interest, (sl.amount " &
    "+ sl.interest)as total, (case when sl.status = '1000' then  'loan applied' when sl.status = '2000' o" &
    "r sl.status = '3000' or sl.status = '4000' or sl.status = '5000' or sl.status = '6000' or sl.status " &
    "= '7000' then  'loan disbursed' when sl.status = '8000' or sl.status = '9000' or sl.status = '10000'" &
    " then  'loan repaid' else 'me' end) as state from salaryloan sl left join statuses s on sl.status = " &
    "s.code where sl.username = @username order by sl.id desc", conn)
                    conn.Open()
                    cmd.Parameters.AddWithValue("@username", jwtv.username)
                    Dim dr As NpgsqlDataReader = cmd.ExecuteReader
                    Dim dt As DataTable = New DataTable("transactions")
                    dt.Load(dr)
                    ' Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer
                    'Dim rows As List(Of Dictionary) = New List(Of Dictionary)
                    'Dim row As Dictionary(Of String, Object)
                    'For Each drr As DataRow In dt.Rows
                    '    row = New Dictionary(Of String, Object)
                    '    For Each col As DataColumn In dt.Columns
                    '        row.Add(col.ColumnName, drr(col))
                    '    Next
                    '    rows.Add(row)
                    'Next
                    'while (dr.Read())
                    '{
                    '    response = dr["Value"].ToString();
                    '}
                    dr.Close()
                    conn.Close()

                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForDatatable(dt)
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                Catch ex As Exception
                    SavetoLog(("MobTransactions Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get MobTransactions"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function

    <HttpPost("application/json")>
    <Route("/mobileapi/postmandate")>
    Public Function MobPostMandate(<FromBody()> ByVal values As MobilePostMandateReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds
            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)


            If jwtv.appversion Is Nothing Then
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf jwtv.appversion = "" Then

                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            ElseIf values.referenceno Is Nothing Then
                Message.Message = "Reference Number Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.referenceno = "" Then
                Message.Message = "Reference Number Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.effectdate Is Nothing Then
                Message.Message = "Effect Date Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.effectdate = "" Then
                Message.Message = "Effect Date Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))

            ElseIf values.transdate Is Nothing Then
                Message.Message = "Transaction Date Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.transdate = "" Then
                Message.Message = "Transaction Date Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If


            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    conn.Open()
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand("UPDATE Mandate set approvalstatus=@approvalstatus,effectdate=@effectdate,transdate=@transdate where m" &
                        "andaterefno=@mandaterefno and username=@username", conn)
                    'passing parameters to query
                    cmd.Parameters.AddWithValue("@approvalstatus", "True")
                    ' string r = "08/05/2015";
                    Dim culture As IFormatProvider = New CultureInfo("en-US", True)
                    Dim t1 As DateTime = DateTime.ParseExact(values.effectdate, "dd-MM-yyyy", culture)
                    cmd.Parameters.AddWithValue("@effectdate", t1)
                    ' cmd.Parameters.AddWithValue("@effectdate", DateTime.Now);
                    Dim t2 As DateTime = DateTime.ParseExact(values.transdate, "dd-MM-yyyy", culture)
                    cmd.Parameters.AddWithValue("@transdate", t2)
                    cmd.Parameters.AddWithValue("@username", jwtv.username)
                    cmd.Parameters.AddWithValue("@mandaterefno", values.referenceno)
                    cmd.ExecuteNonQuery()
                    conn.Close()
                    ' response = "0";
                    '  SavetoLog("FROM POSTMANDATE:Return Value= 0 | Successful");
                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("status", "true")
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                Catch ex As Exception
                    'throw ex;
                    '  response = "Unable to Set Token Expiration";
                    SavetoLog(("FROM POSTMANDATE: " + ("appversion=" _
                                    + (jwtv.appversion + (" ,username = " _
                                    + (jwtv.username + (", token = " _
                                    + (token + (" , referenceno = " _
                                    + (values.referenceno + (",effectdate = " _
                                    + (values.effectdate + (",Transdate= " + values.transdate)))))))))))))
                    SavetoLog(("FROM POSTMANDATE:Return Value= 1 | Error Updating Record" + (",  ERROR= " + ex.Message.ToString)))
                    Message.Message = "Error Unable to Get Update Mandate"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))
                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function
    <HttpPost("application/json")>
    <Route("/mobileapi/validateotp")>
    Public Function MobValidateOTP(<FromBody()> ByVal values As MobileValidateOTPReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds


            If values.otp Is Nothing Then
                Message.Message = "OTP Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.otp = "" Then

                Message.Message = "OTP Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion Is Nothing Then
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then

                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If


            If CheckAppVersion(values.appversion) = True Then

                Try
                    Dim vr As New StructValidateOTPResults
                    vr = _ValidateOTP(values.otp)

                    If (vr.status = True) Then

                        Dim rval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForValidteOTPResponse(vr)
                        rval = ReturnJSONResults(Message)
                        Return Ok(rval)


                    Else
                        Message.Message = "Error Validating OTP"
                        Message.Payload = "Tranaction Processing Failed "
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                Catch ex As Exception
                    'throw ex;
                    SavetoLog(("FROM MobValidateOTP  ERROR= " + ex.Message.ToString))
                    Message.Message = "Error Validating OTP"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))
                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function

    <HttpPost("application/json")>
    <Route("/mobileapi/validateregistrationotp")>
    Public Function MobValidateRegistrationOTP(<FromBody()> ByVal values As MobileValidateRegistrationOTPReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds


            If values.otp Is Nothing Then
                Message.Message = "OTP Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.otp = "" Then

                Message.Message = "OTP Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion Is Nothing Then
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then

                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.username Is Nothing Then

                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.username = "" Then


                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))


            End If


            If CheckAppVersion(values.appversion) = True Then

                Try
                    If (_ValidateRegistrationOTP(values.otp) = True) Then
                        Dim rval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("status", "true")
                        rval = ReturnJSONResults(Message)
                        Return Ok(rval)
                    Else

                        Message.Message = "Error Validating OTP"
                        Message.Payload = "Tranaction Processing Failed "

                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                Catch ex As Exception
                    'throw ex;
                    '  response = "Unable to Set Token Expiration";
                    SavetoLog("MobValidateRegistrationOTP Failed with Error: " & ex.Message)
                    Message.Message = "Error Validating OTP"
                    Message.Payload = "Tranaction Processing Failed "

                    Return BadRequest(ReturnJSONResults(Message))
                    '  Return "1|Error Validating OTP"
                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function
    <HttpGet("application/json")>
    <Route("/mobileapi/getnewmandate")>
    Public Function MobTGetNewMandate(<FromQuery> ByVal values As MobileGetNewMandateReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)



            If values.referenceno Is Nothing Then
                Message.Message = "Reference Number Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.referenceno = "" Then

                Message.Message = "Reference Number Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If


            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    '  Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    'NpgsqlCommand cmd = new NpgsqlCommand("SELECT LEFT(CONVERT(VARCHAR, SL.DateApplied, 120), 10) As DateApplied , SL.Status, SL.Amount, SL.Interest, (SL.Amount + SL.Interest)As Total, (case when SL.Status = '1000' then  'LOAN APPLIED' when SL.Status = '2000' OR SL.Status = '3000' OR SL.Status = '4000' OR SL.Status = '5000' OR SL.Status = '6000' OR SL.Status = '7000' then  'LOAN DISBURSED' when SL.Status = '8000' OR SL.Status = '9000' OR SL.Status = '10000' then  'LOAN REPAID' else 'me' end) AS State FROM SalaryLoan SL LEFT JOIN Statuses S ON SL.Status = S.Code WHERE SL.StaffID = @StaffID ORDER BY SL.id DESC", conn);
                    ' NpgsqlCommand cmd = new NpgsqlCommand("SELECT LEFT(CONVERT(VARCHAR, SL.DateApplied, 120), 10) As DateApplied , SL.Status, SL.Amount, SL.Interest, (SL.Amount + SL.Interest)As Total, (case when SL.Status = '1000' then  'LOAN APPLIED' when SL.Status = '2000' OR SL.Status = '3000' OR SL.Status = '4000' OR SL.Status = '5000' OR SL.Status = '6000' OR SL.Status = '7000' then  'LOAN DISBURSED' when SL.Status = '8000' OR SL.Status = '9000' OR SL.Status = '10000' then  'LOAN REPAID' else 'me' end) AS State FROM SalaryLoan SL LEFT JOIN Statuses S ON SL.Status = S.Code WHERE SL.username = @username ORDER BY SL.id DESC", conn);
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from getmandate where username = @username and refno=@refno", conn)
                    conn.Open()
                    cmd.Parameters.AddWithValue("@username", jwtv.username)
                    cmd.Parameters.AddWithValue("@refno", values.referenceno)
                    Dim dr As NpgsqlDataReader = cmd.ExecuteReader
                    Dim myMandate As structMandate = New structMandate
                    Dim rval() As Byte = Nothing

                    While dr.Read

                        myMandate.Accountname = dr("name").ToString
                        myMandate.Bank = dr("bankname").ToString
                        myMandate.Accountnumber = dr("accountno").ToString
                        myMandate.Amount = dr("amount").ToString
                        myMandate.refno = dr("refno").ToString
                        myMandate.CustomerName = dr("name").ToString
                        myMandate.CustomerBankBranch = dr("branch").ToString
                        myMandate.Amounttorepay = (Decimal.Parse(dr("interest").ToString) + Decimal.Parse(dr("amount").ToString)).ToString
                        'amounttorepay
                        If Not DBNull.Value.Equals(dr("signature")) Then
                            'not null  
                            rval = CType(dr("signature"), Byte())
                            myMandate.signature = Convert.ToBase64String(rval)
                        Else
                            myMandate.signature = ""
                        End If

                        'myMandate.effectdate = dr["effectdate"].ToString();
                        ' myMandate.transdate = dr["transdate"].ToString();
                        myMandate.mandaterefno = dr("refno").ToString
                        Dim mymandatecredit As structMandateCredit = New structMandateCredit
                        mymandatecredit = GetMandateCredit()
                        myMandate.creditacctname = mymandatecredit.creditacctname
                        myMandate.creditacctno = mymandatecredit.creditacctno
                        myMandate.creditbank = mymandatecredit.creditbank
                        myMandate.creditbankbranch = mymandatecredit.creditbankbranch

                    End While

                    dr.Close()
                    conn.Close()
                    'Save the mandate but check duplicate before
                    If (CheckDuplicateMandate(jwtv.username, myMandate.mandaterefno) = False) Then
                        If (SaveMandate(myMandate, jwtv.username, rval) = True) Then

                            Dim rrval As String

                            Message.Message = "Successful"
                            Message.Payload = JsonForNewMandateResponse(myMandate)
                            rrval = ReturnJSONResults(Message)
                            Return Ok(rrval)

                        Else
                            Message.Message = "Error Getting Mandate"
                            Message.Payload = "Unable to Process Request"
                            Return BadRequest(ReturnJSONResults(Message))
                            'Return "1000|Error Getting Mandate"
                        End If
                    Else

                        Dim rrval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForNewMandateResponse(myMandate)
                        rrval = ReturnJSONResults(Message)
                        Return Ok(rrval)

                    End If

                    ' Return serializer.Serialize(myMandate)
                Catch ex As Exception
                    SavetoLog(("GetMandate: " + ex.Message))
                    Message.Message = "Error Getting Mandate"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))
                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function
    <HttpPost("application/json")>
    <Route("/mobileapi/postidcard")>
    Public Function MobPostIDCard(<FromBody()> ByVal values As MobilePostImagesReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds


            If values.username Is Nothing Then
                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.username = "" Then

                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.idcard Is Nothing Then
                Message.Message = "ID Image Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.idcard.Length <= 0 Then

                Message.Message = "ID Image Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion Is Nothing Then

                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then


                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))


            End If


            If CheckAppVersion(values.appversion) = True Then

                Try
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    conn.Open()
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand("update staffs set idcard=@idcard where username=@username", conn)
                    'passing parameters to query
                    cmd.Parameters.AddWithValue("@username", values.username)
                    cmd.Parameters.AddWithValue("@idcard", values.idcard)
                    cmd.ExecuteNonQuery()
                    conn.Close()
                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("status", "true")
                    rval = ReturnJSONResults(Message)
                    Return Ok(rval)

                Catch ex As Exception
                    SavetoLog(("MobPostIDCard Failed With error : " + ex.Message))
                    Message.Message = "Error Saving IDCard "
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function

    <HttpPost("application/json")>
    <Route("/mobileapi/postsignature")>
    Public Function MobPostSignature(<FromBody()> ByVal values As MobilePostImagesReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds


            If values.username Is Nothing Then
                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.username = "" Then

                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.signature Is Nothing Then
                Message.Message = "Signature Image Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.signature.Length <= 0 Then

                Message.Message = "Signature Image Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion Is Nothing Then

                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then


                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))


            End If


            If CheckAppVersion(values.appversion) = True Then

                Try
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    conn.Open()
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand("update staffs set signature=@signature where username=@username", conn)
                    'passing parameters to query
                    cmd.Parameters.AddWithValue("@username", values.username)
                    cmd.Parameters.AddWithValue("@signature", values.signature)
                    cmd.ExecuteNonQuery()
                    conn.Close()
                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("status", "true")
                    rval = ReturnJSONResults(Message)
                    Return Ok(rval)

                Catch ex As Exception
                    SavetoLog(("PostSignature Failed With error : " + ex.Message))
                    Message.Message = "Error Saving Signature "
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function
    <HttpPost("application/json")>
    <Route("/mobileapi/passphoto")>
    Public Function MobPassPhoto(<FromBody()> ByVal values As MobilePostImagesReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds


            If values.username Is Nothing Then
                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.username = "" Then

                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.passphoto Is Nothing Then
                Message.Message = "Passport Image Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.passphoto.Length <= 0 Then

                Message.Message = "Passport Image Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion Is Nothing Then

                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then


                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))

            End If


            If CheckAppVersion(values.appversion) = True Then

                Try
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    conn.Open()
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand("update staffs set picture=@passphoto where username=@username", conn)
                    'passing parameters to query
                    cmd.Parameters.AddWithValue("@username", values.username)
                    cmd.Parameters.AddWithValue("@passphoto", values.passphoto)
                    cmd.ExecuteNonQuery()
                    conn.Close()
                    Dim rval As String

                    Message.Message = "Successful"
                    Message.Payload = JsonForGeneralResponse("status", "true")
                    rval = ReturnJSONResults(Message)
                    Return Ok(rval)

                Catch ex As Exception
                    SavetoLog(("MobPassPhoto Failed With error : " + ex.Message))
                    Message.Message = "Error Saving Signature "
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function
    <HttpGet("application/json")>
    <Route("/mobileapi/getidcard")>
    Public Function MobGetIDCard(<FromQuery> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    Dim rval() As Byte = Nothing
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select idcard from staffs where username = '" _
                + (jwtv.username + "'")), conn)
                    conn.Open()
                    Dim dr As NpgsqlDataReader = cmd.ExecuteReader

                    While dr.Read
                        ' byte[] bytearray = (byte[])row["test"];
                        rval = CType(dr("idcard"), Byte())

                    End While

                    dr.Close()
                    conn.Close()

                    If (rval Is Nothing) Then
                        Message.Message = "No Image Available"
                        Message.Payload = "No Image Available"
                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rrval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("idcard", Convert.ToBase64String(rval))
                        rrval = ReturnJSONResults(Message)

                        Return Ok(rrval)
                    End If



                Catch ex As Exception
                    SavetoLog(("MobGetIDCard Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get MobGetIDCard"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function
    <HttpGet("application/json")>
    <Route("/mobileapi/getsignature")>
    Public Function MobGetSignature(<FromQuery> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    Dim rval() As Byte = Nothing
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select signature from staffs where username = '" _
                + (jwtv.username + "'")), conn)
                    conn.Open()
                    Dim dr As NpgsqlDataReader = cmd.ExecuteReader

                    While dr.Read
                        ' byte[] bytearray = (byte[])row["test"];
                        rval = CType(dr("signature"), Byte())

                    End While

                    dr.Close()
                    conn.Close()

                    If (rval Is Nothing) Then
                        Message.Message = "No Image Available"
                        Message.Payload = "No Image Available"
                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rrval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("signature", Convert.ToBase64String(rval))
                        rrval = ReturnJSONResults(Message)

                        Return Ok(rrval)
                    End If



                Catch ex As Exception
                    SavetoLog(("GetSignature Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get GetSignature"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function

    <HttpGet("application/json")>
    <Route("/mobileapi/getpassphoto")>
    Public Function MobGetPassPhoto(<FromQuery> ByVal values As MobileGenRequest) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            'validate returned values  
            Dim Message As New GeneralResponds

            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            End If

            Dim jwtv As New StructJWTValues
            jwtv = GetJWTValues(myToken)

            If CheckAppVersion(jwtv.appversion) = True Then

                Try
                    Dim rval() As Byte = Nothing
                    Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
                    Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select picture from staffs where username = '" _
                + (jwtv.username + "'")), conn)
                    conn.Open()
                    Dim dr As NpgsqlDataReader = cmd.ExecuteReader

                    While dr.Read
                        ' byte[] bytearray = (byte[])row["test"];
                        rval = CType(dr("picture"), Byte())

                    End While

                    dr.Close()
                    conn.Close()

                    If (rval Is Nothing) Then
                        Message.Message = "No Image Available"
                        Message.Payload = "No Image Available"
                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rrval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("passphoto", Convert.ToBase64String(rval))
                        rrval = ReturnJSONResults(Message)

                        Return Ok(rrval)
                    End If



                Catch ex As Exception
                    SavetoLog(("MobGetPassPhoto Failed With : " + ex.Message))
                    Message.Message = "Error Unable to Get MobGetPassPhoto"
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try
            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If

        End If
    End Function

    <HttpPost("application/json")>
    <Route("/mobileapi/resendregistrationotp")>
    Public Function MobResendRegOTP(<FromBody()> ByVal values As MobileValidateRegistrationOTPReq) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else
            'validate returned values
            Dim Message As New GeneralResponds


            If values.username Is Nothing Then
                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.username = "" Then

                Message.Message = "Username Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.mobileno Is Nothing Then
                Message.Message = "Mobile NUmber Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.mobileno.Length <= 0 Then

                Message.Message = "Mobile NUmber Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion Is Nothing Then

                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))
            ElseIf values.appversion = "" Then

                Message.Message = "App Version Required"
                Message.Payload = "Field Required"
                Return BadRequest(ReturnJSONResults(Message))


            End If


            If CheckAppVersion(values.appversion) = True Then

                Try

                    If (GenerateRegistrationOTP(values.mobileno, values.username) = True) Then

                        Dim rval As String

                        Message.Message = "Successful"
                        Message.Payload = JsonForGeneralResponse("status", "true")
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)

                    Else
                        Message.Message = "Resending OTP Failed"
                        Message.Payload = "Internal Error "
                        Return BadRequest(ReturnJSONResults(Message))

                    End If

                Catch ex As Exception
                    SavetoLog(("MobResendRegOTP Failed With error : " + ex.Message))
                    Message.Message = "Error Resending OTP  "
                    Message.Payload = "Internal Error"
                    Return BadRequest(ReturnJSONResults(Message))

                End Try

            Else
                Message.Message = "Invalid Mobile Version"
                Message.Payload = "Invalid Mobile Version"
                Return BadRequest(ReturnJSONResults(Message))

            End If
        End If
    End Function
End Class
