﻿Public Class Staffs
    Public Property id As Integer
    Public Property StaffID As String
    Public Property Name As String
    Public Property MobileNo As String
    Public Property IsnID As String
    Public Property Password As String
    Public Property UserGuid As Date
    Public Property DateRegistered As String
    Public Property Status As Boolean
    Public Property RegisteredBy As String

    Public Property FirstLogin As Boolean
    Public Property Locked As Boolean
    Public Property Role As String
    Public Property AccountNo As String
    Public Property DOB As String
    Public Property NetSalary As String
    Public Property Bank As String
    Public Property IDType As String
    Public Property IDNO As String
    Public Property Picture As Byte()
    Public Property Code As String
    Public Property CodeTime As String
    Public Property Email As String
    Public Property PassCode As String
    Public Property Mandate As Byte()
    Public Property UserName As String
    Public Property IDCard As Byte()

    Public Property CardStatus As String
    Public Property RegisteredStatus As String
    Public Property Gender As String
    Public Property CompanyName As String
    Public Property Location As String
    Public Property SSNITNo As String
    Public Property SalaryAccountNo As String
    Public Property Branch As String

    Public Property BankName As String
    Public Property Signature As Byte()

End Class
