﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class InsTransaction
    Inherits ControllerBase

    <HttpGet>
    <Route("/institution/transactions")>
    Public Function GetInstTransactions(<FromQuery()> ByVal values As InsTransactions) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            If values.insid Is Nothing Or values.insid.Length = 0 Then
                Message.Message = "No  Institution Number Provided"
                Message.Payload = " Institution Number Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If



            Try
                'validate token
                '   Dim status As String = "4000"
                ' Dim ttime As String = GetConfigurator(4)

                sSQL = "SELECT SL.*, (S.Status)AS State, count(*) OVER() AS full_count FROM SalaryLoan SL LEFT JOIN Statuses S ON SL.Status = S.Code WHERE SL.Institution ='" & values.insid & "'"


                Dim hasmore As Boolean = False


                If values.status IsNot Nothing Then
                    If values.status.Length > 0 Then
                        ' '%\_%'
                        sSQL = sSQL & " And SL.Status ilike '%" & values.status & "%' "
                        hasmore = True
                    End If

                End If



                If values.pagination = True Then

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY SL.id DESC LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If



                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "institution Transaction Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get GetInstTransactions" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function
End Class
