﻿Public Class Banks
    Public Property id As String
    Public Property BankNo As String
    Public Property BankName As String
    Public Property AccountNo As String
    Public Property Details As String
    Public Property AccountName As String
    Public Property DateCreated As Date
    Public Property CreatedBy As String
    Public Property Status As Boolean
    Public Property Token As String
End Class
