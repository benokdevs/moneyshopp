﻿Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class InsStaff
    Inherits ControllerBase
    <HttpGet>
    <Route("/institution/borrower/staffs")>
    Public Function GetInsStaff(<FromQuery()> ByVal qval As InsStaffFilter) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)


                'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
                sSQL = "SELECT id, Name, StaffID, INSID, MobileNo, DateRegistered, Status, RegisteredBy, FirstLogin, Locked, AccountNo, NetSalary, Bank , count(*) OVER() AS full_count FROM Staffs where InsID=@InsID AND Status = 'True'"


                If qval.pagination = False Then
                    ' sSQL = "Select * From FIs"
                Else
                    Dim pagenum As Integer
                    pagenum = qval.pagesize * (qval.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qval.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetInstStaffTableData(sSQL, qval.insid)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("""", "]")
                    Message.Message = "Institution Staffs Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function


    <HttpPost>
    <Route("/institution/borrower/staffssearch")>
    Public Function InsStaffFiltered(<FromQuery()> ByVal qvals As GetRequestFormat, <FromBody()> ByVal values As UploadStaffs) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds



            Dim myToken As String
            myToken = HttpContext.Request.Headers("token").ToString()


            If VerifyJWT(myToken) = False Then
                Message.Message = "Invalid Token/Expired Token"
                Message.Payload = "Invalid Token/Expired Token"
                Return BadRequest(ReturnJSONResults(Message))
            Else

                Try
                    'validate token

                    If values.InsID Is Nothing Then
                        Message.Message = "Institution Id Required"
                        Message.Payload = "Institution Id Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    ElseIf values.InsID.Length <= 0 Then
                        Message.Message = "Institution Id Required"
                        Message.Payload = "Institution Id Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If



                    Dim sSQL As String
                    Dim Dt As DataTable

                    sSQL = "SELECT id, Name, StaffID, INSID, MobileNo, DateRegistered, Status, RegisteredBy, FirstLogin, Locked, AccountNo, NetSalary, Bank, count(*) OVER() AS full_count FROM Staffs where InsID=@InsID "


                    Dim hasmore As Boolean = False

                    ' Public Property id As String
                    'Public Property staffid As String
                    'Public Property Name As String
                    'Public Property DOB As String
                    'Public Property NetSalary As String
                    'Public Property Bank As String
                    'Public Property AccountNo As String
                    'Public Property MobileNo As String
                    'Public Property DateRegistered As DateTime
                    'Public Property RegisteredBy As String
                    'Public Property InsID As String
                    'Public Property Status As String
                    'Public Property FirstLogin As Boolean
                    'Public Property Locked As Boolean
                    'Public Property Role As String

                    If values.Name IsNot Nothing Then
                        If values.Name.Length > 0 Then
                            ' '%\_%'
                            sSQL = sSQL & " or Name ilike '%" & values.Name & "%' "
                            hasmore = True
                        End If

                    End If

                    If values.NetSalary IsNot Nothing Then
                        If values.NetSalary.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "OR NetSalary ilike '%" & values.NetSalary & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " or NetSalary ilike '%" & values.NetSalary & "%' "
                                hasmore = True
                            End If

                        End If
                    End If


                    If values.AccountNo IsNot Nothing Then
                        If values.AccountNo.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "Or AccountNo ilike '%" & values.AccountNo & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " or AccountNo ilike '%" & values.AccountNo & "%' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.Bank IsNot Nothing Then
                        If values.Bank.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "Or Bank ilike '%" & values.Bank & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " or Bank ilike '%" & values.Bank & "%' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.MobileNo IsNot Nothing Then
                        If values.MobileNo.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "Or MobileNo ilike '%" & values.MobileNo & "%' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " or MobileNo ilike '%" & values.MobileNo & "%' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.Status IsNot Nothing Then
                        If values.Status.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "or  Status = '" & values.Status & "' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " or Status = '" & values.Status & "' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If values.Locked IsNot Nothing Then
                        If values.Locked.Length > 0 Then
                            ' '%\_%'
                            If hasmore = True Then
                                sSQL = sSQL & "or  Locked = '" & values.Locked & "' "
                                hasmore = True
                            Else
                                sSQL = sSQL & " or Locked = '" & values.Locked & "' "
                                hasmore = True
                            End If

                        End If
                    End If

                    If qvals.pagination = True Then

                        Dim pagenum As Integer
                        pagenum = qvals.pagesize * (qvals.pagenumber - 1)
                        sSQL = sSQL & " ORDER BY id  LIMIT " & qvals.pagesize.ToString & " OFFSET " & pagenum

                    End If


                    '  sSQL = sSQL & " ORDER BY BankNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"


                    Dt = DBConnection.GetInstStaffTableData(sSQL, values.InsID)


                    If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                        Message.Message = "No records Found "
                        Message.Payload = "No Records Found "
                        Return BadRequest(ReturnJSONResults(Message))

                    Else

                        Dim glob As New Chilkat.Global
                        Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                        'Log("Chilkat License Status : " & succes)


                        Message.Payload = JsonForDatatable(Dt)
                        'Message.Payload = Message.Payload.Replace("""[", "[")
                        'Message.Payload = Message.Payload.Replace("]""", "]")
                        Message.Message = "Institution Staffs Filtered"
                        Dim jsonconvert As New JsonResult(Message)
                        ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If
                Catch ex As Exception

                End Try


                ' Return jsonconvert
            End If

        End If

    End Function
End Class
'SELECT [id], Name, [StaffID], INSID, [MobileNo], [DateRegistered], [Status], [RegisteredBy], [FirstLogin], [Locked], AccountNo, NetSalary, Bank FROM [Staffs] where InsID=@InsID AND Status = 'True' ORDER BY [id]