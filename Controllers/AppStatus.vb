﻿

Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class AppStatus
    Inherits ControllerBase

    <HttpGet>
    <Route("/")>
    Public Function Status() As ActionResult

        SavetoLog("Server Started!!")
        Return Ok("Server IS UP !!!!")
    End Function
End Class
