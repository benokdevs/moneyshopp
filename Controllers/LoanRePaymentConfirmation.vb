﻿

Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class LoanRePaymentConfirmation
    Inherits ControllerBase

    <HttpPost>
    <Route("/admin/loans/loanrepaymentconf")>
    Public Function LoanRePaymentConfirmation(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'process body json

                    ' Go ahead to Update Record

                    Message = ProcessLoanRePaymentConfirmation(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    Public Function ProcessLoanRePaymentConfirmation(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token



            ' This is the above JSON with whitespace chars removed (SPACE, TAB, CR, and LF chars).
            ' The presence of whitespace chars for pretty-printing makes no difference to the Load
            ' method. 

            '  Dim jsonStr As String = "{""employees"":[{""firstName"":""John"", ""lastName"":""Doe""},{""firstName"":""Anna"", ""lastName"":""Smith""},{""firstName"":""Peter"",""lastName"":""Jones""}]}"

            '
            '{

            '"applicants": [
            '{
            '"id": 30,
            '"CompanyName": "Fan milk",
            '"Amount": "2000",
            '"BankName": "Ecobank"

            '},
            '{
            '"id": 45,
            '"CompanyName": "Benok",
            '"Amount": "2000",
            '"BankName": "Barclays",
            '}
            ']
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessLoanRePaymentConfirmation : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "employees" array.
            Dim applicants As Chilkat.JsonArray = json.ArrayOf("applicants")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessLoanRePaymentConfirmation : " & "No applicants Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            '  Dim InstID As String = json.StringOf("institution_id")

            ' Iterate over each employee, getting the JSON object at each index.
            Dim numApplicants As Integer = applicants.Size
            Dim i As Integer = 0
            While i < numApplicants

                Dim appObj As Chilkat.JsonObject = applicants.ObjectAt(i)

                Dim ssql As String
                Dim status As String
                Dim id As Integer


                status = appObj.StringOf("status")

                id = Integer.Parse(appObj.StringOf("id"))

                ssql = "update SalaryLoan set Status='8000', RepaidConfirmedDate=@RepaidConfirmedDate, RepaidConfirmedBy=@RepaidConfirmedBy WHERE ID=" & appObj.StringOf("id")
                ' ssql = "update Staffs set RegisteredStatus='" & status & "', ApprovedBy='" & username & "', ApprovedDate = '" & DateTime.Now & "', InsID = '" & InstID & "' WHERE ID=" & id

                Message.mError = Not DBConnection.LoanRePaymentConfirmation(ssql, username, appObj.StringOf("id"), appObj.StringOf("CompanyName"), appObj.StringOf("Amount"), appObj.StringOf("BankName"))
                ' Debug.WriteLine("employee[" & i & "] = " & appObj.StringOf("id") & " " & appObj.StringOf("status"))
                'update the individual records

                i = i + 1
            End While


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessPaydayLoansApproval : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function


    <HttpGet>
    <Route("/admin/loans/loanrepaymentconf")>
    Public Function GetLoanRePaymentConfirmation(<FromQuery()> ByVal qval As InstLoanApprFilters) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)


                sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, (SL.Amount + Interest)As Total, SL.FI, SL.Status, SL.DateAcknowledged, B.BankName, F.CompanyName, (I.Name)AS IName, S.Name, count(*) OVER() AS full_count
 FROM SalaryLoan SL
 Left Join DisbursementMode DM
 On SL.Modde = DM.id
 Left Join Banks B
ON SL.RepaymentBankID = B.BankNo
 Left Join FIs F
ON SL.FI = F.CompanyNo
Left Join Institutions I
ON SL.Institution = I.InsNo
Left Join Staffs S
On SL.StaffID = S.StaffID
  where SL.Status = '7000'"



                If qval.pagination = False Then
                    sSQL = sSQL & " ORDER BY id"
                Else
                    Dim pagenum As Integer
                    pagenum = qval.pagesize * (qval.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qval.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Loan RePayment Confirmation  Records Completed Successfully"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

End Class
