﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class BorrowerUploadStaff
    Inherits ControllerBase


    <HttpPost>
    <Route("/institution/borrower/uploadstaff")>
    Public Function AddNewUser(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try
                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If



                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    Message = ProcessUploadStaff(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If




            Catch ex As Exception

            End Try



        End If

    End Function

    Public Function ProcessUploadStaff(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Dim Dr As DataRow
        Dim sSQL As String
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token


            '{
            '    "RoleName": "Admin",
            '    "Description": "Test Admin",
            '    "staffs": [
            '        {
            '            "staffid": 1166,
            '            "Name": "True",
            '            "DOB":"AddFIs",
            '            "NetSalary" 1166,
            '            "Bank": "True",
            '            "AccountNo":"AddFIs",
            '            "MobileNo": 1166,
            '            "InsID": "True",
            '            "Status":"AddFIs",
            '            "Role" 1166,
            '           


            '        },
            '        {
            '            "id": 1167,
            '            "status": "False",
            '            "permissionname":"AddStaffs"
            '        }
            '    ]
            '}


            '              cmd.Parameters.AddWithValue("@StaffID", Convert.ToString(dr1["StaffID"]));
            '              cmd.Parameters.AddWithValue("@Name", Convert.ToString(dr1["Name"]));
            '              cmd.Parameters.AddWithValue("@DOB", Convert.ToString(dr1["DOB"]));
            '              cmd.Parameters.AddWithValue("@NetSalary", Convert.ToString(dr1["NetSalary"]));
            '              cmd.Parameters.AddWithValue("@Bank", Convert.ToString(dr1["Bank"]));
            '              cmd.Parameters.AddWithValue("@AccountNo", Convert.ToString(dr1["AccountNo"]));
            '              cmd.Parameters.AddWithValue("@MobileNo", Convert.ToString(dr1["MobileNo"]));
            '              cmd.Parameters.AddWithValue("@DateRegistered", Convert.ToString(dr1["DateRegistered"]));
            '              cmd.Parameters.AddWithValue("@RegisteredBy", Convert.ToString(dr1["RegisteredBy"]));
            '              cmd.Parameters.AddWithValue("@InsID", Convert.ToString(dr1["InsID"]));
            '              cmd.Parameters.AddWithValue("@Status", Convert.ToString(dr1["Status"]));
            '              cmd.Parameters.AddWithValue("@FirstLogin", Convert.ToString(dr1["FirstLogin"]));
            '              cmd.Parameters.AddWithValue("@Locked", Convert.ToString(dr1["Locked"]));
            '              cmd.Parameters.AddWithValue("@Role", Convert.ToString(dr1["Role"]));


            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessUploadStaff : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "permissions" array.
            Dim permissions As Chilkat.JsonArray = json.ArrayOf("staffs")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessUploadStaff : " & "No Staffs Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            Dim svalues As New UploadStaffs
            ' Dim Descr As String = json.StringOf("Description")

            'If Rolename = "" Then
            '    Message.Message = "Role name Required"
            '    Message.Payload = "Enter Role Name"
            '    Message.mError = True
            '    Return Message
            'End If



            ' Check for Duplicate Entry





            'Message = DBConnection.SaveNewRole(vvals, username)
            'If Message.Message = "" Or Message.Message Is Nothing Then

            '    Message.Message = "Unable to Save Records"
            '    Message.Payload = "Internal Error"
            '    Message.mError = True
            '    Return Message

            'Else
            ' Iterate over each permissions, getting the JSON object at each index.
            Dim numApplicants As Integer = permissions.Size
                Dim i As Integer = 0
                While i < numApplicants

                    Dim appObj As Chilkat.JsonObject = permissions.ObjectAt(i)

                    ' Dim ssql As String
                    Dim status As Boolean
                    Dim id As Integer
                    Dim permissionname As String

                With svalues
                    .staffid = appObj.StringOf("staffid")
                    .Name = appObj.StringOf("Name")
                    .DOB = appObj.StringOf("DOB")
                    .NetSalary = appObj.StringOf("NetSalary")
                    .Bank = appObj.StringOf("Bank")
                    .AccountNo = appObj.StringOf("AccountNo")
                    .MobileNo = appObj.StringOf("MobileNo")
                    .DateRegistered = appObj.StringOf("DateRegistered")
                    .RegisteredBy = username
                    .InsID = appObj.StringOf("InsID")
                    .Status = appObj.StringOf("Status")
                    .FirstLogin = appObj.StringOf("staffid")
                    .Locked = appObj.StringOf("staffid")
                    .Role = appObj.StringOf("staffid")

                End With



                sSQL = "SELECT COUNT(MobileNo)AS NAME from Staffs WHERE MobileNo = '" + svalues.MobileNo + "'"
                Dr = DBConnection.GetRowInfo(sSQL)
                If Dr Is Nothing Then
                    Message.Message = "Internal Error  "
                    Message.Payload = "Cannot Validate Staffs "
                    Message.mError = True
                    Return Message
                Else

                    Dim fint As Integer
                    fint = CInt(Dr("Name").ToString)

                    If fint > 0 Then

                        Message.Message = "Record Already Exist"
                        Message.Payload = "Record Already Exist"
                        Message.mError = True

                        '  Return Message
                    Else

                        ' Message.mError = Not DBConnection.SaveRolePermissions(username, permissionname, status, Rolename)


                        i = i + 1

                        If Message.mError = False Then
                            Message.Message = "Processed Successfully"
                            Message.Payload = "Successful"
                        Else
                            Message.Message = "Unable to Complete Request"
                            Message.Payload = "Error Processing request"
                            ' Message.mError = True

                        End If
                        '  Message.mError = False
                    End If


                End If
            End While



            Return Message





        Catch ex As Exception
            SavetoLog("Error From ProcessRoles : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function


End Class
