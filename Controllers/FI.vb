﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class FI
    Inherits ControllerBase
    <HttpPost>
    <Route("/admin/addfi")>
    Public Function FI(<FromBody()> ByVal values As FIModel) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim Dr As DataRow
            Dim sSQL As String
            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try
                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.CompanyName.Length = 0 Then
                        Message.Message = "Company Name Required"
                        Message.Payload = "No Company Name Entered"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Generate the FI Code ie the ID Number
                    values.CompanyNo = GenerateFIID()

                    If values.CompanyNo = "" Then
                        Message.Message = "Internal Error"
                        Message.Payload = "Unable to Generate Company ID"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    ' Check for Duplicate Entry

                    sSQL = "SELECT COUNT(CompanyNo)AS NAME from FIs WHERE CompanyNo = '" + values.CompanyNo + "'"
                    Dr = DBConnection.GetRowInfo(sSQL)
                    If Dr Is Nothing Then
                        Message.Message = "No Records Found"
                        Message.Payload = "No Records Found"
                        Return BadRequest(ReturnJSONResults(Message))
                    Else

                        Dim fint As Integer
                        fint = CInt(Dr("Name").ToString)

                        If fint > 0 Then

                            Message.Message = "Record Already Exist"
                            Message.Payload = "Record Already Exist"

                            Return BadRequest(ReturnJSONResults(Message))
                        Else
                            ' Go ahead to save Record

                            Dim username As String

                            username = GetJWTUsername(myToken)

                            If username = "" Or username Is Nothing Then
                                Message.Message = "Unable to Retrieve Username from token"
                                Message.Payload = "Unable to Retrieve Username from token"
                                Return BadRequest(ReturnJSONResults(Message))
                            End If
                            values.RegisteredBy = username
                            Message = DBConnection.SaveFI(values)
                            If Message.Message = "" Or Message.Message Is Nothing Then

                                Message.Message = "Unable to Save Records"
                                Message.Payload = "Internal Error"

                                Return BadRequest(ReturnJSONResults(Message))

                            Else
                                Dim rval As String
                                rval = ReturnJSONResults(Message)

                                Return Ok(rval)
                            End If

                        End If
                        'Dim cRole As New Roles
                        'With cRole
                        '    .RoleId = Dr("RoleId")
                        '    .RoleName = Dr("RoleName")
                        '    .Description = Dr("Description")
                        '    .IsEnabled = Dr("IsEnabled")
                        '    .IsAuto = Dr("IsAuto")
                        '    .DateCreated = Dr("DateCreated")
                        '    .DateModified = Dr("DateModified")

                        'End With

                        'Dim result As New JsonResult(cRole)

                        'Return Ok(result)

                    End If



                End If
            Catch ex As Exception

            End Try



        End If

    End Function
    <HttpPost>
    <Route("/admin/updatefi")>
    Public Function UpdateFI(<FromBody()> ByVal values As FIModel) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.CompanyNo.Length = 0 Then
                        Message.Message = "Company Number Required"
                        Message.Payload = "Company Number Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Update Record
                    Message = DBConnection.UpdateFI(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Update Record or Record May Not Be Found"
                        Message.Payload = "Error Processing Request"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/deletefi")>
    Public Function DeleteFI(<FromBody()> ByVal values As FIModel) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.CompanyNo.Length = 0 Then
                        Message.Message = "Company Number Required"
                        Message.Payload = "Company Number Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Delete Record
                    Message = DBConnection.DeleteFI(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Delete Records Or Record Does not Exist"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpGet>
    <Route("/admin/fis")>
    Public Function GetFIs(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From FIs"
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select *, count(*) OVER() AS full_count  From FIs ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Financial Institution Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get FIs" & ex.Message)
            End Try


            ' Return jsonconvert
        End If



        '--------------------------
        'Expected Results

        '     {  
        '    "Title": "The Cuckoo's Calling",  
        '    "Author": "Robert Galbraith",  
        '    "Genre": "classic crime novel",  
        '    "Detail": {  
        '        "Publisher": "Little Brown",  
        '        "Publication_Year": 2013,  
        '        "ISBN-13": 9781408704004,  
        '        "Language": "English",  
        '        "Pages": 494  
        '    },  
        '    "Price": [  
        '        {  
        '            "type": "Hardcover",  
        '            "price": 16.65  
        '        },  
        '        {  
        '            "type": "Kindle Edition",  
        '            "price": 7.00  
        '        }  
        '    ]  
        '}  

        '
        ''  The only reason for failure in the following lines of code would be an out-of-memory condition..

        ''  An index value of -1 is used to append at the end.
        'Dim index As Integer = -1

        'success = Json.AddStringAt(-1, "Title", "The Cuckoo's Calling")
        'success = Json.AddStringAt(-1, "Author", "Robert Galbraith")
        'success = Json.AddStringAt(-1, "Genre", "classic crime novel")

        ''  Let's create the Detail JSON object:
        'success = Json.AddObjectAt(-1, "Detail")
        'Dim detail As Chilkat.JsonObject = Json.ObjectAt(Json.Size - 1)
        'success = detail.AddStringAt(-1, "Publisher", "Little Brown")
        'success = detail.AddIntAt(-1, "Publication_Year", 2013)
        'success = detail.AddNumberAt(-1, "ISBN-13", "9781408704004")
        'success = detail.AddStringAt(-1, "Language", "English")
        'success = detail.AddIntAt(-1, "Pages", 494)


        ''  Add the array for Price
        'success = Json.AddArrayAt(-1, "Price")
        'Dim aPrice As Chilkat.JsonArray = Json.ArrayAt(Json.Size - 1)

        ''  Entry entry in aPrice will be a JSON object.

        ''  Append a new/empty ojbect to the end of the aPrice array.
        'success = aPrice.AddObjectAt(-1)
        ''  Get the object that was just appended.
        'Dim priceObj As Chilkat.JsonObject = aPrice.ObjectAt(aPrice.Size - 1)
        'success = priceObj.AddStringAt(-1, "type", "Hardcover")
        'success = priceObj.AddNumberAt(-1, "price", "16.65")


        'success = aPrice.AddObjectAt(-1)
        'priceObj = aPrice.ObjectAt(aPrice.Size - 1)
        'success = priceObj.AddStringAt(-1, "type", "Kindle Edition")
        'success = priceObj.AddNumberAt(-1, "price", "7.00")




        'Json.EmitCompact = False
        'Console.WriteLine(Json.Emit())


        '---------------

        ' Dim JsonConvert As New JsonResult()


        '  Return JsonConvert
    End Function

    <HttpPost>
    <Route("/admin/fis/filter")>
    Public Function GetFIFilter(<FromBody()> ByVal values As FIfilters) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)


                'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
                sSQL = "Select * , count(*) OVER() AS full_count From FIs"

                If values.companyno.Length <= 0 And values.companyname.Length <= 0 Then
                    Message.Message = "No Valid Filter Value Available"
                    Message.Payload = "Provide at least one valid filter value"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    If values.companyname.Length > 0 Then
                        ' '%\_%'
                        sSQL = sSQL & " Where CompanyName ilike '%" & values.companyname & "%' "
                    End If

                    If values.companyno.Length > 0 Then

                        If values.companyname.Length > 0 Then
                            sSQL = sSQL & "And CompanyNo='" & values.companyno & "' "
                        Else
                            sSQL = sSQL & " Where CompanyNo='" & values.companyno & "' "
                        End If

                    End If
                End If

                If values.pagination = False Then
                    ' sSQL = "Select * From FIs"
                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Financial Institution Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If



        '--------------------------
        'Expected Results

        '     {  
        '    "Title": "The Cuckoo's Calling",  
        '    "Author": "Robert Galbraith",  
        '    "Genre": "classic crime novel",  
        '    "Detail": {  
        '        "Publisher": "Little Brown",  
        '        "Publication_Year": 2013,  
        '        "ISBN-13": 9781408704004,  
        '        "Language": "English",  
        '        "Pages": 494  
        '    },  
        '    "Price": [  
        '        {  
        '            "type": "Hardcover",  
        '            "price": 16.65  
        '        },  
        '        {  
        '            "type": "Kindle Edition",  
        '            "price": 7.00  
        '        }  
        '    ]  
        '}  

        '
        ''  The only reason for failure in the following lines of code would be an out-of-memory condition..

        ''  An index value of -1 is used to append at the end.
        'Dim index As Integer = -1

        'success = Json.AddStringAt(-1, "Title", "The Cuckoo's Calling")
        'success = Json.AddStringAt(-1, "Author", "Robert Galbraith")
        'success = Json.AddStringAt(-1, "Genre", "classic crime novel")

        ''  Let's create the Detail JSON object:
        'success = Json.AddObjectAt(-1, "Detail")
        'Dim detail As Chilkat.JsonObject = Json.ObjectAt(Json.Size - 1)
        'success = detail.AddStringAt(-1, "Publisher", "Little Brown")
        'success = detail.AddIntAt(-1, "Publication_Year", 2013)
        'success = detail.AddNumberAt(-1, "ISBN-13", "9781408704004")
        'success = detail.AddStringAt(-1, "Language", "English")
        'success = detail.AddIntAt(-1, "Pages", 494)


        ''  Add the array for Price
        'success = Json.AddArrayAt(-1, "Price")
        'Dim aPrice As Chilkat.JsonArray = Json.ArrayAt(Json.Size - 1)

        ''  Entry entry in aPrice will be a JSON object.

        ''  Append a new/empty ojbect to the end of the aPrice array.
        'success = aPrice.AddObjectAt(-1)
        ''  Get the object that was just appended.
        'Dim priceObj As Chilkat.JsonObject = aPrice.ObjectAt(aPrice.Size - 1)
        'success = priceObj.AddStringAt(-1, "type", "Hardcover")
        'success = priceObj.AddNumberAt(-1, "price", "16.65")


        'success = aPrice.AddObjectAt(-1)
        'priceObj = aPrice.ObjectAt(aPrice.Size - 1)
        'success = priceObj.AddStringAt(-1, "type", "Kindle Edition")
        'success = priceObj.AddNumberAt(-1, "price", "7.00")




        'Json.EmitCompact = False
        'Console.WriteLine(Json.Emit())


        '---------------

        ' Dim JsonConvert As New JsonResult()


        '  Return JsonConvert
    End Function


End Class
