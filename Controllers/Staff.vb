﻿

Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class Staff
    Inherits ControllerBase

    <HttpPost>
    <Route("/admin/addstaff")>
    Public Function AddStaffs(<FromBody()> ByVal values As Staffs) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim Dr As DataRow
            Dim sSQL As String
            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.Name.Length = 0 Then
                        Message.Message = "Staff Name Required"
                        Message.Payload = "Staff Name Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    ElseIf values.MobileNo.Length = 0 Then
                        Message.Message = "Mobile Number Required"
                        Message.Payload = "Mobile Number Entered"
                        Return BadRequest(ReturnJSONResults(Message))
                        'ElseIf values.Picture.Length = 0 Then
                        '    Message.Message = "Mobile Number Required"
                        '    Message.Payload = "Mobile Number Entered"
                        '    Return BadRequest(ReturnJSONResults(Message))
                    End If

                    ' Check for Duplicate Entry
                    'string Name, string Gender, string DateOfBirth, string PhoneNo, string Email, string CompanyName, string Location, decimal NetSalary, string SSNITNo, st
                    sSQL = "SELECT COUNT(MobileNo)AS NAME from Staffs WHERE MobileNo = '" + values.MobileNo + "'"
                    Dr = DBConnection.GetRowInfo(sSQL)
                    If Dr Is Nothing Then
                        Message.Message = "Internal Error"
                        Message.Payload = "Cannot Get Staff Count"
                        Return BadRequest(ReturnJSONResults(Message))
                    Else

                        Dim fint As Integer
                        fint = CInt(Dr("Name").ToString)

                        If fint > 0 Then

                            Message.Message = "Staff Already Exist"
                            Message.Payload = "Staff Already Exist"

                            Return BadRequest(ReturnJSONResults(Message))
                        Else

                            ' get the username from  the token
                            Dim username As String = GetJWTUsername(myToken)
                            If username = "" Or username Is Nothing Then
                                Message.Message = "Unable to Retrieve Username from token"
                                Message.Payload = "Unable to Retrieve Username from token"
                                Return BadRequest(ReturnJSONResults(Message))
                            End If

                            ' Go ahead to save Record

                            Message = DBConnection.SaveStaff(values, username)
                            If Message.Message = "" Or Message.Message Is Nothing Then

                                Message.Message = "Unable to Save Records"
                                Message.Payload = "Internal Error"

                                Return BadRequest(ReturnJSONResults(Message))

                            Else


                                Dim rval As String
                                rval = ReturnJSONResults(Message)

                                Return Ok(rval)
                            End If

                        End If

                    End If



                End If
            Catch ex As Exception

            End Try



        End If

    End Function
    <HttpPost>
    <Route("/admin/updatestaff")>
    Public Function UpdateStaff(<FromBody()> ByVal values As Staffs) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.UserName.Length = 0 Then
                        Message.Message = "Username  Required"
                        Message.Payload = "Username  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    If values.MobileNo.Length = 0 Then
                        Message.Message = "Mobile Number Required"
                        Message.Payload = "Mobile Number Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Update Record
                    Message = DBConnection.UpdateStaff(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Update Records or Record Does not Exist"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/approverejectborrower")>
    Public Function Approverejectborrower(<FromBody()> ByVal values As Staffs) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.id <= 0 Then
                        Message.Message = "id  Required"
                        Message.Payload = "id  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    'If values.Status.Length = 0 Then
                    '    Message.Message = "Mobile Number Required"
                    '    Message.Payload = "Mobile Number Required"
                    '    Return BadRequest(ReturnJSONResults(Message))
                    'End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Update Record
                    Message = DBConnection.ApproveRejectBorrower(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Update Records or Record Does not Exist"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/deletestaff")>
    Public Function DeleteStaff(<FromBody()> ByVal values As Staffs) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.id <= 0 Then
                        Message.Message = "ID Required"
                        Message.Payload = "ID Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Delete Record
                    Message = DBConnection.DeleteStaff(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Delete Records or Record Does not Exist"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpGet>
    <Route("/admin/staffs")>
    Public Function GetStaffs(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From Staffs"
                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = "Select *, count(*) OVER() AS full_count From Staffs  ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Staffs  Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpPost>
    <Route("/admin/staff/filter")>
    Public Function GetStaffFilter(<FromBody()> ByVal values As StaffFilters, <FromQuery()> ByVal qval As StaffFilters) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token


                'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
                sSQL = "Select id, Name, Gender, DOB, MobileNo, Email, CompanyName, Location, NetSalary, SSNITNo,InsID, SalaryAccountNo, BankName, Branch, AccountNo, UserName, Status, RegisteredStatus, IDCard, Picture, Signature, IDType, IDNO, StaffID , count(*) OVER() AS full_count From Staffs"


                '' where ((InsID = @InsID and @InsID <> '0') or (InsID = InsID and @InsID = '0')) AND ((Status = @Status and @Status <> '0') or (Status = Status and @Status = '0'))
                'sSQL = "Select * From Staffs where ((InsID = '" & values.insID & "' and " & values.insID & " <> '0') or (InsID = InsID and " & values.insID & " = '0')) AND ((Status = " & values.status & " and " & values.status & " <> '0') or (Status = Status and " & values.status & " = '0'))"

                If values.status.Length <= 0 And values.insID.Length <= 0 Then
                    Message.Message = "No Valid Filter Value Available"
                    Message.Payload = "Provide at least one valid filter value"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    '----
                    If values.insID.Length > 0 Then
                        ' '%\_%'
                        sSQL = sSQL & " Where InsID ilike '%" & values.insID & "%' "
                    End If


                    If values.status <> "" Then

                        If values.status.ToLower = "true" Then
                            values.bstatus = True
                        Else
                            values.bstatus = False
                        End If


                        If values.insID.Length > 0 Then

                            sSQL = sSQL & "And Status='" & values.bstatus & "' "
                        Else
                            sSQL = sSQL & " Where Status='" & values.bstatus & "' "


                        End If
                    End If
                    ' ----

                End If

                If qval.pagination = False Then
                    ' sSQL = "Select * From FIs"
                Else
                    Dim pagenum As Integer
                    pagenum = qval.pagesize * (qval.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qval.pagesize.ToString & " OFFSET " & pagenum
                End If
                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)



                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Staff  Records Filtered"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function



End Class
