﻿Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO

Public Class Disbursment
    Inherits ControllerBase

    <HttpGet>
    <Route("/admin/disbursement/momo")>
    Public Function GetMOMODisbursement(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token


                sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, SL.Amount, SL.FI, SL.Status, DM.Modde, S.Name, SL.MobileNo, S.AccountNo,count(*) OVER() AS full_count
 FROM SalaryLoan SL
 Left Join DisbursementMode DM
 On SL.Modde = DM.id
 Left Join Staffs S
 On SL.StaffID = S.StaffID
  where SL.Status = '1500' and DM.modde ilike '%mobile%'"


                Dim hasmore As Boolean = False


                If values.pagination = True Then

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)



                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Disbursement Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Disbursement" & ex.Message)
            End Try


            ' Return jsonconvert
        End If



        '--------------------------
        'Expected Results

        '     {  
        '    "Title": "The Cuckoo's Calling",  
        '    "Author": "Robert Galbraith",  
        '    "Genre": "classic crime novel",  
        '    "Detail": {  
        '        "Publisher": "Little Brown",  
        '        "Publication_Year": 2013,  
        '        "ISBN-13": 9781408704004,  
        '        "Language": "English",  
        '        "Pages": 494  
        '    },  
        '    "Price": [  
        '        {  
        '            "type": "Hardcover",  
        '            "price": 16.65  
        '        },  
        '        {  
        '            "type": "Kindle Edition",  
        '            "price": 7.00  
        '        }  
        '    ]  
        '}  

        '
        ''  The only reason for failure in the following lines of code would be an out-of-memory condition..

        ''  An index value of -1 is used to append at the end.
        'Dim index As Integer = -1

        'success = Json.AddStringAt(-1, "Title", "The Cuckoo's Calling")
        'success = Json.AddStringAt(-1, "Author", "Robert Galbraith")
        'success = Json.AddStringAt(-1, "Genre", "classic crime novel")

        ''  Let's create the Detail JSON object:
        'success = Json.AddObjectAt(-1, "Detail")
        'Dim detail As Chilkat.JsonObject = Json.ObjectAt(Json.Size - 1)
        'success = detail.AddStringAt(-1, "Publisher", "Little Brown")
        'success = detail.AddIntAt(-1, "Publication_Year", 2013)
        'success = detail.AddNumberAt(-1, "ISBN-13", "9781408704004")
        'success = detail.AddStringAt(-1, "Language", "English")
        'success = detail.AddIntAt(-1, "Pages", 494)


        ''  Add the array for Price
        'success = Json.AddArrayAt(-1, "Price")
        'Dim aPrice As Chilkat.JsonArray = Json.ArrayAt(Json.Size - 1)

        ''  Entry entry in aPrice will be a JSON object.

        ''  Append a new/empty ojbect to the end of the aPrice array.
        'success = aPrice.AddObjectAt(-1)
        ''  Get the object that was just appended.
        'Dim priceObj As Chilkat.JsonObject = aPrice.ObjectAt(aPrice.Size - 1)
        'success = priceObj.AddStringAt(-1, "type", "Hardcover")
        'success = priceObj.AddNumberAt(-1, "price", "16.65")


        'success = aPrice.AddObjectAt(-1)
        'priceObj = aPrice.ObjectAt(aPrice.Size - 1)
        'success = priceObj.AddStringAt(-1, "type", "Kindle Edition")
        'success = priceObj.AddNumberAt(-1, "price", "7.00")




        'Json.EmitCompact = False
        'Console.WriteLine(Json.Emit())


        '---------------

        ' Dim JsonConvert As New JsonResult()


        '  Return JsonConvert
    End Function

    <HttpGet>
    <Route("/admin/disbursement/bank")>
    Public Function GetBankDisbursement(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token


                sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, SL.Amount, SL.FI, SL.Status, DM.Modde, S.Name, SL.MobileNo, S.AccountNo,count(*) OVER() AS full_count
 FROM SalaryLoan SL
 Left Join DisbursementMode DM
 On SL.Modde = DM.id
 Left Join Staffs S
 On SL.StaffID = S.StaffID
  where SL.Status = '1500' and DM.modde ilike '%bank%'"


                Dim hasmore As Boolean = False


                If values.pagination = True Then

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)



                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Disbursement Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Disbursement" & ex.Message)
            End Try

            ' Return jsonconvert
        End If

    End Function

    <HttpPost>
    <Route("/admin/disbursement/momo")>
    Public Function MOMO(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'process body json

                    ' Go ahead to Update Record

                    Message = ProcessMOMO(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function



    Public Function ProcessMOMO(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token



            ' This is the above JSON with whitespace chars removed (SPACE, TAB, CR, and LF chars).
            ' The presence of whitespace chars for pretty-printing makes no difference to the Load
            ' method. 

            '  Dim jsonStr As String = "{""employees"":[{""firstName"":""John"", ""lastName"":""Doe""},{""firstName"":""Anna"", ""lastName"":""Smith""},{""firstName"":""Peter"",""lastName"":""Jones""}]}"
            ' ItemID = item["ID"].Text;
            'staffid = item["StaffID"].Text;
            '        amount = item["Amount"].Text;
            '        number = item["MobileNo"].Text;
            '         ddd = int.Parse(ItemID);
            '
            '{

            '"applicants" :[
            '{
            '"id": 30,
            '"staffid": "nkunim",
            '"amount": "1000",
            '"number": "0570099699"

            '},
            '{
            '"id": 45,
            '"staffid": "nkunim2",
            '"amount": "1000",
            '"number": "0570099699"
            '}
            ']
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessMOMO : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "employees" array.
            Dim applicants As Chilkat.JsonArray = json.ArrayOf("applicants")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessMOMO : " & "No applicants Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            '  Dim InstID As String = json.StringOf("institution_id")

            ' Iterate over each employee, getting the JSON object at each index.
            Dim numApplicants As Integer = applicants.Size
            Dim i As Integer = 0
            While i < numApplicants

                Dim appObj As Chilkat.JsonObject = applicants.ObjectAt(i)

                Dim ssql As String
                Dim staffid As String
                Dim number As String
                Dim amount As String
                Dim id As Integer

                Dim MERCHANTID, PRODUCTID, REFNUMBER, PHONENUMBER, NETWORK, NARRATION, APIKEY, URL, HOST As String

                ' Dim ssql As String

                MERCHANTID = GetSettings("General.merchantId")
                PRODUCTID = GetSettings("General.productId")
                APIKEY = GetSettings("General.apiKey")
                HOST = GetSettings("General.itchost")
                URL = HOST & GetSettings("General.itccreditcustomer")

                REFNUMBER = DateTime.Now.ToString("yyyMMddhhmmsshh")

                staffid = appObj.StringOf("staffid")
                number = appObj.StringOf("number")
                amount = appObj.StringOf("amount")
                id = Integer.Parse(appObj.StringOf("id"))

                NETWORK = GetNetwork(number)

                PHONENUMBER = FormatSento(number)
                NARRATION = "MoneyShopp Loan Payment"

                Dim CreditCustomertemplate As String

                CreditCustomertemplate = "{
      ""merchantId"":""#MERCHANTID"",
      ""productId"":""#PRODUCTID"",
      ""refNo"":""#REFNUMBER"",
      ""msisdn"":""#PHONENUMBER"",
      ""amount"":""#AMOUNT"",
      ""network"":""#NETWORK"",
      ""narration"":""#NARRATION"",
      ""apiKey"":""#APIKEY""
}
"

                '--------------------

                Dim PostMsg As String

                PostMsg = CreditCustomertemplate.Replace("#MERCHANTID", MERCHANTID)
                PostMsg = PostMsg.Replace("#PRODUCTID", PRODUCTID)
                PostMsg = PostMsg.Replace("#REFNUMBER", REFNUMBER)
                PostMsg = PostMsg.Replace("#PHONENUMBER", PHONENUMBER)
                PostMsg = PostMsg.Replace("#AMOUNT", amount)
                PostMsg = PostMsg.Replace("#NETWORK", NETWORK)
                PostMsg = PostMsg.Replace("#NARRATION", NARRATION)
                PostMsg = PostMsg.Replace("#APIKEY", APIKEY)
                Dim MESS As New GeneralResponds
                MESS = PostTransaction(PostMsg, URL)
                If MESS.mError = False Then
                    ssql = "update SalaryLoan set TransferCode='" & MESS.Message & "', TransferMessage='" & MESS.Payload & "', TransID='" & REFNUMBER & "', MessageID='" + REFNUMBER + "', DateDisbursed=@DateDisbursed WHERE ID=" & id

                    Message.mError = Not DBConnection.MOMODisbursement(ssql, username, staffid, amount)


                Else
                    ssql = "update SalaryLoan set TransferCode='" & MESS.Message & "', TransferMessage='" & MESS.Payload & "', TransID='" & REFNUMBER & "', MessageID='" + REFNUMBER + "', DateDisbursed=@DateDisbursed WHERE ID=" & id

                    DBConnection.MOMODisbursement(ssql, username, staffid, amount)

                    Message.Message = "Processed With Errors"
                    Message.Payload = "Disbursment Failed"

                    Return Message
                End If


                '----------------------------

                i = i + 1
            End While


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessMOMO : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function


    <HttpPost>
    <Route("/admin/disbursement/bank")>
    Public Function BankManulaDisbursement(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'process body json

                    ' Go ahead to Update Record

                    Message = ProcessBank_Manual(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function
    Public Function ProcessBank_Manual(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token



            ' This is the above JSON with whitespace chars removed (SPACE, TAB, CR, and LF chars).
            ' The presence of whitespace chars for pretty-printing makes no difference to the Load
            ' method. 

            '  Dim jsonStr As String = "{""employees"":[{""firstName"":""John"", ""lastName"":""Doe""},{""firstName"":""Anna"", ""lastName"":""Smith""},{""firstName"":""Peter"",""lastName"":""Jones""}]}"
            ' ItemID = item["ID"].Text;
            'staffid = item["StaffID"].Text;
            '        amount = item["Amount"].Text;
            '        number = item["MobileNo"].Text;
            '         ddd = int.Parse(ItemID);
            '
            '{

            '"applicants" :[
            '{
            '"id": 30,
            '"staffid": "nkunim",
            '"amount": "1000",
            '"number": "0570099699"

            '},
            '{
            '"id": 45,
            '"staffid": "nkunim2",
            '"amount": "1000",
            '"number": "0570099699"
            '}
            ']
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessBank_Manual : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "employees" array.
            Dim applicants As Chilkat.JsonArray = json.ArrayOf("applicants")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessBank_Manual : " & "No applicants Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            '  Dim InstID As String = json.StringOf("institution_id")

            ' Iterate over each employee, getting the JSON object at each index.
            Dim numApplicants As Integer = applicants.Size
            Dim i As Integer = 0
            While i < numApplicants

                Dim appObj As Chilkat.JsonObject = applicants.ObjectAt(i)

                Dim ssql As String
                Dim staffid As String
                Dim number As String
                Dim amount As String
                Dim id As Integer

                'Dim MERCHANTID, PRODUCTID, REFNUMBER, PHONENUMBER, NETWORK, NARRATION, APIKEY, URL, HOST As String

                ' Dim ssql As String

                staffid = appObj.StringOf("staffid")
                number = appObj.StringOf("number")
                amount = appObj.StringOf("amount")
                id = Integer.Parse(appObj.StringOf("id"))

                '--------------------

                ssql = "update SalaryLoan set Status='2000', DateDisbursed=@DateDisbursed WHERE ID=" & id

                Message.mError = Not DBConnection.MOMODisbursement(ssql, username, staffid, amount)
                '----------------------------

                i = i + 1
            End While

            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessBank_Manual : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function
End Class
