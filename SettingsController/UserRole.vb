﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class UserRole
    Inherits ControllerBase
    <HttpGet>
    <Route("/admin/settings/roles")>
    Public Function GetUserRoles(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From Roles"
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select *, count(*) OVER() AS full_count  From Roles ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Users Roles Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Roles" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpGet>
    <Route("/admin/settings/roles/permissions")>
    Public Function GetRolesPermissions(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From permissions"
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select *, count(*) OVER() AS full_count  From permissions ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Users Permissions Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Permissions" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function


    <HttpPost>
    <Route("/admin/settings/roles/addrole")>
    Public Function AddNewUser(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try
                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If




                    Dim username As String

                            username = GetJWTUsername(myToken)

                            If username = "" Or username Is Nothing Then
                                Message.Message = "Unable to Retrieve Username from token"
                                Message.Payload = "Unable to Retrieve Username from token"
                                Return BadRequest(ReturnJSONResults(Message))
                            End If

                            Message = ProcessRoles(values, username)

                            If Message.mError = True Or Message.Message Is Nothing Then

                                'Message.Message = "Unable to Update Records"
                                'Message.Payload = "Internal Error"

                                Return BadRequest(ReturnJSONResults(Message))

                            Else
                                Dim rval As String
                                rval = ReturnJSONResults(Message)

                                Return Ok(rval)
                            End If


                    'Dim cRole As New Roles
                    'With cRole
                    '    .RoleId = Dr("RoleId")
                    '    .RoleName = Dr("RoleName")
                    '    .Description = Dr("Description")
                    '    .IsEnabled = Dr("IsEnabled")
                    '    .IsAuto = Dr("IsAuto")
                    '    .DateCreated = Dr("DateCreated")
                    '    .DateModified = Dr("DateModified")

                    'End With

                    'Dim result As New JsonResult(cRole)

                    'Return Ok(result)

                End If




            Catch ex As Exception

            End Try



        End If

    End Function
    Public Function ProcessRoles(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Dim Dr As DataRow
        Dim sSQL As String
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token


            '{
            '    "RoleName": "Admin",
            '    "Description": "Test Admin",
            '    "permissions": [
            '        {
            '            "id": 1166,
            '            "status": "True",
            '            "permissionname":"AddFIs"
            '        },
            '        {
            '            "id": 1167,
            '            "status": "False",
            '            "permissionname":"AddStaffs"
            '        }
            '    ]
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessRoles : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "permissions" array.
            Dim permissions As Chilkat.JsonArray = json.ArrayOf("permissions")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessRoles : " & "No permissions Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            Dim Rolename As String = json.StringOf("RoleName")
            Dim Descr As String = json.StringOf("Description")

            If Rolename = "" Then
                Message.Message = "Role name Required"
                Message.Payload = "Enter Role Name"
                Message.mError = True
                Return Message
            End If

            'Save the Role First then go ahead to save the corresponding permissions
            Dim vvals As New Roles

            vvals.RoleName = Rolename
            vvals.Description = Descr

            ' Check for Duplicate Entry

            sSQL = "SELECT COUNT(RoleName)AS Name from Roles WHERE RoleName='" & vvals.RoleName & "'"
            Dr = DBConnection.GetRowInfo(sSQL)
            If Dr Is Nothing Then
                Message.Message = "Internal Error  "
                Message.Payload = "Cannot Validate Role Name"
                Message.mError = True
                Return Message
            Else

                Dim fint As Integer
                fint = CInt(Dr("Name").ToString)

                If fint > 0 Then

                    Message.Message = "Record Already Exist"
                    Message.Payload = "Record Already Exist"
                    Message.mError = True

                    Return Message
                Else




                    Message = DBConnection.SaveNewRole(vvals, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Save Records"
                        Message.Payload = "Internal Error"
                        Message.mError = True
                        Return Message

                    Else
                        ' Iterate over each permissions, getting the JSON object at each index.
                        Dim numApplicants As Integer = permissions.Size
                        Dim i As Integer = 0
                        While i < numApplicants

                            Dim appObj As Chilkat.JsonObject = permissions.ObjectAt(i)

                            ' Dim ssql As String
                            Dim status As Boolean
                            Dim id As Integer
                            Dim permissionname As String


                            status = CBool(appObj.StringOf("status"))
                            permissionname = appObj.StringOf("permissionname")
                            id = Integer.Parse(appObj.StringOf("id"))

                            ' ssql = "update Staffs set RegisteredStatus='" & status & "', ApprovedBy='" & username & "', ApprovedDate = '" & DateTime.Now & "', InsID = '" & InstID & "' WHERE ID=" & id

                            Message.mError = Not DBConnection.SaveRolePermissions(username, permissionname, status, Rolename)
                            ' Debug.WriteLine("employee[" & i & "] = " & appObj.StringOf("id") & " " & appObj.StringOf("status"))
                            'update the individual records

                            i = i + 1
                        End While


                        If Message.mError = False Then
                            Message.Message = "Processed Successfully"
                            Message.Payload = "Successful"
                        Else
                            Message.Message = "Unable to Complete Request"
                            Message.Payload = "Error Processing request"
                            ' Message.mError = True

                        End If
                        '  Message.mError = False
                        Return Message



                    End If




            End If

                End If

        Catch ex As Exception
            SavetoLog("Error From ProcessRoles : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function

    <HttpPost>
    <Route("/admin/settings/roles/update")>
    Public Function UpdateRoles(<FromBody()> ByVal values As Roles) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.RoleName.Length = 0 Then
                        Message.Message = "UserName  Required"
                        Message.Payload = "UserName  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    'If values.id.Length = 0 Then
                    '    Message.Message = "Id  Required"
                    '    Message.Payload = "Id  Required"
                    '    Return BadRequest(ReturnJSONResults(Message))
                    'End If
                    If values.Description.Length = 0 Then
                        Message.Message = "Description  Required"
                        Message.Payload = "Description  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Go ahead to Update Record
                    Message = DBConnection.UpdateRoles(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Update Record or Record May Not Be Found"
                        Message.Payload = "Error Processing Request"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/settings/roles/delete")>
    Public Function DeleteRoles(<FromBody()> ByVal values As Roles) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.RoleName.Length = 0 Then
                        Message.Message = "Rolenname  Required"
                        Message.Payload = "Rolename  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Delete Record
                    Message = DBConnection.DeleteRoles(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Delete Records Or Record Does not Exist"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function


    <HttpPost>
    <Route("/admin/settings/rolespermission/update")>
    Public Function UpdateRolePermission(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try
                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If




                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    Message = ProcessUpdateRolesPermission(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                    'Dim cRole As New Roles
                    'With cRole
                    '    .RoleId = Dr("RoleId")
                    '    .RoleName = Dr("RoleName")
                    '    .Description = Dr("Description")
                    '    .IsEnabled = Dr("IsEnabled")
                    '    .IsAuto = Dr("IsAuto")
                    '    .DateCreated = Dr("DateCreated")
                    '    .DateModified = Dr("DateModified")

                    'End With

                    'Dim result As New JsonResult(cRole)

                    'Return Ok(result)

                End If




            Catch ex As Exception

            End Try



        End If

    End Function

    Public Function ProcessUpdateRolesPermission(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds

        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token


            '{
            '    "RoleName": "Admin",
            '    "Description": "Test Admin",
            '    "permissions": [
            '        {
            '            "id": 1166,
            '            "status": "True",
            '            "permissionname":"AddFIs"
            '        },
            '        {
            '            "id": 1167,
            '            "status": "False",
            '            "permissionname":"AddStaffs"
            '        }
            '    ]
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessRoles : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "permissions" array.
            Dim permissions As Chilkat.JsonArray = json.ArrayOf("permissions")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessRoles : " & "No permissions Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            Dim Rolename As String = json.StringOf("RoleName")
            Dim Descr As String = json.StringOf("Description")

            If Rolename = "" Then
                Message.Message = "Role name Required"
                Message.Payload = "Enter Role Name"
                Message.mError = True
                Return Message
            End If

            'Save the Role First then go ahead to save the corresponding permissions
            Dim vvals As New Roles

            vvals.RoleName = Rolename
            vvals.Description = Descr





            ' Iterate over each permissions, getting the JSON object at each index.
            Dim numApplicants As Integer = permissions.Size
            Dim i As Integer = 0
            While i < numApplicants

                Dim appObj As Chilkat.JsonObject = permissions.ObjectAt(i)

                ' Dim ssql As String
                Dim status As Boolean
                Dim id As Integer
                Dim permissionname As String


                status = CBool(appObj.StringOf("status"))
                permissionname = appObj.StringOf("permissionname")
                id = Integer.Parse(appObj.StringOf("id"))

                ' ssql = "update Staffs set RegisteredStatus='" & status & "', ApprovedBy='" & username & "', ApprovedDate = '" & DateTime.Now & "', InsID = '" & InstID & "' WHERE ID=" & id

                Message.mError = Not DBConnection.UpdateRolePermissions(username, permissionname, status, Rolename, id)
                ' Debug.WriteLine("employee[" & i & "] = " & appObj.StringOf("id") & " " & appObj.StringOf("status"))
                'update the individual records

                i = i + 1
            End While


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"
                ' Message.mError = True

            End If
            '  Message.mError = False
            Return Message




        Catch ex As Exception
            SavetoLog("Error From ProcessRoles : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function

    <HttpGet>
    <Route("/admin/settings/rolespermission")>
    Public Function Getpayday(<FromQuery()> ByVal qval As RolePermissionsFilter) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)


                'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
                sSQL = "SELECT id, Permission, Status, Role FROM RolePermissions WHERE Role = '" & qval.Role & "'"


                If qval.pagination = False Then
                    ' sSQL = "Select * From FIs"
                Else
                    Dim pagenum As Integer
                    pagenum = qval.pagesize * (qval.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qval.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Role Permission Records Filtered"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

End Class
