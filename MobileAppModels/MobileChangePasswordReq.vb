﻿Public Class MobileChangePasswordReq
    Public Property username As String
    Public Property oldpassword As String
    Public Property newpassword As String
    Public Property token As String
    Public Property forgotpassword As Boolean
    Public Property appversion As String

End Class
