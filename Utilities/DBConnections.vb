﻿Imports System.Data
'Imports System.Data.SqlClient
Imports Npgsql
'Imports MySql.Data.MySqlClient
Public Class DBConnection
    Public Shared Function DbConString() As String
        'DbConString = "Data Source= localhost\sqlExpress;" & _
        '                "initial catalog=School;integrated security = True"
        DbConString = GetSettings("General.DBString")

        ' SavetoLog("DbConString : " & DbConString)
        '  DbConString = "Database=Opticareplusdb;" & "Data Source=localhost;" & "User Id=admin;Password=admin1234"
    End Function

    Public Shared Sub DbRun(ByVal sSql As String, Optional ByVal Pic As String = "", Optional ByVal arPic() As Byte = Nothing)

        If arPic IsNot Nothing Then
            'save with picture
            Dim DbConn As NpgsqlConnection
            Dim CmdSql As NpgsqlCommand

            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
            Try
                With CmdSql
                    .Connection.Open()
                    .Parameters.AddWithValue("@Photo", arPic)
                    .ExecuteNonQuery()
                    .Connection.Close()
                    .Dispose()
                End With
            Catch ex As Exception
                ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
                Throw ex
            Finally
                DbConn.Close()
                DbConn.Dispose()
            End Try


            Exit Sub
        End If


        If Pic = "" Then
            Dim DbConn As NpgsqlConnection
            Dim CmdSql As NpgsqlCommand

            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)

            Try
                With CmdSql
                    .Connection.Open()
                    .ExecuteNonQuery()
                    .Connection.Close()
                    .Dispose()
                End With
            Catch ex As Exception
                'MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
                Throw ex
            Finally
                DbConn.Close()
                DbConn.Dispose()
            End Try


        Else
            'save with picture
            Dim DbConn As NpgsqlConnection
            Dim CmdSql As NpgsqlCommand

            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
            Try
                With CmdSql
                    .Connection.Open()
                    .Parameters.AddWithValue("@Photo", IO.File.ReadAllBytes(Pic))
                    .ExecuteNonQuery()
                    .Connection.Close()
                    .Dispose()
                End With
            Catch ex As Exception
                ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
                Throw ex
            Finally
                DbConn.Close()
                DbConn.Dispose()
            End Try

        End If
    End Sub

    Public Shared Sub DbRunTran(ByVal sSQLs As ArrayList)
        Dim DbConn As New NpgsqlConnection(DbConString)
        Dim SqlTrans As NpgsqlTransaction

        DbConn.Open()
        SqlTrans = DbConn.BeginTransaction()

        Try
            For Each sSQL As String In sSQLs
                Dim CmdSql As New NpgsqlCommand(sSQL, DbConn)
                With CmdSql
                    .Transaction = SqlTrans
                    .ExecuteNonQuery()
                End With
            Next
            SqlTrans.Commit()
        Catch ex As Exception
            SqlTrans.Rollback()
            Throw ex 'New Exception("Data Entry Failed")
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try

    End Sub

    Public Shared Function GetRowInfo(ByVal sSql As String) As DataRow
        Dim sDa As New NpgsqlDataAdapter(sSql, DbConString)
        Dim Dt As New DataTable

        Try
            sDa.Fill(Dt)
            sDa.Dispose()
            If Dt.Rows.Count = 0 Then
                Return Nothing
            ElseIf Dt.Rows.Count > 1 Then
                Throw New Exception("Multiple rows effected")
            Else
                Return Dt.Rows(0)
            End If
        Catch ex As Exception
            SavetoLog("Error MSG From  GetRowInfo:" & ex.Message)
            Return Nothing
        End Try
        'Return Dt.Rows(0)
    End Function

    Public Shared Function GetTableData(ByVal sSql As String) As DataTable
        Try
            Dim sDa As New NpgsqlDataAdapter(sSql, DbConString)
            Dim Dt As New DataTable
            sDa.Fill(Dt)
            sDa.Dispose()
            ' Dim i As Integer = Dt.Rows.Count
            Return Dt
        Catch ex As Exception
            SavetoLog("Error from GetTableData :" & ex.Message & vbNewLine & "With SQL: " & sSql)
        End Try


    End Function
    Public Shared Function GetUserTableData(ByVal sSql As String, values As AuthModel) As DataTable

        Dim sDa As New NpgsqlDataAdapter()
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Try
            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            With CmdSql
                .Parameters.AddWithValue("@UName", values.username.ToLower())

            End With

            Dim dt As New DataTable()
            sDa.SelectCommand = CmdSql
            sDa.Fill(dt)

            Return dt

        Catch ex As Exception

        End Try






        'Dim Dt As New DataTable
        'sDa.Fill(Dt)
        'sDa.Dispose()
        'Return Dt

    End Function

    Public Shared Function GetGeneralTableData(ByVal sSql As String, values As String, Optional intvalue As Integer = 0) As DataTable

        Dim sDa As New NpgsqlDataAdapter()
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Try
            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            With CmdSql
                If intvalue = 0 Then
                    .Parameters.AddWithValue("@val", values)
                Else
                    .Parameters.AddWithValue("@val", Convert.ToInt32(values))
                End If
            End With

            Dim dt As New DataTable()
            sDa.SelectCommand = CmdSql
            sDa.Fill(dt)

            Return dt

        Catch ex As Exception
            SavetoLog("GetGeneralTableData Failed with Error: " & ex.Message)
        End Try

    End Function

    Public Shared Function GetMobileUserTableData(ByVal sSql As String, values As AuthModel) As DataTable

        Dim sDa As New NpgsqlDataAdapter()
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Try
            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            With CmdSql
                .Parameters.AddWithValue("@UName", values.username.ToLower())

            End With

            Dim dt As New DataTable()
            sDa.SelectCommand = CmdSql
            sDa.Fill(dt)

            Return dt

        Catch ex As Exception

        End Try






        'Dim Dt As New DataTable
        'sDa.Fill(Dt)
        'sDa.Dispose()
        'Return Dt

    End Function
    Public Shared Function GetInstStaffTableData(ByVal sSql As String, insid As String) As DataTable

        Dim sDa As New NpgsqlDataAdapter()
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Try
            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            With CmdSql
                .Parameters.AddWithValue("@InsID", insid)

            End With

            Dim dt As New DataTable()
            sDa.SelectCommand = CmdSql
            sDa.Fill(dt)

            Return dt

        Catch ex As Exception
            Return Nothing
        End Try






        'Dim Dt As New DataTable
        'sDa.Fill(Dt)
        'sDa.Dispose()
        'Return Dt

    End Function


    Public Shared Function GetNewID(ByVal Tbl As String, ByVal Fld As String) As Integer
        Dim NewRow As DataRow
        Dim sSQL As String

        GetNewID = -1
        sSQL = "SELECT COUNT(" & Fld & ") AS Co, Max(" & Fld & ") + 1 AS ID FROM " & Tbl
        NewRow = GetRowInfo(sSQL)

        If CType(NewRow("Co"), Integer) = 0 Then
            GetNewID = 0
        Else
            GetNewID = CType(NewRow("ID"), Integer)
        End If

    End Function

    Public Shared Function GetIDByName(ByVal Tbl As String, ByVal fld As String, ByVal fldName As String, ByVal fldVal As String) As String

        Dim sSQL As String
        Dim NewRow As DataRow

        GetIDByName = "0"
        sSQL = "SELECT " & fld & " FROM " & Tbl & " WHERE " & fldName & " = '" & fldVal & "'"

        NewRow = GetRowInfo(sSQL)
        GetIDByName = CType(NewRow(fld), String)

    End Function

    Public Shared Function GetNameByID(ByVal Tbl As String, ByVal fld As String, ByVal fldName As String, ByVal fldVal As String) As String

        Dim sSQL As String
        Dim NewRow As DataRow

        GetNameByID = ""
        sSQL = "SELECT " & fldName & " FROM " & Tbl & " WHERE " & fld & " = " & fldVal
        NewRow = GetRowInfo(sSQL)
        GetNameByID = CType(NewRow(fldName), String)

    End Function
    Public Shared Sub DbRunAll(ByVal sSql As String)


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSql, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With
        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            Throw ex
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try


        Exit Sub


    End Sub
    'SaveNewUser

    Public Shared Function SaveNewUser(ByVal sVal As Users, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)

        Dim userGuid As Guid = System.Guid.NewGuid()

        ' Hash the password together with our unique userGuid
        Dim sec As New Security
        Dim hashedPassword As String = sec.HashSHA1(sVal.Password & userGuid.ToString())

        'RETURNING *

        sSQL = "INSERT INTO Users(FirstName, LastName, UserName, Password, UserGuid, Email, Role, EnteredBy, DateEntered, Status, Branch, Locked, ExpiringDate, LastLogin, FirstTimeLogin, MobileNo,usertype) VALUES (@FirstName, @LastName, @UserName, @Password, @UserGuid, @Email, @Role, @EnteredBy, @DateEntered, @Status, @Branch, @Locked, @ExpiringDate, @LastLogin, @FirstTimeLogin, @MobileNo,@usertype) RETURNING id,FirstName, LastName, UserName, Email, Role, EnteredBy, DateEntered, Status, Branch, Locked, ExpiringDate, LastLogin, FirstTimeLogin, MobileNo,usertype"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@FirstName", sVal.FirstName)
                .Parameters.AddWithValue("@LastName", sVal.LastName)
                .Parameters.AddWithValue("@UserName", sVal.UserName.ToLower())
                .Parameters.AddWithValue("@Password", hashedPassword)
                .Parameters.AddWithValue("@UserGuid", userGuid)
                .Parameters.AddWithValue("@Email", sVal.Email)
                .Parameters.AddWithValue("@Role", sVal.Role)
                .Parameters.AddWithValue("@DateEntered", DateTime.Now)
                .Parameters.AddWithValue("@EnteredBy", sVal.EnteredBy)
                .Parameters.AddWithValue("@Status", True)
                .Parameters.AddWithValue("@Branch", sVal.Branch)
                .Parameters.AddWithValue("@Locked", False)
                .Parameters.AddWithValue("@ExpiringDate", CDate(DateTime.Now.ToString("yyyy-MM-dd")))
                .Parameters.AddWithValue("@LastLogin", CDate(DateTime.Now.ToString("yyyy-MM-dd")))
                .Parameters.AddWithValue("@FirstTimeLogin", True)
                .Parameters.AddWithValue("@MobileNo", sVal.MobileNo)
                .Parameters.AddWithValue("@usertype", sVal.usertype)


                .Connection.Open()
                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "New User  Added Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()


                'Make and Audit Trail

                Dim mesage As String = (username + " Created User " + sVal.UserName + " as role " + sVal.Role + " for " + sVal.Branch & " onto the system ")
                Dim log As String = "Create New User"


                AuditLogs(username, mesage, DBConnection.DbConString, log)


            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveNewUser : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function


    Public Shared Function SaveNewRole(ByVal sVal As Roles, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)



        'RETURNING *

        sSQL = "INSERT INTO Roles(RoleName, Description) VALUES (@RoleName, @Description) RETURNING *"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@RoleName", sVal.RoleName)
                .Parameters.AddWithValue("@Description", sVal.Description)

                .Connection.Open()
                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "New Role Added Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()


                'Make and Audit Trail

                Dim mesage As String = (username + " Created Role " & sVal.RoleName & " onto the system ")
                Dim log As String = "Create New Role"


                AuditLogs(username, mesage, DBConnection.DbConString, log)


            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveNewRole : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function SaveRolePermissions(username As String, Permission As String, Status As Boolean, Role As String) As Boolean


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)



        'RETURNING *

        sSQL = "INSERT INTO  RolePermissions(Permission, Status, Role) VALUES (@Permission, @Status, @Role)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@Permission", Permission)
                .Parameters.AddWithValue("@Status", Status)
                .Parameters.AddWithValue("@Role", Role)

                .Connection.Open()
                .ExecuteNonQuery()


                'Message.Message = "New Role Added Successfully"
                'Message.Payload = "New Role Added Successfully"

                .Connection.Close()
                .Dispose()


                'Make and Audit Trail

                Dim mesage As String = (username + " Created Permission " & Permission & " For Role" & Role & "onto the system ")
                Dim log As String = "Create New RolePermission"


                AuditLogs(username, mesage, DBConnection.DbConString, log)


            End With

            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveRolePermissions : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function


    Public Shared Function UpdateRolePermissions(username As String, Permission As String, Status As Boolean, Role As String, id As Integer) As Boolean


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)



        'RETURNING *

        sSQL = "UPDATE RolePermissions SET Status = @Status WHERE id = @id"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                ' .Parameters.AddWithValue("@Permission", Permission)
                .Parameters.AddWithValue("@Status", Status)
                .Parameters.AddWithValue("@id", id)
                ' .Parameters.AddWithValue("@Role", Role)

                .Connection.Open()
                Dim ir As Integer = .ExecuteNonQuery()


                'Message.Message = "New Role Added Successfully"
                'Message.Payload = "New Role Added Successfully"

                .Connection.Close()
                .Dispose()


                'Make and Audit Trail

                Dim mesage As String = (username + " Updated Permission " & Permission & " For Role" & Role & "onto the system ")
                Dim log As String = "Updated  RolePermission"


                AuditLogs(username, mesage, DBConnection.DbConString, log)


            End With

            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateRolePermissions : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function


    Public Shared Function SaveFI(ByVal sVal As FIModel) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        'RETURNING *

        sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address,LoanLimit, DateRegistered, RegisteredBy, Status,emailaddress) VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status,@Emailaddress) RETURNING *"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                .Parameters.AddWithValue("@Address", sVal.Address)
                .Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                .Parameters.AddWithValue("@Telephone", sVal.Telephone)
                .Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                .Parameters.AddWithValue("@Location", sVal.Location)
                .Parameters.AddWithValue("@Status", True)
                .Parameters.AddWithValue("@CompanyNo", sVal.CompanyNo)
                .Parameters.AddWithValue("@DateRegistered", CDate(DateTime.Now.ToString("yyyy-MM-dd")))
                .Parameters.AddWithValue("@RegisteredBy", sVal.RegisteredBy)
                .Parameters.AddWithValue("@Emailaddress", sVal.Emailaddress)
                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Financial Institution Added Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If




                .Connection.Close()
                .Dispose()
            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveFI : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function
    Public Shared Function SaveBank(ByVal sVal As Banks, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)

        sSQL = "INSERT INTO Banks(BankNo, BankName, AccountNo, Details, AccountName, DateCreated, CreatedBy, Status) VALUES (@BankNo, @BankName, @AccountNo, @Details, @AccountName, @DateCreated, @CreatedBy, @Status) RETURNING *"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@BankNo", sVal.BankNo)
                .Parameters.AddWithValue("@BankName", sVal.BankName)
                .Parameters.AddWithValue("@AccountNo", sVal.AccountNo)
                .Parameters.AddWithValue("@Details", sVal.Details)
                .Parameters.AddWithValue("@AccountName", sVal.AccountName)
                .Parameters.AddWithValue("@DateCreated", CDate(DateTime.Now.ToString("yyyy-MM-dd")))
                .Parameters.AddWithValue("@CreatedBy", username)
                .Parameters.AddWithValue("@Status", True)

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Bank Added Successfully"
                        Message.Payload = JsonForDataReader(reader)



                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Make and Audit Trail

                Dim mesage As String = (username + "  Successfully Added a Bank " & sVal.BankName & "(" & sVal.BankNo & ") onto the system ")
                Dim log As String = "Create Bank"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveBank : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function SaveStaff(ByVal sVal As Staffs, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)

        Dim userGuid As Guid = System.Guid.NewGuid()

        ' Hash the password together with our unique userGuid
        Dim sec As New Security
        Dim hashedPassword As String = sec.HashSHA1(sVal.Password & userGuid.ToString())




        sSQL = "INSERT INTO Staffs(Name, Gender, DOB, MobileNo, Email, CompanyName, Location, NetSalary, SSNITNo, SalaryAccountNo, BankName, Branch, AccountNo, UserName, Password, UserGuid, Status, RegisteredStatus,IDCard,Picture,Signature,IDType,IDNO,StaffID) VALUES (@Name, @Gender, @DOB, @MobileNo, @Email, @CompanyName, @Location, @NetSalary, @SSNITNo, @SalaryAccountNo, @BankName, @Branch, @AccountNo, @UserName, @Password, @UserGuid, @Status, @RegisteredStatus,@IDCard,@Picture,@Signature,@IDType,@IDNO,@StaffID) RETURNING id, Name, Gender, DOB, MobileNo, Email, CompanyName, Location, NetSalary, SSNITNo, SalaryAccountNo, BankName, Branch, AccountNo, UserName, Status, RegisteredStatus, IDCard, Picture, Signature, IDType, IDNO, StaffID"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@StaffID", username)
                .Parameters.AddWithValue("@Name", sVal.Name)
                .Parameters.AddWithValue("@Gender", sVal.Gender)
                .Parameters.AddWithValue("@DOB", sVal.DOB)
                .Parameters.AddWithValue("@MobileNo", sVal.MobileNo)
                .Parameters.AddWithValue("@Email", sVal.Email)
                .Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                .Parameters.AddWithValue("@Location", sVal.Location)
                .Parameters.AddWithValue("@NetSalary", sVal.NetSalary)
                .Parameters.AddWithValue("@IDType", sVal.IDType)
                .Parameters.AddWithValue("@IDNO", sVal.IDNO)

                .Parameters.AddWithValue("@SSNITNo", sVal.SSNITNo)
                .Parameters.AddWithValue("@SalaryAccountNo", sVal.SalaryAccountNo)
                .Parameters.AddWithValue("@BankName", sVal.BankName)
                .Parameters.AddWithValue("@Branch", sVal.Branch)
                .Parameters.AddWithValue("@AccountNo", sVal.AccountNo)
                .Parameters.AddWithValue("@UserName", sVal.UserName)
                .Parameters.AddWithValue("@Password", hashedPassword)
                .Parameters.AddWithValue("@UserGuid", userGuid)
                .Parameters.AddWithValue("@Status", sVal.Status)
                .Parameters.AddWithValue("@RegisteredStatus", sVal.Status)


                If (sVal.IDCard) IsNot Nothing Then

                    .Parameters.AddWithValue("@IDCard", sVal.IDCard)


                Else
                    .Parameters.Add("@IDCard", NpgsqlTypes.NpgsqlDbType.Bytea).Value = DBNull.Value
                    ' .Parameters.AddWithValue("@IDCard", DBNull.Value)
                End If


                If (sVal.Picture) IsNot Nothing Then

                    .Parameters.AddWithValue("@Picture", sVal.Picture)

                Else
                    ' .Parameters.AddWithValue("@Picture", DBNull.Value)
                    .Parameters.Add("@Picture", NpgsqlTypes.NpgsqlDbType.Bytea).Value = DBNull.Value
                End If

                If sVal.Signature IsNot Nothing Then

                    .Parameters.AddWithValue("@Signature", sVal.Signature)

                Else
                    ' .Parameters.AddWithValue("@Signature", DBNull.Value)
                    .Parameters.Add("@Signature", NpgsqlTypes.NpgsqlDbType.Bytea).Value = DBNull.Value
                End If



                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable To Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Staff Added Successfully"
                        Message.Payload = JsonForDataReader(reader)



                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Make and Audit Trail

                Dim mesage As String = (username + "  Successfully Added a Staff " & sVal.Name & "(" & sVal.UserName & ") onto the system ")
                Dim log As String = "Create Staff"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable To Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveStaff : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function
    Public Shared Function SaveConfiguration(ByVal sVal As Configurations, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        'RETURNING *

        sSQL = "INSERT INTO Configurators (Name, Value) VALUES (@Name, @Value) RETURNING *"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@Name", sVal.Name)
                .Parameters.AddWithValue("@Value", sVal.Value)

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Configuration Added Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Make and Audit Trail

                Dim mesage As String = (username + "  Successfully Added  Configuration " & sVal.Name & " onto the system ")
                Dim log As String = "Create Configuration"


                AuditLogs(username, mesage, DBConnection.DbConString, log)
            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveConfiguration : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function
    Public Shared Function SaveInstitution(ByVal sVal As Institutions, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)

        'Dim userGuid As Guid = System.Guid.NewGuid()

        ' Hash the password together with our unique userGuid



        sSQL = "INSERT INTO Institutions(Name, Address, Phone, Location, Status, InsNo, DateRegistered, BusinessType, ContactPerson, ContactEmail) VALUES (@Name, @Address, @Phone, @Location, @Status, @InsNo, @DateRegistered, @BusinessType, @ContactPerson, @ContactEmail) RETURNING * "
        'sSQL = "INSERT INTO Staffs(Name, Gender, DOB, MobileNo, Email, CompanyName, Location, NetSalary, SSNITNo, SalaryAccountNo, BankName, Branch, AccountNo, UserName, Password, UserGuid, Status, RegisteredStatus,IDCard,Picture,Signature,IDType,IDNO,StaffID) OUTPUT inserted.id, inserted.Name, inserted.Gender, inserted.DOB, inserted.MobileNo, inserted.Email, inserted.CompanyName, inserted.Location, inserted.NetSalary, inserted.SSNITNo, inserted.SalaryAccountNo, inserted.BankName, inserted.Branch, inserted.AccountNo, inserted.UserName, inserted.Status, inserted.RegisteredStatus, inserted.IDCard, inserted.Picture, inserted.Signature, inserted.IDType, inserted.IDNO, inserted.StaffID VALUES (@Name, @Gender, @DOB, @MobileNo, @Email, @CompanyName, @Location, @NetSalary, @SSNITNo, @SalaryAccountNo, @BankName, @Branch, @AccountNo, @UserName, @Password, @UserGuid, @Status, @RegisteredStatus,@IDCard,@Picture,@Signature,@IDType,@IDNO,@StaffID)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@Name", sVal.Name)
                .Parameters.AddWithValue("@Address", sVal.Address)
                .Parameters.AddWithValue("@Phone", sVal.Phone)
                .Parameters.AddWithValue("@Location", sVal.Location)
                .Parameters.AddWithValue("@Status", sVal.Status)
                .Parameters.AddWithValue("@InsNo", sVal.InsNo)
                .Parameters.AddWithValue("@DateRegistered", CDate(DateTime.Now))
                .Parameters.AddWithValue("@BusinessType", sVal.BusinessType)
                .Parameters.AddWithValue("@ContactPerson", sVal.ContactPerson)
                .Parameters.AddWithValue("@ContactEmail", sVal.ContactEmail)

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable To Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Institution Added Successfully"
                        Message.Payload = JsonForDataReader(reader)



                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Make and Audit Trail

                Dim mesage As String = (username + "  Successfully Added an Institution " & sVal.Name & "(" & username & ") onto the system ")
                Dim log As String = "Create Institution"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable To Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From SaveInstitution : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function
    'UpdateUser

    Public Shared Function UpdateUser(ByVal sVal As Users, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "UPDATE Users SET FirstName=@FirstName, LastName=@LastName, Email = @Email, Role = @Role, Branch=@Branch, Status=@Status, Locked=@Locked ,MobileNo=@MobileNo,usertype=@usertype WHERE UserName = '" & sVal.UserName & "' RETURNING id,FirstName, LastName, UserName, Email, Role, EnteredBy, DateEntered, Status, Branch, Locked, ExpiringDate, LastLogin, FirstTimeLogin, MobileNo,usertype"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@FirstName", sVal.FirstName)
                .Parameters.AddWithValue("@LastName", sVal.LastName)
                .Parameters.AddWithValue("@UserName", sVal.UserName.ToLower())
                .Parameters.AddWithValue("@Email", sVal.Email)
                .Parameters.AddWithValue("@Role", sVal.Role)
                .Parameters.AddWithValue("@Status", True)
                .Parameters.AddWithValue("@Branch", sVal.Branch)
                .Parameters.AddWithValue("@Locked", False)
                .Parameters.AddWithValue("@MobileNo", sVal.MobileNo)
                .Parameters.AddWithValue("@usertype", sVal.usertype)

                .Connection.Open()
                reader = .ExecuteReader()


                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "User Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully updated User " & sVal.UserName & " onto the system ")
                Dim log As String = "Update User"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateUser : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function UpdateRoles(ByVal sVal As Roles, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "UPDATE Roles SET  Description=@Description WHERE RoleName = @RoleName RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@RoleName", sVal.RoleName)
                .Parameters.AddWithValue("@Description", sVal.Description)

                .Connection.Open()
                reader = .ExecuteReader()


                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Role Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()




                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully updated Role to " & sVal.RoleName & " onto the system ")
                Dim log As String = "Update Role"


                AuditLogs(username, mesage, DBConnection.DbConString, log)



                ''Update same role in the rolepermissions table

                ' UpdateRolePermissionsRolename(username, sVal.RoleName)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateRoles : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function DeleteRolePermissions(username As String, Role As String) As Boolean


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)



        'RETURNING *

        sSQL = "DELETE FROM RolePermissions WHERE Role = @Role"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                '.Parameters.AddWithValue("@Permission", Permission)
                ' .Parameters.AddWithValue("@Status", Status)
                .Parameters.AddWithValue("@Role", Role)

                .Connection.Open()
                .ExecuteNonQuery()


                'Message.Message = "New Role Added Successfully"
                'Message.Payload = "New Role Added Successfully"

                .Connection.Close()
                .Dispose()


                'Make and Audit Trail

                Dim mesage As String = (username + " Deleted RolePermission Role Name" & Role & "onto the system ")
                Dim log As String = "Delete  RolePermission Rolename"


                AuditLogs(username, mesage, DBConnection.DbConString, log)


            End With

            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteRolePermissions : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function UpdateFI(ByVal sVal As FIModel, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "update FIs set CompanyName=@CompanyName, Location=@Location, Telephone=@Telephone, PhoneNo=@PhoneNo, Address=@Address, LoanLimit=@LoanLimit, Status=@Status, emailaddress=@emailaddress  WHERE CompanyNo='" + sVal.CompanyNo + "' RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                .Parameters.AddWithValue("@Address", sVal.Address)
                .Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                .Parameters.AddWithValue("@Telephone", sVal.Telephone)
                .Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                .Parameters.AddWithValue("@Location", sVal.Location)
                .Parameters.AddWithValue("@Status", sVal.Status)
                .Parameters.AddWithValue("@emailaddress", sVal.Emailaddress)
                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Financial Institution Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + " Successfully updated FI " & sVal.CompanyName & "(" & sVal.CompanyNo & ") onto the system ")
                Dim log As String = "Update FI"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateFI : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function


    Public Shared Function UpdateConfigurations(ByVal sVal As Configurations, username As String) As GeneralResponds


        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "UPDATE Configurators SET Value = @Value WHERE id = @id RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@Value", sVal.Value)
                .Parameters.AddWithValue("@id", sVal.id)


                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Configuration Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully updated Configuration " & sVal.Name & "(" & sVal.Value & ") onto the system ")
                Dim log As String = "Update Configuration"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateFI : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function UpdateBank(ByVal sVal As Banks, username As String) As GeneralResponds


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "UPDATE Banks set BankName=@BankName, AccountNo=@AccountNo, Details=@Details, AccountName=@AccountName, Status=@Status WHERE BankNo='" + sVal.BankNo + "' RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@BankName", sVal.BankName)
                .Parameters.AddWithValue("@AccountNo", sVal.AccountNo)
                .Parameters.AddWithValue("@Details", sVal.Details)
                .Parameters.AddWithValue("@AccountName", sVal.AccountName)
                .Parameters.AddWithValue("@Status", sVal.Status)

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Bank Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully updated Bank " & sVal.BankName & "(" & sVal.BankNo & ") onto the system ")
                Dim log As String = "Update Bank"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateBank : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function UpdateStaff(ByVal sVal As Staffs, username As String) As GeneralResponds


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds


        DbConn = New NpgsqlConnection(DbConString)
        'sSQL = "UPDATE Banks set BankName=@BankName, AccountNo=@AccountNo, Details=@Details, AccountName=@AccountName, Status=@Status OUTPUT inserted.BankNo, inserted.BankName, inserted.AccountNo, inserted.Details, inserted.AccountName, inserted.DateCreated, inserted.CreatedBy, inserted.Status WHERE BankNo='" + sVal.BankNo + "'"
        sSQL = "UPDATE Staffs set Name=@Name , Gender=@Gender , DOB=@DOB , MobileNo=@MobileNo , Email=@Email , CompanyName=@CompanyName , Location=@Location , NetSalary=@NetSalary , SSNITNo=@SSNITNo , SalaryAccountNo=@SalaryAccountNo , BankName=@BankName , Branch=@Branch , AccountNo=@AccountNo , UserName=@UserName , Status=@Status , RegisteredStatus=@RegisteredStatus ,IDCard=@IDCard ,Picture=@Picture ,Signature=@Signature ,IDType=@IDType ,IDNO=@IDNO,StaffID=@StaffID WHERE id=" & sVal.id & " RETURNING id, Name, Gender, DOB, MobileNo, Email, CompanyName, Location, NetSalary, SSNITNo, SalaryAccountNo, BankName, Branch, AccountNo, UserName, Status, RegisteredStatus, IDCard, Picture, Signature, IDType, IDNO, StaffID"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@StaffID", username)
                .Parameters.AddWithValue("@Name", sVal.Name)
                .Parameters.AddWithValue("@Gender", sVal.Gender)
                .Parameters.AddWithValue("@DOB", sVal.DOB)
                .Parameters.AddWithValue("@MobileNo", sVal.MobileNo)
                .Parameters.AddWithValue("@Email", sVal.Email)
                .Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                .Parameters.AddWithValue("@Location", sVal.Location)
                .Parameters.AddWithValue("@NetSalary", sVal.NetSalary)
                .Parameters.AddWithValue("@IDType", sVal.IDType)
                .Parameters.AddWithValue("@IDNO", sVal.IDNO)

                .Parameters.AddWithValue("@SSNITNo", sVal.SSNITNo)
                .Parameters.AddWithValue("@SalaryAccountNo", sVal.SalaryAccountNo)
                .Parameters.AddWithValue("@BankName", sVal.BankName)
                .Parameters.AddWithValue("@Branch", sVal.Branch)
                .Parameters.AddWithValue("@AccountNo", sVal.AccountNo)
                .Parameters.AddWithValue("@UserName", sVal.UserName)

                .Parameters.AddWithValue("@Status", sVal.Status)
                .Parameters.AddWithValue("@RegisteredStatus", sVal.Status)

                If (sVal.IDCard) IsNot Nothing Then

                    .Parameters.AddWithValue("@IDCard", sVal.IDCard)


                Else
                    .Parameters.Add("@IDCard", NpgsqlTypes.NpgsqlDbType.Bytea).Value = DBNull.Value
                    ' .Parameters.AddWithValue("@IDCard", DBNull.Value)
                End If


                If (sVal.Picture) IsNot Nothing Then

                    .Parameters.AddWithValue("@Picture", sVal.Picture)

                Else
                    ' .Parameters.AddWithValue("@Picture", DBNull.Value)
                    .Parameters.Add("@Picture", NpgsqlTypes.NpgsqlDbType.Bytea).Value = DBNull.Value
                End If

                If sVal.Signature IsNot Nothing Then

                    .Parameters.AddWithValue("@Signature", sVal.Signature)

                Else
                    ' .Parameters.AddWithValue("@Signature", DBNull.Value)
                    .Parameters.Add("@Signature", NpgsqlTypes.NpgsqlDbType.Bytea).Value = DBNull.Value
                End If




                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Staff Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully updated Staff " & sVal.Name & "(" & sVal.UserName & ") onto the system ")
                Dim log As String = "Update Staff"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateStaff : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function


    Public Shared Function ApproveRejectBorrower(ByVal sVal As Staffs, username As String) As GeneralResponds


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds


        DbConn = New NpgsqlConnection(DbConString)
        'sSQL = "UPDATE Banks set BankName=@BankName, AccountNo=@AccountNo, Details=@Details, AccountName=@AccountName, Status=@Status OUTPUT inserted.BankNo, inserted.BankName, inserted.AccountNo, inserted.Details, inserted.AccountName, inserted.DateCreated, inserted.CreatedBy, inserted.Status WHERE BankNo='" + sVal.BankNo + "'"
        sSQL = "UPDATE Staffs set Status=@Status WHERE id=" & sVal.id & " RETURNING id, Status"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@Status", sVal.Status)

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Staff Approved/Reject Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Update Staff to with Status " & "(" & sVal.Status & ") onto the system ")
                Dim log As String = "ApproveRejectBorrower Staff"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From ApproveRejectBorrower : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function


    Public Shared Function PaydayApproval(ByVal sSQL As String, username As String, staffID As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username + " Approved Pay day Loan Loan for Staff with ID " + staffID + " onto the system"
            Dim log As String = "Payday Loan Approval"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From PaydayApproval : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function

    Public Shared Function InstLoanApproval(ByVal sSQL As String, username As String, staffID As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username + " Approved Loan with ID " + staffID + " onto the system"
            Dim log As String = "Institution Loan Approval"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From InstLoanApproval : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try



    End Function

    Public Shared Function LoanAllocations(ByVal sSQL As String, username As String, id As String, fid As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql
                .Parameters.AddWithValue("@FI", fid)
                .Parameters.AddWithValue("@DateProcessed", DateTime.Now)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Allocated Loan with  ID " & id & " onto the system"
            Dim log As String = "Loan Allocation"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From InstLoanApproval : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function
    Public Shared Function Callback(ByVal sSQL As String, username As String, refno As String, Optional success As Boolean = True) As Boolean

        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@DateDisbursed", DateTime.Now)
                '.Parameters.AddWithValue("@InsRepaymentBy", username)
                '.Parameters.AddWithValue("@RepaymentBankID", Bank)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String
            Dim log As String
            If success = False Then
                mesage = username & " Loan Disbursement Request Failed Loan for RefNO: " + refno + " onto the system"
                log = "Loan Disbursement"

            Else
                mesage = username & " Successfully confirmed Disbursement of Loan RefNO: " + refno + " onto the system"
                log = "Loan Disbursement"
            End If

            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From Callback : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function
    Public Shared Function MOMODisbursement(ByVal sSQL As String, username As String, staffid As String, amount As String, Optional manual As Boolean = False) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@DateDisbursed", DateTime.Now)
                '.Parameters.AddWithValue("@InsRepaymentBy", username)
                '.Parameters.AddWithValue("@RepaymentBankID", Bank)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String
            Dim log As String
            If manual = False Then
                mesage = username & "  Loan Disbursement Request sent Loan for " + staffid + " with amount " + amount + " onto the system"
                log = "Loan Disbursement"

            Else
                mesage = username & " Successfully confirmed Disbursement of Loan for " + staffid + " with amount " + amount + " onto the system"
                log = "Loan Disbursement"
            End If

            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From MOMODisbursement : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function

    'InsRepaymentDate=@InsRepaymentDate, InsRepaymentBy=@InsRepaymentBy, RepaymentBankID=@RepaymentBankID 
    Public Shared Function InsLoanRePayment(ByVal sSQL As String, username As String, id As String, staffid As String, amount As String, bank As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@InsRepaymentDate", DateTime.Now)
                .Parameters.AddWithValue("@InsRepaymentBy", username)
                .Parameters.AddWithValue("@RepaymentBankID", bank)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Repaid Loan for " + staffid + " with amount " + amount + " onto the system"
            Dim log As String = "Institution Loan Repayment"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From InsLoanRePayment : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function


    Public Shared Function LoanPaymentConfirmation(ByVal sSQL As String, username As String, id As String, company As String, amount As String, bank As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@DatePayed", DateTime.Now)
                .Parameters.AddWithValue("@PaymentBy", username)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Confirmed Loan Payment for " & company + " with amount " & amount + " at " + bank + " onto the system"
            Dim log As String = "Loan Payment Confirmation"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From LoanPaymentConfirmation : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function
    '"update SalaryLoan set Status='5000', DateAcknowledged=@DateAcknowledged, AcknowledgedBy=@AcknowledgedBy, BankID=@BankID WHERE ID= = @id
    Public Shared Function FILoanPayment(ByVal sSQL As String, username As String, id As Integer, staffid As String, amount As String, bankid As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@id", id)
                .Parameters.AddWithValue("@DateAcknowledged", DateTime.Now)
                .Parameters.AddWithValue("@AcknowledgedBy", username)
                .Parameters.AddWithValue("@BankID", bankid)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Acknowledged Loan for " + staffid + " with amount " + amount + " onto the system"
            Dim log As String = "Loan Acknowledgement"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From FILoanPayment : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function

    Public Shared Function FILoanConfirmation(ByVal sSQL As String, username As String, id As Integer, staffid As String, amount As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@id", id)
                .Parameters.AddWithValue("@DateConfirmed", DateTime.Now)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Confirmed Loan for " + staffid & " with amount " & amount & " onto the system"
            Dim log As String = "Loan Confirmation"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From FILoanConfirmation : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function


    'FIRePaymentBy=@FIRePaymentBy, FIRepaymentDate=@FIRepaymentDate, Details=@Details 
    Public Shared Function LoanRePaymentConfirmation(ByVal sSQL As String, username As String, id As String, company As String, amount As String, bank As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@RepaidConfirmedDate", DateTime.Now)
                .Parameters.AddWithValue("@RepaidConfirmedBy", username)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Confirmed Loan RePayment for " & company + " with amount " & amount + " at " + bank + " onto the system"
            Dim log As String = "Loan Payment Confirmation"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From LoanRePaymentConfirmation : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function

    Public Shared Function LoanPaid(ByVal sSQL As String, username As String, id As String, StaffID As String, amount As String, Details As String) As Boolean


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Dim Message As New GeneralResponds

        DbConn = New NpgsqlConnection(DbConString)
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@FIRepaymentDate", DateTime.Now)
                .Parameters.AddWithValue("@FIRePaymentBy", username)
                .Parameters.AddWithValue("@Details", Details)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                .Dispose()
            End With


            'Audit Trail Entry
            Dim mesage As String = username & " Successfully Processed Loan RePayment to FI for " + StaffID + " with amount " + amount + " onto the system"
            Dim log As String = "Loan Payment Confirmation"


            AuditLogs(username, mesage, DBConnection.DbConString, log)
            Return True

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From LoanRePaymentConfirmation : " & ex.Message)
            Throw ex
            Return False
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try





    End Function
    Public Shared Function GetReAllocationRecords(ByVal sSql As String, ptime As String, htime As String) As DataTable

        Dim sDa As New NpgsqlDataAdapter()
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand

        Try
            DbConn = New NpgsqlConnection(DbConString)
            CmdSql = New NpgsqlCommand(sSql, DbConn)
            With CmdSql
                ' .Parameters.AddWithValue("@Time", NpgsqlTypes.NpgsqlDbType.Integer, ptime)
                '.Parameters.AddWithValue("@HTime", NpgsqlTypes.NpgsqlDbType.Integer, ptime)

            End With

            Dim dt As New DataTable()
            sDa.SelectCommand = CmdSql
            sDa.Fill(dt)

            Return dt

        Catch ex As Exception

        End Try

    End Function




    'Dim Dt As New DataTable

    Public Shared Function UpdateInstitution(ByVal sVal As Institutions, username As String) As GeneralResponds


        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds


        DbConn = New NpgsqlConnection(DbConString)
        'sSQL = "UPDATE Banks set BankName=@BankName, AccountNo=@AccountNo, Details=@Details, AccountName=@AccountName, Status=@Status OUTPUT inserted.BankNo, inserted.BankName, inserted.AccountNo, inserted.Details, inserted.AccountName, inserted.DateCreated, inserted.CreatedBy, inserted.Status WHERE BankNo='" + sVal.BankNo + "'"
        sSQL = "update Institutions SET Status=@Status, Name=@Name, Address=@Address, Phone=@Phone, Location=@Location, BusinessType=@BusinessType, ContactPerson=@ContactPerson, ContactEmail=@ContactEmail WHERE InsNo='" + sVal.InsNo + "' RETURNING * "
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@Status", sVal.Status)
                .Parameters.AddWithValue("@Name", sVal.Name)
                .Parameters.AddWithValue("@Address", sVal.Address)
                .Parameters.AddWithValue("@Phone", sVal.Phone)
                .Parameters.AddWithValue("@Location", sVal.Location)
                ' //   .Parameters.AddWithValue("@Status", RadComboBoxStatus.SelectedValue)
                ' //  .Parameters.AddWithValue("@InsNo", RadTxtInso.Text)
                ' // .Parameters.AddWithValue("@DateRegistered", DateTime.Now)
                .Parameters.AddWithValue("@BusinessType", sVal.BusinessType)
                .Parameters.AddWithValue("@ContactPerson", sVal.ContactPerson)
                .Parameters.AddWithValue("@ContactEmail", sVal.ContactEmail)



                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Institution Updated Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully updated Institution " & sVal.Name & "(" & username & ") onto the system ")
                Dim log As String = "Update Institutions"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From UpdateInstitutions : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function
    'DeleteUser
    Public Shared Function DeleteUser(ByVal sVal As Users, username As String) As GeneralResponds

        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "DELETE FROM Users WHERE UserName ='" & sVal.UserName & "' RETURNING id,FirstName, LastName, UserName, Email, Role, EnteredBy, DateEntered, Status, Branch, Locked, ExpiringDate, LastLogin, FirstTimeLogin, MobileNo"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                '.Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                '.Parameters.AddWithValue("@Address", sVal.Address)
                '.Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                '.Parameters.AddWithValue("@Telephone", sVal.Telephone)
                '.Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                '.Parameters.AddWithValue("@Location", sVal.Location)
                '.Parameters.AddWithValue("@Status", "True")

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "User Deleted Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Deleted User " & sVal.UserName & " onto the system ")
                Dim log As String = "Delete User"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteUser : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function DeleteRoles(ByVal sVal As Roles, username As String) As GeneralResponds

        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "DELETE FROM Roles WHERE RoleName = @RoleName RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                .Parameters.AddWithValue("@RoleName", sVal.RoleName)
                '.Parameters.AddWithValue("@Address", sVal.Address)
                '.Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                '.Parameters.AddWithValue("@Telephone", sVal.Telephone)
                '.Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                '.Parameters.AddWithValue("@Location", sVal.Location)
                '.Parameters.AddWithValue("@Status", "True")

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Roles Deleted Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Deleted User " & sVal.RoleName & " onto the system ")
                Dim log As String = "Delete User"


                AuditLogs(username, mesage, DBConnection.DbConString, log)



                'Delete Recorrespinding entries in rolepermission table
                If DeleteRolePermissions(username, sVal.RoleName) = False Then
                    Message.Message = ""
                    Return Message

                End If

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteRoles : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function



    Public Shared Function DeleteFI(ByVal sVal As FIModel, username As String) As GeneralResponds

        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "Delete from  FIs WHERE CompanyNo='" + sVal.CompanyNo + "' RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                '.Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                '.Parameters.AddWithValue("@Address", sVal.Address)
                '.Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                '.Parameters.AddWithValue("@Telephone", sVal.Telephone)
                '.Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                '.Parameters.AddWithValue("@Location", sVal.Location)
                '.Parameters.AddWithValue("@Status", "True")

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Financial Institution Deleted Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Deleted FI " & sVal.CompanyName & "(" & sVal.CompanyNo & ") onto the system ")
                Dim log As String = "Delete FI"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteFi : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function DeleteBank(ByVal sVal As Banks, username As String) As GeneralResponds

        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "Delete from  Banks  WHERE BankNo='" + sVal.BankNo + "' RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                '.Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                '.Parameters.AddWithValue("@Address", sVal.Address)
                '.Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                '.Parameters.AddWithValue("@Telephone", sVal.Telephone)
                '.Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                '.Parameters.AddWithValue("@Location", sVal.Location)
                '.Parameters.AddWithValue("@Status", "True")

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Bank Deleted Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Deleted Bank " & sVal.BankNo & "(" & sVal.BankName & ") onto the system ")
                Dim log As String = "Delete Bank"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteBank : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function
    Public Shared Function DeleteStaff(ByVal sVal As Staffs, username As String) As GeneralResponds

        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "Delete from  Staffs WHERE id=" & sVal.id & " RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                '.Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                '.Parameters.AddWithValue("@Address", sVal.Address)
                '.Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                '.Parameters.AddWithValue("@Telephone", sVal.Telephone)
                '.Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                '.Parameters.AddWithValue("@Location", sVal.Location)
                '.Parameters.AddWithValue("@Status", "True")

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Staff Deleted Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Deleted Staff " & sVal.StaffID & "(" & sVal.Name & ") onto the system ")
                Dim log As String = "Delete Staff"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteBank : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function

    Public Shared Function DeleteInstitution(ByVal sVal As Institutions, username As String) As GeneralResponds

        'save with picture
        Dim DbConn As NpgsqlConnection
        Dim CmdSql As NpgsqlCommand
        Dim reader As NpgsqlDataReader
        Dim sSQL As String
        Dim Message As New GeneralResponds



        DbConn = New NpgsqlConnection(DbConString)
        sSQL = "Delete from  Institutions WHERE InsNo='" & sVal.InsNo & "' RETURNING *"
        ' sSQL = "INSERT INTO FIs(CompanyNo, CompanyName, Location, Telephone, PhoneNo, Address, LoanLimit, DateRegistered, RegisteredBy, Status) OUTPUT inserted.CompanyNo, inserted.CompanyName, inserted.Location, inserted.Telephone, inserted.PhoneNo, inserted.Address, inserted.LoanLimit, inserted.DateRegistered, inserted.RegisteredBy, inserted.Status VALUES (@CompanyNo, @CompanyName, @Location, @Telephone, @PhoneNo, @Address, @LoanLimit, @DateRegistered, @RegisteredBy, @Status)"
        CmdSql = New NpgsqlCommand(sSQL, DbConn)
        'Pic = AppDomain.CurrentDomain.BaseDirectory & "\images\personal.png"
        Try
            With CmdSql

                '.Parameters.AddWithValue("@CompanyName", sVal.CompanyName)
                '.Parameters.AddWithValue("@Address", sVal.Address)
                '.Parameters.AddWithValue("@PhoneNo", sVal.PhoneNo)
                '.Parameters.AddWithValue("@Telephone", sVal.Telephone)
                '.Parameters.AddWithValue("@LoanLimit", sVal.LoanLimit)
                '.Parameters.AddWithValue("@Location", sVal.Location)
                '.Parameters.AddWithValue("@Status", "True")

                .Connection.Open()

                reader = .ExecuteReader()



                If reader Is Nothing Then

                    'Message.Message = "Unable to Save Records"
                    'Message.Payload = "Internal Error"

                    'Return BadRequest(ReturnJSONResults(Message))
                    Message.Message = ""
                    Return Message
                Else

                    If reader.HasRows = False Then


                        Message.Message = ""
                        Return Message
                    Else

                        Message.Message = "Institution Deleted Successfully"
                        Message.Payload = JsonForDataReader(reader)


                    End If

                End If

                .Connection.Close()
                .Dispose()

                'Audit Trail Entry
                Dim mesage As String = (username + "  Successfully Deleted Institution " & sVal.Name & "(" & sVal.InsNo & ") onto the system ")
                Dim log As String = "Delete Institution"


                AuditLogs(username, mesage, DBConnection.DbConString, log)

            End With

            Return Message

        Catch ex As Exception
            ' MsgBox("Unable to Complete Request . " & ex.Message)  'New Exception("Data Entry Error")
            SavetoLog("Error From DeleteInstitution : " & ex.Message)
            Throw ex
            Return Nothing
        Finally
            DbConn.Close()
            DbConn.Dispose()
        End Try




    End Function



    Public Shared Function CheckStatus(values As AuthModel) As StructCheckStatus
        Dim conn As NpgsqlConnection = New NpgsqlConnection(DbConString)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand("SELECT (Username)As Name, Status, Locked, (FirstTimeLogin)As FF from Users where UserName = '" _
                        & (values.username.ToLower & ("' UNION SELECT (StaffID)As Name, Status, Locked, (FirstLogin)As FF from Staffs where StaffID = '" _
                        & (values.username.ToLower & "'").ToLower)), conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader
        Dim mycheckstatus As New StructCheckStatus
        While dr.Read
            mycheckstatus.UserStatus = dr("Status").ToString
            mycheckstatus.UserLocked = dr("Locked").ToString
            mycheckstatus.UserFirstTime = dr("FF").ToString
            '       TxtExpireDate.Text = dr"ExpiringDate"].ToString();

        End While

        dr.Close()
        conn.Close()

        Return mycheckstatus
    End Function

    Public Shared Function CheckInstStatus(InstID As String) As StructCheckInstitutionStatus
        Dim conn As NpgsqlConnection = New NpgsqlConnection(DbConString)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand("Select Status from Institutions Where InsNo = '" _
                        & (InstID & ("' UNION Select Status from FIs where CompanyNo = '" _
                        & (InstID & ("' UNION Select Status from Lenders where IDNO = '" _
                        & (InstID & "'"))))), conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader
        Dim myCheckInstStatus As New StructCheckInstitutionStatus
        While dr.Read
            myCheckInstStatus.InstStatus = dr("Status").ToString

        End While

        dr.Close()
        conn.Close()

        Return myCheckInstStatus
    End Function

    Public Shared Function UserTypeContent(values As AuthModel) As StructUserTypeContent

        Dim conn As NpgsqlConnection = New NpgsqlConnection(DbConString)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand("SELECT Role, (Branch)AS BB from Users where UserName = @UCode UNION select Role, (InsID) AS BB from S" &
            "taffs where StaffID = @UCode ", conn)
        cmd.Parameters.AddWithValue("@UCode", values.username.ToLower)
        cmd.Parameters.AddWithValue("@pw", values.password)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader
        Dim myUserTypeContent As New StructUserTypeContent
        While dr.Read
            myUserTypeContent.UserRole = dr("Role").ToString
            myUserTypeContent.UserBranch = dr("BB").ToString
            myUserTypeContent.UserInstID = dr("BB").ToString

        End While

        conn.Close()
        Return myUserTypeContent
    End Function

End Class

