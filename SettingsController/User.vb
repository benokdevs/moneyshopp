﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class User
    Inherits ControllerBase

    <HttpGet>
    <Route("/admin/settings/usertypes")>
    Public Function GetUserTypes(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From usertypes"
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select *, count(*) OVER() AS full_count  From usertypes ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Users Types"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get usertypes" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpGet>
    <Route("/admin/settings/users")>
    Public Function GetUsers(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From users"
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select *, count(*) OVER() AS full_count  From users ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Users Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Users" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpPost>
    <Route("/admin/settings/users/filter")>
    Public Function GeUsersFilter(<FromBody()> ByVal values As UsersFilter, <FromQuery()> ByVal qvals As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)


                'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
                sSQL = "Select * , count(*) OVER() AS full_count From Users"

                '              Public Property UserName As String
                'Public Property FirstName As String
                'Public Property LastName As String
                'Public Property Email As String
                'Public Property Role As String
                'Public Property Status As Boolean
                'Public Property Branch As String
                'Public Property Locked As Boolean
                'Public Property MobileNo As String


                Dim hasmore As Boolean = False


                If values.UserName.Length > 0 Then
                    ' '%\_%'
                    sSQL = sSQL & " Where UserName ilike '%" & values.UserName & "%' "
                    hasmore = True
                End If



                If values.FirstName.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "OR FirstName ilike '%" & values.FirstName & "%' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where FirstName ilike '%" & values.FirstName & "%' "
                        hasmore = True
                    End If

                End If

                If values.LastName.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "Or LastName ilike '%" & values.LastName & "%' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where LastName ilike '%" & values.LastName & "%' "
                        hasmore = True
                    End If

                End If

                If values.Email.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "Or Email ilike '%" & values.Email & "%' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where Email ilike '%" & values.Email & "%' "
                        hasmore = True
                    End If

                End If

                If values.Role.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "Or Role ilike '%" & values.Role & "%' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where Role ilike '%" & values.Role & "%' "
                        hasmore = True
                    End If

                End If

                If values.Status.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "or  Status = '" & values.Status & "' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where Status = '" & values.Status & "' "
                        hasmore = True
                    End If

                End If

                If values.Branch.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "or Branch = '%" & values.Branch & "%' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where Branch ilike '%" & values.Branch & "%' "
                        hasmore = True
                    End If

                End If

                If values.Locked.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & "or Locked = '" & values.Locked & "' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where Locked = '" & values.Locked & "' "
                        hasmore = True
                    End If

                End If

                If values.MobileNo.Length > 0 Then
                    ' '%\_%'
                    If hasmore = True Then
                        sSQL = sSQL & " or MobileNo ilike '%" & values.MobileNo & "%' "
                        hasmore = True
                    Else
                        sSQL = sSQL & " Where MobileNo ilike '%" & values.MobileNo & "%' "
                        hasmore = True
                    End If

                End If




                'If values.companyno.Length > 0 Then

                '        If values.companyname.Length > 0 Then
                '            sSQL = sSQL & "And CompanyNo='" & values.companyno & "' "
                '        Else
                '            sSQL = sSQL & " Where CompanyNo='" & values.companyno & "' "
                '        End If

                '    End If


                If qvals.pagination = False Then
                    ' sSQL = "Select * From FIs"
                Else
                    Dim pagenum As Integer
                    pagenum = qvals.pagesize * (qvals.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qvals.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Users Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception
                SavetoLog("Error from GeUsersFilter : " & ex.Message)
            End Try


            ' Return jsonconvert
        End If



    End Function

    <HttpGet>
    <Route("/admin/settings/users/institutions")>
    Public Function GetUserInstitutions(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select CompanyNo, CompanyName, 'Lenders' AS Type from FIs UNION select (InsNo) as CompanyNo,  (Name) as CompanyName, 'Companies' AS Type, count(*) OVER() AS full_count from Institutions"

                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select CompanyNo, CompanyName, 'Lenders' AS Type , count(*) OVER() AS full_count from FIs UNION select (InsNo) as CompanyNo,  (Name) as CompanyName, 'Companies' AS Type, count(*) OVER() AS full_count from Institutions ORDER BY CompanyName  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Users Institution Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Users" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function

    'Add New User

    <HttpPost>
    <Route("/admin/settings/users/adduser")>
    Public Function AddNewUser(<FromBody()> ByVal values As Users) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim Dr As DataRow
            Dim sSQL As String
            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try
                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.UserName.Length = 0 Then
                        Message.Message = "UserName  Required"
                        Message.Payload = "UserName  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Generate the FI Code ie the ID Number
                    '  values.CompanyNo = GenerateFIID()

                    If values.Password = "" Then
                        Message.Message = "Password Required"
                        Message.Payload = "Password Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    If values.Role = "" Then
                        Message.Message = "Role Required"
                        Message.Payload = "Role Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    ' Check for Duplicate Entry

                    sSQL = "SELECT COUNT(UserName)AS Name from Users WHERE UserName = '" & values.UserName & "'"
                    Dr = DBConnection.GetRowInfo(sSQL)
                    If Dr Is Nothing Then
                        Message.Message = "Internal Error  "
                        Message.Payload = "Cannot Validate Username"
                        Return BadRequest(ReturnJSONResults(Message))
                    Else

                        Dim fint As Integer
                        fint = CInt(Dr("Name").ToString)

                        If fint > 0 Then

                            Message.Message = "Record Already Exist"
                            Message.Payload = "Record Already Exist"

                            Return BadRequest(ReturnJSONResults(Message))
                        Else
                            ' Go ahead to save Record

                            Dim username As String

                            username = GetJWTUsername(myToken)

                            If username = "" Or username Is Nothing Then
                                Message.Message = "Unable to Retrieve Username from token"
                                Message.Payload = "Unable to Retrieve Username from token"
                                Return BadRequest(ReturnJSONResults(Message))
                            End If
                            values.EnteredBy = username
                            Message = DBConnection.SaveNewUser(values, username)
                            If Message.Message = "" Or Message.Message Is Nothing Then

                                Message.Message = "Unable to Save Records"
                                Message.Payload = "Internal Error"

                                Return BadRequest(ReturnJSONResults(Message))

                            Else
                                Dim rval As String
                                rval = ReturnJSONResults(Message)

                                Return Ok(rval)
                            End If

                        End If
                        'Dim cRole As New Roles
                        'With cRole
                        '    .RoleId = Dr("RoleId")
                        '    .RoleName = Dr("RoleName")
                        '    .Description = Dr("Description")
                        '    .IsEnabled = Dr("IsEnabled")
                        '    .IsAuto = Dr("IsAuto")
                        '    .DateCreated = Dr("DateCreated")
                        '    .DateModified = Dr("DateModified")

                        'End With

                        'Dim result As New JsonResult(cRole)

                        'Return Ok(result)

                    End If



                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/settings/users/update")>
    Public Function UpdateUsers(<FromBody()> ByVal values As Users) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.UserName.Length = 0 Then
                        Message.Message = "UserName  Required"
                        Message.Payload = "UserName  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Generate the FI Code ie the ID Number
                    '  values.CompanyNo = GenerateFIID()
                    If values.Role = "" Then
                        Message.Message = "Role Required"
                        Message.Payload = "Role Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Update Record
                    Message = DBConnection.UpdateUser(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Update Record or Record May Not Be Found"
                        Message.Payload = "Error Processing Request"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/settings/users/delete")>
    Public Function DeleteUser(<FromBody()> ByVal values As Users) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.UserName.Length = 0 Then
                        Message.Message = "Username  Required"
                        Message.Payload = "Username  Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Delete Record
                    Message = DBConnection.DeleteUser(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Delete Records Or Record Does not Exist"
                        Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

End Class
