﻿Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data

Public Class dashboard

    Inherits ControllerBase

    <HttpGet>
    <Route("/admin/dashboard/pendingdisbursement")>
    Public Function Getpendingdisbursement(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token
                sSQL = "SELECT Amount, (DATE_PART('day', DateApplied::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateApplied::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateApplied::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE Status = '1000' "
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Pending Disbursement  Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpGet>
    <Route("/admin/dashboard/pendingallocation")>
    Public Function Getpendingallocation(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token


        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token
                sSQL = "SELECT Amount, (DATE_PART('day', DateApplied::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateApplied::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateApplied::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE Status = '2000' "
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Pending Allocations  Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpGet>
    <Route("/admin/dashboard/pendingreallocation")>
    Public Function Getpendingreallocation(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token


        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try

                'Get the SQL parameter values

                Dim pTime As Integer
                Dim hTime As Integer


                pTime = CInt(GetConfigurator(3))
                hTime = CInt(GetConfigurator(4))

                'validate token
                'SELECT Amount, DATEDIFF(minute, DateProcessed, getdate())AS Time from SalaryLoan WHERE (Status = @Status OR Status='4000') AND (DATEDIFF(minute, DateProcessed, getdate()) >= @Time OR DATEDIFF(hour, DateConfirmed, getdate()) >= @HTime)

                sSQL = "SELECT Amount, (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE (Status = '3000'or Status='4000') AND (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint >=" & pTime & " or (DATE_PART('day', DateConfirmed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateConfirmed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateConfirmed::timestamp - CURRENT_DATE::timestamp)::bigint >= " & hTime
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Pending Re-Allocations  Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function


    <HttpGet>
    <Route("/admin/dashboard/pendingficonfirmation")>
    Public Function Getpendingficonfirmation(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token


        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try

                'Get the SQL parameter values

                Dim pTime As Integer
                Dim hTime As Integer


                pTime = CInt(GetConfigurator(3))
                ' hTime = CInt(GetConfigurator(4))

                'validate token
                'SELECT Amount, DATEDIFF(minute, DateProcessed, getdate())AS Time from SalaryLoan WHERE Status = @Status AND DATEDIFF(minute, DateProcessed, getdate()) < @Time

                sSQL = "SELECT Amount, (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE Status = '3000' AND (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint >=" & pTime
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Pending FI Confirmation  Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function


    <HttpGet>
    <Route("/admin/dashboard/allpendingpayments")>
    Public Function Getallpendingpayments(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token


        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try

                'Get the SQL parameter values

                ' Dim pTime As Integer
                Dim hTime As Integer


                ' pTime = CInt(GetConfigurator(3))
                hTime = CInt(GetConfigurator(4))

                'validate token
                'SELECT Amount, DATEDIFF(minute, DateProcessed, getdate())AS Time from SalaryLoan WHERE Status = @Status AND DATEDIFF(hour, DateConfirmed, getdate()) < @Time

                sSQL = "SELECT Amount, (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE Status = '4000' AND (DATE_PART('day', DateConfirmed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateConfirmed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateConfirmed::timestamp - CURRENT_DATE::timestamp)::bigint >=" & hTime
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "All Pending Payments  Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpGet>
    <Route("/admin/dashboard/paiditems")>
    Public Function Getapaiditems(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token


        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try

                'Get the SQL parameter values

                ' Dim pTime As Integer
                Dim hTime As Integer


                ' pTime = CInt(GetConfigurator(3))
                hTime = CInt(GetConfigurator(4))

                'validate token
                'SELECT Amount, BankID,  DATEDIFF(minute, DateAcknowledged, getdate())AS Time from SalaryLoan WHERE Status = @Status 

                sSQL = "SELECT  Amount, BankID,mobileno, (DATE_PART('day', DateAcknowledged::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateAcknowledged::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateAcknowledged::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE Status = '5000'"
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Paid Items Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpGet>
    <Route("/admin/dashboard/allpendingrepayment")>
    Public Function Getallpendingrepayment(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token


        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try

                'Get the SQL parameter values

                ' Dim pTime As Integer
                Dim hTime As Integer


                ' pTime = CInt(GetConfigurator(3))
                hTime = CInt(GetConfigurator(4))

                'validate token
                'SELECT Amount, DATEDIFF(minute, DateProcessed, getdate())AS Time from SalaryLoan WHERE Status in ('6000','7000','8000') and FIStatus is null 

                sSQL = "SELECT  Amount, (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp))* 60 + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint AS Time, count(*) OVER() AS full_count from SalaryLoan WHERE Status in ('6000','7000','8000') and FIStatus is null"
                If values.pagination = False Then

                Else
                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    '  sSQL = sSQL & " ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                    sSQL = sSQL & "Order By id ASC  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum

                End If

                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "All Pending Re-Payment Records"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function


End Class
