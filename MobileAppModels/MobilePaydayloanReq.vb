﻿Public Class MobilePaydayloanReq
    Public Property staffid As String
    Public Property name As String
    Public Property gender As String
    Public Property dateofbirth As String
    Public Property mobileno As String
    Public Property email As String
    Public Property companyname As String
    Public Property location As String
    Public Property netsalary As String
    Public Property idtype As String
    Public Property idnumber As String
    Public Property ssnitno As String
    Public Property salaryaccountno As String
    Public Property bankname As String
    Public Property branch As String
    Public Property accountno As String
    Public Property username As String
    Public Property password As String
    Public Property userguid As String
    Public Property status As String
    Public Property registeredstatus As String
    Public Property idcard As Byte()
    Public Property picture As Byte()
    Public Property signature As Byte()
    Public Property appversion As String



End Class
