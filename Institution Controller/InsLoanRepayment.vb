﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class InsLoanRepayment
    Inherits ControllerBase

    <HttpGet>
    <Route("/institution/loanrepayment")>
    Public Function GetInsLoanRepayment(<FromQuery()> ByVal values As InsTransactions) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            If values.insid Is Nothing Or values.insid.Length = 0 Then
                Message.Message = "No  Institution Number Provided"
                Message.Payload = " Institution Number Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If



            Try
                'validate token
                Dim status As String = "6000"
                ' Dim ttime As String = GetConfigurator(4)

                ' sSQL = "SELECT SL.*, (S.Status)AS State, count(*) OVER() AS full_count FROM SalaryLoan SL LEFT JOIN Statuses S ON SL.Status = S.Code WHERE SL.Institution ='" & values.insid & "'"
                sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, SL.Amount, SL.FI, SL.Status, DM.Modde, SL.DateAcknowledged, B.BankName, F.CompanyName, (I.Name)AS IName, S.Name, count(*) OVER() AS full_count
 FROM SalaryLoan SL
 Left Join DisbursementMode DM
 On SL.Modde = DM.id
 Left Join Banks B
ON SL.BankID = B.BankNo
 Left Join FIs F
ON SL.FI = F.CompanyNo
Left Join Institutions I
ON SL.Institution = I.InsNo
Left Join Staffs S
On SL.StaffID = S.StaffID   WHERE SL.Institution = '" & values.insid & "' AND SL.Status = '" & status & "'"

                Dim hasmore As Boolean = False


                'If values.status IsNot Nothing Then
                '    If values.status.Length > 0 Then
                '        ' '%\_%'
                '        sSQL = sSQL & " And SL.Status ilike '%" & values.status & "%' "
                '        hasmore = True
                '    End If

                'End If



                If values.pagination = True Then

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY SL.id LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If



                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Institution Loan Repayment Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get GetInsLoanRepayment" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function

    <HttpPost>
    <Route("/institution/loanrepayment")>
    Public Function InsReLoanPayment(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'process body json

                    ' Go ahead to Update Record

                    Message = ProcessInsLoanRePayment(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    Public Function ProcessInsLoanRePayment(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token



            ' This is the above JSON with whitespace chars removed (SPACE, TAB, CR, and LF chars).
            ' The presence of whitespace chars for pretty-printing makes no difference to the Load
            ' method. 

            '  Dim jsonStr As String = "{""employees"":[{""firstName"":""John"", ""lastName"":""Doe""},{""firstName"":""Anna"", ""lastName"":""Smith""},{""firstName"":""Peter"",""lastName"":""Jones""}]}"

            '
            '{

            '"applicants" :[
            '{
            '"id": 30,
            '"staffid": "Fan milk",
            '"amount": "1000",
            '"bankid": "Ecobank"

            '},
            '{
            '"id": 45,
            '"staffid": "Fan milk",
            '"amount": "1000",
            '"bankid": "Ecobank"
            '}
            ']
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessInsLoanRePayment : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the "employees" array.
            Dim applicants As Chilkat.JsonArray = json.ArrayOf("applicants")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessInsLoanRePayment : " & "No applicants Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message

            End If

            ' Get the Institution ID

            '  Dim InstID As String = json.StringOf("institution_id")

            ' Iterate over each employee, getting the JSON object at each index.
            Dim numApplicants As Integer = applicants.Size
            Dim i As Integer = 0
            While i < numApplicants

                Dim appObj As Chilkat.JsonObject = applicants.ObjectAt(i)

                Dim ssql As String
                Dim staffid As String
                Dim bankid As String
                Dim amount As String
                Dim id As Integer


                ' Dim ssql As String



                staffid = appObj.StringOf("staffid")
                bankid = appObj.StringOf("bankid")
                amount = appObj.StringOf("amount")
                id = Integer.Parse(appObj.StringOf("id"))

                ssql = "update SalaryLoan set Status='7000', InsRepaymentDate=@InsRepaymentDate, InsRepaymentBy=@InsRepaymentBy, RepaymentBankID=@RepaymentBankID WHERE ID=" & id
                ' ssql = "update Staffs set RegisteredStatus='" & status & "', ApprovedBy='" & username & "', ApprovedDate = '" & DateTime.Now & "', InsID = '" & InstID & "' WHERE ID=" & id

                Message.mError = Not DBConnection.InsLoanRePayment(ssql, username, id, staffid, amount, bankid)
                ' Debug.WriteLine("employee[" & i & "] = " & appObj.StringOf("id") & " " & appObj.StringOf("status"))
                'update the individual records

                i = i + 1
            End While


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessInsLoanRePayment : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function


End Class
