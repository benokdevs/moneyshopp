﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO

Public Class Configuration
    Inherits ControllerBase
    <HttpGet>
    <Route("/admin/settings/configurations")>
    Public Function GetConfigurations(<FromQuery()> ByVal values As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                If values.pagination = False Then
                    sSQL = "Select *, count(*) OVER() AS full_count From Configurators"
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = "Select *, count(*) OVER() AS full_count  From Configurators ORDER BY id  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If
                'ORDER BY EmpID  LIMIT Paging_PageSize OFFSET PageNumber; 




                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Configuration Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get Configuration" & ex.Message)
            End Try


            ' Return jsonconvert
        End If

    End Function

    <HttpPost>
    <Route("/admin/settings/configurations/add")>
    Public Function AddNewConfig(<FromBody()> ByVal values As Configurations) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim Dr As DataRow
            Dim sSQL As String
            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try
                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    If values.Name.Length = 0 Then
                        Message.Message = "Name Required"
                        Message.Payload = "Name Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    If values.Value = "" Then
                        Message.Message = "Value Required"
                        Message.Payload = "Value Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    ' Check for Duplicate Entry

                    sSQL = "SELECT COUNT(Name)AS Names from Configurators WHERE Name = '" + values.Name + "'"
                    Dr = DBConnection.GetRowInfo(sSQL)
                    If Dr Is Nothing Then
                        Message.Message = "No Records Found"
                        Message.Payload = "No Records Found"
                        Return BadRequest(ReturnJSONResults(Message))
                    Else

                        Dim fint As Integer
                        fint = CInt(Dr("Names").ToString)

                        If fint > 0 Then

                            Message.Message = "Record Already Exist"
                            Message.Payload = "Record Already Exist"

                            Return BadRequest(ReturnJSONResults(Message))
                        Else
                            ' Go ahead to save Record

                            Dim username As String

                            username = GetJWTUsername(myToken)

                            If username = "" Or username Is Nothing Then
                                Message.Message = "Unable to Retrieve Username from token"
                                Message.Payload = "Unable to Retrieve Username from token"
                                Return BadRequest(ReturnJSONResults(Message))
                            End If

                            Message = DBConnection.SaveConfiguration(values, username)
                            If Message.Message = "" Or Message.Message Is Nothing Then

                                Message.Message = "Unable to Save Records"
                                Message.Payload = "Internal Error"

                                Return BadRequest(ReturnJSONResults(Message))

                            Else
                                Dim rval As String
                                rval = ReturnJSONResults(Message)

                                Return Ok(rval)
                            End If

                        End If


                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

    <HttpPost>
    <Route("/admin/settings/configurations/update")>
    Public Function UpdateConfigurtions(<FromBody()> ByVal values As Configurations) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned

            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else

                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.id <= 0 Then
                        Message.Message = "Id Required"
                        Message.Payload = "Id Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If



                    If values.Value Is Nothing Or values.Value.Length = 0 Then
                        Message.Message = "Value Required"
                        Message.Payload = "Value Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If
                    ' Update so no need for generation of ID
                    ' values.CompanyNo = GenerateFIID()


                    ' Go ahead to Update Record
                    Message = DBConnection.UpdateConfigurations(values, username)
                    If Message.Message = "" Or Message.Message Is Nothing Then

                        Message.Message = "Unable to Update Record or Record May Not Be Found"
                        Message.Payload = "Error Processing Request"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function

End Class
