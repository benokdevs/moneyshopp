﻿Imports System
Imports Microsoft.AspNetCore.Hosting
Imports Microsoft.AspNetCore
Imports Microsoft.Extensions.DependencyInjection
Imports Microsoft.AspNetCore.Builder
Imports Microsoft.AspNetCore.Mvc
Imports Microsoft.Extensions.Logging
Imports Microsoft.Extensions

Class Startup

    Public Sub Startup(env As IHostingEnvironment)
        AppPath = env.WebRootPath
    End Sub

    Sub ConfigureServices(Services As IServiceCollection)

        Services.AddCors(Function(options)
                             options.AddPolicy("CorsPolicy", Function(builder) builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials())
                         End Function)
        Services.AddOptions()



        Services.AddCors() '// Make sure you Call this previous To AddMvc
        Services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)

        Services.AddMvc()
    End Sub

    Sub Configure(app As IApplicationBuilder, env As IHostingEnvironment, loggerFactory As ILoggerFactory)

        '    app.UseCors(Options.WithOrigins("http://example.com").AllowAnyMethod()
        ')
        ''app.UseCors(Function(builder)
        ''                builder.AllowAnyOrigin().AllowAnyMethod().AllowCredentials().WithMethods("GET", "PUT",
        ''"POST", "DELETE").AllowAnyHeader()
        ''                ' builder .WithOrigins 
        ''            End Function)
        app.UseCors("CorsPolicy")
        app.UseStaticFiles()
        app.UseMvcWithDefaultRoute()
    End Sub
End Class