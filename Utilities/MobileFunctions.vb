﻿
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Text
Imports Npgsql
Imports Troschuetz.Random


Public Module MobileFunctions

    Public strSMSGateway As String = GetSettings("General.SMSGateway").ToString()
    Public strconnection As String = GetSettings("General.MoneyShoppConn").ToString()
    Public SMSRegtemplate As String = GetSettings("General.SMSRegtemplate").ToString()
    Public SMSForgotpasswordtemplate As String = GetSettings("General.SMSForgotpasswordtemplate").ToString()

    Public Structure StructStaffDetails
        Dim staffid As String
        Dim name As String
        Dim institution As String
        Dim accountno As String
        Dim mobile As String
        Dim netsalary As String
    End Structure
    Public Structure clsAffordabilty
        Dim affordabilityamount As String
        Dim defaulted As Boolean
    End Structure
    Public Structure StructJWTValues
        Dim username As String
        Dim status As String
        Dim appversion As String
    End Structure
    Public Structure StructValidateOTPResults
        Dim RESETUSERNAME As String
        Dim NEWPASSWORD As String
        Dim status As Boolean
    End Structure
    Public Structure structMandate

        Dim CustomerName As String
        Dim CustomerBankBranch As String
        Dim Accountname As String
        Dim Bank As String
        Dim Accountnumber As String
        Dim Amount As String

        Dim Amounttorepay As String
        Dim refno As String
        Dim signature As String
        Dim effectdate As String
        Dim transdate As String
        Dim mandaterefno As String

        Dim creditacctname As String
        Dim creditbank As String
        Dim creditbankbranch As String
        Dim creditacctno As String

    End Structure
    Public Structure structMandateCredit

        Dim creditacctname As String
        Dim creditbank As String
        Dim creditbankbranch As String
        Dim creditacctno As String
    End Structure
    Public Function CheckAppVersion(ByVal appversion As String) As Boolean
        Try
            Dim vers As String = ""
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from configurators where name = 'appversion'", conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader

            While dr.Read
                vers = dr("value").ToString

            End While

            dr.Close()
            conn.Close()

            If (vers = appversion) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function GetAppVersion() As String
        Try
            Dim vers As String = ""
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from configurators where name = 'appversion'", conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader

            While dr.Read
                vers = dr("value").ToString

            End While

            dr.Close()
            conn.Close()

            Return vers
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function GetTokenDuration() As Double

        'tokenduration

        Try
            Dim rval As Double = 0.0
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from configurators where name = 'tokenduration'", conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader()

            While dr.Read()
                rval = Convert.ToDouble(dr(CStr("value")).ToString())
            End While

            dr.Close()
            conn.Close()

            If rval = 0 Then
                Return 0
            Else
                Return rval
            End If

        Catch __unusedException1__ As Exception
            Return 0
        End Try
    End Function

    Public Function GenerateOTP() As String
        Dim otp As New TRandom
        Return otp.Next(100000, 999999).ToString("000000")
    End Function
    Public Function CheckDuplicateOTP(ByVal otp As String) As Boolean
        Try
            ' string vers="";
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from forgetpassword where otp = '" & otp & "'", conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader()

            If dr.HasRows = True Then
                dr.Close()
                conn.Close()
                Return True
            Else
                dr.Close()
                conn.Close()
                Return False

            End If

        Catch ex As Exception
            SavetoLog("CheckDuplicateOTP Failed with error : " & ex.Message)
            Return True
        End Try
    End Function
    Public Function DeleteOldOTPRecords(ByVal mobile As String) As Boolean
        Try
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            conn.Open()
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("delete from forgetpassword where mobileno=@mobileno", conn)


            'passing parameters to query
            cmd.Parameters.AddWithValue("@mobileno", mobile)
            cmd.ExecuteNonQuery()
            conn.Close()
            ' response = "0";

            Return True
        Catch ex As Exception
            'throw ex;
            '  response = "Unable to Set Token Expiration";
            Return False
        End Try
    End Function
    Public Function GetOTPDuration() As Double

        'tokenduration

        Try
            Dim rval As Double = 0.0
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from configurators where name = 'otpduration'", conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader()

            While dr.Read()
                rval = Convert.ToDouble(dr(CStr("value")).ToString())
            End While

            dr.Close()
            conn.Close()

            If rval = 0 Then
                Return 0
            Else
                Return rval
            End If

        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Function SaveOTP(ByVal myFP As MobForgotPassword) As Boolean
        Try
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            conn.Open()
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("insert into forgetpassword( mobileno,otp ,expiry,username, createdon) values (@mobileno, @otp,@expiry ,@username,@createdon)", conn)
            '  string refno = GenNewRefNo("SalaryLoan", "id").ToString();
            'passing parameters to query
            cmd.Parameters.AddWithValue("@Mobileno", myFP.mobileno)
            cmd.Parameters.AddWithValue("@username", myFP.username)
            cmd.Parameters.AddWithValue("@otp", myFP.otp)
            cmd.Parameters.AddWithValue("@expiry", myFP.expiry)
            cmd.Parameters.AddWithValue("@createdon", Date.Now)
            cmd.ExecuteNonQuery()
            conn.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function SendSMS(ByVal message As String, ByVal destination As String, ByVal sentBy As String, ByVal email As String) As Boolean
        ' ***************mnotify settings****************
        ' Dim str5 As String
        ' Dim str As String = "b4f4f9c987f5e3cc0478"
        ' Dim str4 As String = desti
        ' Dim str3 As String = sentby
        ' Try
        '     Dim request As WebRequest = WebRequest.Create(String.Concat(New String() {"http://bulk.mnotify.net/smsapi?key=", str, "&to=", str4, "&msg=", MSG, "&sender_id=", str3}))
        '     request.Timeout = &H7530
        '     str5 = New StreamReader(request.GetResponse.GetResponseStream).ReadToEnd
        ' Catch exception1 As Exception
        ' End Try
        ' Return str5
        ' ****************Wigal settings******************
        'bool sucess = false;
        'string username = "godlyfav";
        'string password = "Gfofavoured";

        '{
        '    "type" "0",
        '    "message": "A quick message to see how good my calculation is. I am here on a test pilot",
        '    "destination": "233207488366",
        '    "sentby": "KlarysLtd"
        '}'


        'https://api.echanga.com/v1/external/messaging/quick



        Dim request As WebRequest
        Dim response As HttpWebResponse
        Dim responseMess As String = ""
        Dim desti As String = FormatSento(destination)
        'Dim apiparams As String = "
        '{
        '    ""type"": ""0"",
        '    ""message"": """ & message  & """,
        '    ""destination"": """ & desti  & """,
        '    ""sentby"": """ & sentBy &"""
        '}"

        Dim URL As String = strSMSGateway
        '  apiparams = String.Format(apiparams, message, desti, sentBy, email)


        '****************Wigal settings******************
        Dim sucess As Boolean = False
        Dim username As String = "spygbo"
        Dim password As String = "admin1234"
        'use comma (,) to separate multiple recipients. remember that the numbers has to be ininternational Format
        '//example to = 23327612806,2335487899,233244587896,233266000751,233277014525

        Dim apiparams As String = "username={0}&password={1}&from={2}&to={3}&message={4}"
        'Dim URL As String = "http://isms.wigalsolutions.com/ismsweb/sendmsg/"
        apiparams = String.Format(apiparams, username, password, sentBy, desti, message)
        Try
            request = HttpWebRequest.Create(URL)
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            Dim Data = Encoding.ASCII.GetBytes(apiparams)
            request.ContentLength = Data.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(Data, 0, Data.Length)
            dataStream.Close()
            request.Timeout = 5000

            response = CType(request.GetResponse(), HttpWebResponse)
            Dim reader As StreamReader = New StreamReader(response.GetResponseStream())

            responseMess = reader.ReadToEnd()
            response.Close()
            If responseMess.ToLower.Contains("SUCCESS".ToLower) Then
                Return True
            Else
                SavetoLog("SMS Sending Failed with : " & responseMess)
                Return False

            End If
            ' Return responseMess
        Catch ex As Exception
            SavetoLog("SendSMS Faild with : " & ex.Message)
            Return False
        End Try
    End Function
    Public Function CheckOwing(ByVal staffid As String) As String
        Dim count As Integer
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select count(staffid)as ccc from salaryloan where staffid = '" _
                        + (staffid + "' and status in ('1000', '2000', '3000', '4000', '5000', '6000') and date_part('mon', dateapplied::ti" &
                        "mestamp) < date_part('mon', current_date::timestamp)")), conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader

        While dr.Read
            count = dr("ccc").ToString

        End While

        dr.Close()
        conn.Close()
        Return count
    End Function

    Public Function CheckApplication(ByVal staffid As String) As String
        Dim count As String = ""
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select sum(amount)as count from salaryloan where staffid = '" _
                        + (staffid + "' and date_part('year', datedisbursed::timestamp) = date_part('year', current_date::timestamp) and  d" &
                        "ate_part('mon', datedisbursed::timestamp) = date_part('mon', current_date::timestamp)")), conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader

        While dr.Read
            count = dr("Count").ToString

        End While

        dr.Close()
        conn.Close()

        If ((count Is Nothing) _
                    OrElse (count = "")) Then
            count = "0.00"
        End If

        Return count
    End Function
    'Dim staffID, Name, Institution, AccountNo, Mobile, NetSalary, count, availableloan As String

    Public Function StaffDetails(ByVal staffid As String) As StructStaffDetails
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select staffid, name, insid, accountno, mobileno, netsalary from staffs where username = '" _
                        + (staffid + "'")), conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader
        Dim sd As New StructStaffDetails
        While dr.Read
            '  txt = dr["name"].tostring();
            With sd
                .staffid = dr("staffid").ToString
                .name = dr("name").ToString
                .institution = dr("insid").ToString
                .accountno = dr("accountno").ToString
                .mobile = dr("mobileno").ToString
                .netsalary = dr("netsalary").ToString
            End With
        End While

        dr.Close()
        conn.Close()
        Return sd
    End Function

    Public Function GenNewRefNo(ByVal Tbl As String, ByVal Fld As String) As String
        Dim otp As New TRandom
        Return otp.Next(100000, 999999).ToString("000000") & GetNewID(Tbl, Fld).ToString()
    End Function

    Public Function GetNewID(ByVal Tbl As String, ByVal Fld As String) As Integer
        Dim NewRow As DataRow
        Dim sSQL As String
        ' GetNewID = -1;
        sSQL = ("select count(" _
                    + (Fld + (") as co, max(" _
                    + (Fld + (") + 1 as id from " + Tbl)))))
        NewRow = GetRowInfo(sSQL)
        If (System.Convert.ToInt32(NewRow("co")) = 0) Then
            Return 0
        Else
            Return System.Convert.ToInt32(NewRow("id"))
        End If

    End Function

    Public Function GetRowInfo(ByVal sSql As String) As DataRow
        ' Dim strcon As String = WebConfigurationManager.AppSettings("MoneyShoppConn").ToString
        Dim sDa As NpgsqlDataAdapter = New NpgsqlDataAdapter(sSql, strconnection)
        Dim Dt As DataTable = New DataTable
        Try
            sDa.Fill(Dt)
            sDa.Dispose()

            If (Dt.Rows.Count = 0) Then
                Return Nothing
            ElseIf (Dt.Rows.Count > 1) Then
                Throw New Exception("Multiple rows effected")
            Else
                Return Dt.Rows(0)
            End If

        Catch ex As Exception
            'Interaction.MsgBox("Sorry Unable to Connect to Database Please Contact Administrator");
            SavetoLog("GetRowInfo Failed with Error : " & ex.Message)
            Return Nothing
        End Try

    End Function
    Public Function InterestRate() As String
        'If (ValidateToken(username, token) = False) Then
        '    response = "Invalid Token"
        '    Return response
        'End If
        Dim response As String = ""
        ' If (CheckAppVersion(appversion) = True) Then
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand("select value from configurators where id = '1'", conn)
        '
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader

        While dr.Read
            response = dr("value").ToString

        End While

        dr.Close()
        conn.Close()
        Return response
        'Else
        '    response = "Invalid Mobile Version"
        '    Return response
        'End If

    End Function

    Public Function LoanLimit() As String
        'If (ValidateToken(username, token) = False) Then
        '    response = "Invalid Token"
        '    Return response
        'End If
        Dim response As String = ""
        '  If (CheckAppVersion(appversion) = True) Then
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand("select value from configurators where id = '2'", conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader

        While dr.Read
            response = dr("value").ToString

        End While

        dr.Close()
        conn.Close()
        Return response
        'Else
        '    response = "Invalid Mobile Version"
        '    Return response
        'End If

    End Function

    Public Function CheckDefaults(ByVal username As String) As Boolean
        Dim rval As String = ""
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select count(staffid)as ccc from salaryloan where staffid = '" _
                        + (username + "' and status in ('1000', '2000', '3000', '4000', '5000', '6000') and date_part('mon', dateapplied::ti" &
                        "mestamp) < date_part('mon', current_date::timestamp)")), conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader

        While dr.Read
            rval = dr("ccc").ToString

        End While

        dr.Close()
        conn.Close()

        If ((rval Is Nothing) _
                    OrElse (rval = "")) Then
            rval = "0.00"
        End If

        If (Integer.Parse(rval) > 0) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function CheckPaydayDuplicate(ByVal mobileno As String, ByVal uname As String, ByVal email As String, ByVal IDNO As String) As Boolean
        Try
            ' string vers="";
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select * from staffs where mobileno = '" _
                            + (mobileno + ("' or username = '" _
                            + (uname + ("' or email = '" _
                            + (email + ("' or idno ='" _
                            + (IDNO + "'")))))))), conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader
            If (dr.HasRows = True) Then
                dr.Close()
                conn.Close()
                Return True
            Else
                dr.Close()
                conn.Close()
                Return False
            End If


        Catch ex As Exception
            SavetoLog("CheckPaydayDuplicate Failed with error: " & ex.Message)
            Return True
        End Try

    End Function

    Public Function GenerateRegistrationOTP(ByVal MobileNo As String, ByVal uname As String) As Boolean
        Dim otp As String = ""
        otp = GenerateOTP()
        Do While (CheckDuplicateNewRegistrationOTP(otp) = True)
            otp = GenerateOTP()
        Loop




        Dim myFP As New MobForgotPassword
        'set values to be saved
        If (DeleteOldRegistrationOTP(MobileNo) = True) Then
            Dim currentTime As DateTime = DateTime.Now
            Dim exptotptime As DateTime = currentTime.AddMinutes(GetOTPDuration)
            myFP.mobileno = MobileNo
            myFP.username = uname
            myFP.otp = otp
            myFP.expiry = exptotptime
            If (SaveRegistrationOTP(myFP) = True) Then
                'Send SMS here
                Dim msg As String = SMSRegtemplate.Replace("<<OTP>>", otp)
                Dim desti As String = MobileNo
                Dim sendbyy As String = "MoneyShopp"
                Dim email As String = "spyglassgh@gmail.com"
                If (SendSMS(msg, desti, sendbyy, email) = True) Then
                    Return True
                Else
                    Return False
                End If

            Else
                Return False
            End If

        Else
            Return False
        End If

    End Function
    Public Function CheckDuplicateNewRegistrationOTP(ByVal otp As String) As Boolean
        Try
            ' string vers="";
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand(("select * from registrationotp where otp = '" _
                            + (otp + "'")), conn)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader
            If (dr.HasRows = True) Then
                dr.Close()
                conn.Close()
                Return True
            Else
                dr.Close()
                conn.Close()
                Return False
            End If

        Catch ex As Exception
            Return True
        End Try

    End Function
    Public Function SaveRegistrationOTP(ByVal myFP As MobForgotPassword) As Boolean
        Try
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            conn.Open()
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("insert into registrationotp( mobileno,otp ,expiry,username, createdon) values (@mobileno, @otp,@expir" &
                "y ,@username,@createdon)", conn)
            '  string refno = GenNewRefNo("SalaryLoan", "id").ToString();
            'passing parameters to query
            cmd.Parameters.AddWithValue("@mobileno", myFP.mobileno)
            cmd.Parameters.AddWithValue("@username", myFP.username)
            cmd.Parameters.AddWithValue("@otp", myFP.otp)
            cmd.Parameters.AddWithValue("@expiry", myFP.expiry)
            cmd.Parameters.AddWithValue("@createdon", DateTime.Now)
            cmd.ExecuteNonQuery()
            conn.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DeleteOldRegistrationOTP(ByVal mobile As String) As Boolean
        Try
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            conn.Open()
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("delete from registrationotp where mobileno=@mobileno", conn)
            'passing parameters to query
            cmd.Parameters.AddWithValue("@mobileno", mobile)
            cmd.ExecuteNonQuery()
            conn.Close()
            ' response = "0";
            Return True
        Catch ex As Exception
            SavetoLog(("DeleteOldRegistrationOTP :  Error = " + ex.Message))
            'throw ex;
            '  response = "Unable to Set Token Expiration";
            Return False
        End Try

    End Function
    Public Function _ValidateOTP(ByVal otp As String) As StructValidateOTPResults
        Dim vr As New StructValidateOTPResults
        Try
            'Return True means Good .....All is Well
            'DateTime myDateTime = DateTime.Now;
            'string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff")
            Dim myFP As MobForgotPassword = New MobForgotPassword
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from forgetPassword where otp =@otp", conn)
            conn.Open()
            'passing parameters to query
            cmd.Parameters.AddWithValue("@otp", otp)
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader


            If (dr.HasRows = True) Then

                While dr.Read
                    myFP.otp = dr("otp").ToString
                    myFP.username = dr("username").ToString
                    myFP.expiry = Convert.ToDateTime(dr("expiry").ToString)
                    myFP.mobileno = dr("MobileNo").ToString
                    ' status = dr["status"].ToString();
                    vr.RESETUSERNAME = myFP.username

                End While

                dr.Close()
                conn.Close()
                Dim myDateTime As DateTime = DateTime.Now
                'string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff")
                ' DateTime date1 = new DateTime(2009, 8, 1, 0, 0, 0);
                ' DateTime date2 = new DateTime(2009, 8, 1, 12, 0, 0);
                Dim result As Integer = DateTime.Compare(myFP.expiry, myDateTime)
                If (result < 0) Then
                    ' RemoveToken(token, username);
                    vr.status = False
                ElseIf (result = 0) Then
                    ' RemoveToken(token, username);
                    vr.status = False
                Else
                    'Valid OTP not expired , so generate new Password and return it
                    Dim newpass As String = GenerateToken(6)
                    vr.NEWPASSWORD = newpass
                    If (SetPassword(newpass, myFP.mobileno) = True) Then
                        vr.status = True
                    Else
                        vr.status = False
                    End If

                End If

            Else
                vr.status = False
            End If
            Return vr
        Catch ex As Exception
            SavetoLog("_ValidateOTP Failed with Error : " & ex.Message)
            vr.status = False
            Return vr
        End Try

    End Function
    Public Function GenerateToken(ByVal size As Integer) As String
        ' Characters except I, l, O, 1, and 0 to decrease confusion when hand typing tokens
        Dim charSet = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789"
        Dim chars = charSet.ToCharArray
        Dim data = New Byte((1) - 1) {}
        Dim crypto = New RNGCryptoServiceProvider
        crypto.GetNonZeroBytes(data)
        data = New Byte((size) - 1) {}
        crypto.GetNonZeroBytes(data)
        Dim result = New StringBuilder(size)
        For Each b In data
            result.Append(chars(b Mod chars.Length))
        Next
        Return result.ToString
    End Function
    Public Function SetPassword(ByVal newpassword As String, ByVal mobile As String) As Boolean
        Try
            'Now set the new password and Update password
            Dim sec As New Security
            Dim userGuid As Guid = System.Guid.NewGuid
            ' Hash the password together with our unique userGuid
            Dim hashedPassword As String = sec.HashSHA1((newpassword + userGuid.ToString))
            Dim connn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            connn.Open()
            Dim cmmd As NpgsqlCommand = New NpgsqlCommand("update staffs set password=@password,userguid=@userguid where mobileno=@mobileno", connn)
            'passing parameters to query
            cmmd.Parameters.AddWithValue("@mobileno", mobile)
            cmmd.Parameters.AddWithValue("@password", hashedPassword)
            cmmd.Parameters.AddWithValue("@userguid", userGuid)
            cmmd.ExecuteNonQuery()
            connn.Close()
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            conn.Open()
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("update forgetpassword set newpassword=@newpassword where mobileno=@mobileno", conn)
            'passing parameters to query
            cmd.Parameters.AddWithValue("@mobileno", mobile)
            cmd.Parameters.AddWithValue("@newpassword", newpassword)
            cmd.ExecuteNonQuery()
            conn.Close()
            Return True
        Catch ex As Exception
            Return False
            ' throw ex;
        End Try

    End Function
    Public Function _ValidateRegistrationOTP(ByVal otp As String) As Boolean
        Try
            'Return True means Good .....All is Well
            'DateTime myDateTime = DateTime.Now;
            'string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff")
            Dim myFP As MobForgotPassword = New MobForgotPassword
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from registrationotp where otp =@otp", conn)
            conn.Open()
            'passing parameters to query
            cmd.Parameters.AddWithValue("@otp", otp)
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader
            If (dr.HasRows = True) Then

                While dr.Read
                    myFP.otp = dr("otp").ToString
                    myFP.username = dr("username").ToString
                    myFP.expiry = Convert.ToDateTime(dr("expiry").ToString)
                    myFP.mobileno = dr("mobileno").ToString
                    ' status = dr["status"].tostring();
                    '  resetusername = myfp.username;

                End While

                dr.Close()
                conn.Close()
                Dim myDateTime As DateTime = DateTime.Now
                'string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff")
                ' DateTime date1 = new DateTime(2009, 8, 1, 0, 0, 0);
                ' DateTime date2 = new DateTime(2009, 8, 1, 12, 0, 0);
                Dim result As Integer = DateTime.Compare(myFP.expiry, myDateTime)
                If (result < 0) Then
                    ' RemoveToken(token, username);
                    Return False
                ElseIf (result = 0) Then
                    ' RemoveToken(token, username);
                    Return False
                Else
                    'Valid OTP not expired , so Set The Status of the Account 
                    If (SetRegistrationStatus(myFP.username) = True) Then
                        Return True
                    Else
                        Return False
                    End If

                End If

            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function


    Public Function SetRegistrationStatus(ByVal username As String) As Boolean
        Try
            'Now set the new password and Update password
            ' Guid userGuid = System.Guid.NewGuid();
            ' Hash the password together with our unique userGuid
            ' string hashedPassword = HashSHA1(newpassword + userGuid);
            Dim connn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            connn.Open()
            Dim cmmd As NpgsqlCommand = New NpgsqlCommand("update staffs set registeredstatus=@registeredstatus where username=@username", connn)
            'passing parameters to query
            cmmd.Parameters.AddWithValue("@registeredstatus", Convert.ToBoolean("true"))
            cmmd.Parameters.AddWithValue("@username", username)
            cmmd.ExecuteNonQuery()
            connn.Close()
            Return True
        Catch ex As Exception
            Return False
            ' throw ex;
        End Try

    End Function
    Public Function GetMandateCredit() As structMandateCredit
        Dim CreditMandate As structMandateCredit = New structMandateCredit
        Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
        Dim cmd As NpgsqlCommand = New NpgsqlCommand("SELECT * from configurators", conn)
        conn.Open()
        Dim dr As NpgsqlDataReader = cmd.ExecuteReader

        While dr.Read
            Select Case (dr("name").ToString)
                Case "creditacctname"
                    CreditMandate.creditacctname = dr("value").ToString
                Case "creditbank"
                    CreditMandate.creditbank = dr("value").ToString
                Case "creditbankbranch"
                    CreditMandate.creditbankbranch = dr("value").ToString
                Case "creditacctno"
                    CreditMandate.creditacctno = dr("value").ToString
            End Select


        End While

        dr.Close()
        conn.Close()
        Return CreditMandate
    End Function
    Public Function CheckDuplicateMandate(ByVal username As String, ByVal refno As String) As Boolean
        Try
            ' string vers="";
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("select * from mandate where username=@username and mandaterefno=@mandaterefno", conn)
            'passing parameters to query
            cmd.Parameters.AddWithValue("@username", username)
            cmd.Parameters.AddWithValue("@mandaterefno", refno)
            conn.Open()
            Dim dr As NpgsqlDataReader = cmd.ExecuteReader
            If (dr.HasRows = True) Then
                dr.Close()
                conn.Close()
                Return True
            Else
                dr.Close()
                conn.Close()
                Return False
            End If

            'while (dr.Read())
            '{
            '    vers = dr["value"].ToString();
            '}f
        Catch ex As Exception
            Return True
        End Try

    End Function
    Public Function SaveMandate(ByVal myMandate As structMandate, ByVal username As String, ByVal rval() As Byte) As Boolean
        Try
            Dim conn As NpgsqlConnection = New NpgsqlConnection(strconnection)
            conn.Open()
            Dim cmd As NpgsqlCommand = New NpgsqlCommand("insert into mandate(username, accountname,bank ,amount, signature, mandaterefno, accountnumber, credi" &
                "tacctname,creditacctno,creditbank,creditbankbranch,approvalstatus,customername,customerbankbranch,am" &
                "ounttorepay) values (@username, @accountname,@bank ,@amount, @signature, @mandaterefno, @accountnumb" &
                "er, @creditacctname,@creditacctno,@creditbank,@creditbankbranch,@approvalstatus,@customername,@custo" &
                "merbankbranch,@amounttorepay)", conn)
            '  string refno = gennewrefno("salaryloan", "id").tostring();
            'passing parameters to query
            cmd.Parameters.AddWithValue("@username", username)
            cmd.Parameters.AddWithValue("@accountname", myMandate.Accountname)
            cmd.Parameters.AddWithValue("@bank", myMandate.Bank)
            cmd.Parameters.AddWithValue("@amount", myMandate.Amount)
            cmd.Parameters.AddWithValue("@customername", myMandate.CustomerName)
            cmd.Parameters.AddWithValue("@customerbankbranch", myMandate.CustomerBankBranch)
            cmd.Parameters.AddWithValue("@amounttorepay", myMandate.Amounttorepay)
            'amounttorepay
            If (myMandate.signature = "") Then
                cmd.Parameters.Add("@signature", NpgsqlTypes.NpgsqlDbType.Bytea, -1)
                cmd.Parameters("@signature").Value = DBNull.Value
            Else
                cmd.Parameters.AddWithValue("@signature", rval)
            End If

            '  cmd.parameters.addwithvalue("@effectdate",convert.todatetime(effectdate));
            ' cmd.parameters.addwithvalue("@transdate", datetime.now);
            cmd.Parameters.AddWithValue("@mandaterefno", myMandate.mandaterefno)
            cmd.Parameters.AddWithValue("@accountnumber", myMandate.Accountnumber)
            cmd.Parameters.AddWithValue("@creditacctname", myMandate.creditacctname)
            cmd.Parameters.AddWithValue("@creditacctno", myMandate.creditacctno)
            cmd.Parameters.AddWithValue("@creditbank", myMandate.creditbank)
            cmd.Parameters.AddWithValue("@creditbankbranch", myMandate.creditbankbranch)
            cmd.Parameters.AddWithValue("@approvalstatus", False)
            cmd.ExecuteNonQuery()
            conn.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Module
