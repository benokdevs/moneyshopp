﻿
Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO

Public Class InstLoanApproval
    Inherits ControllerBase

    <HttpPost>
    <Route("/admin/loans/instloanapproval")>
    Public Function InstLoanApproval(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                If VerifyJWT(myToken) = False Then
                    Message.Message = "Invalid Token/Expired Token"
                    Message.Payload = "Invalid Token/Expired Token"
                    Return BadRequest(ReturnJSONResults(Message))
                Else
                    Dim username As String

                    username = GetJWTUsername(myToken)

                    If username = "" Or username Is Nothing Then
                        Message.Message = "Unable to Retrieve Username from token"
                        Message.Payload = "Unable to Retrieve Username from token"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If


                    If values.Length = 0 Then
                        Message.Message = "No Body Content "
                        Message.Payload = "Content Required"
                        Return BadRequest(ReturnJSONResults(Message))
                    End If

                    'process body json

                    ' Go ahead to Update Record

                    Message = ProcessInstLoansApproval(values, username)

                    If Message.mError = True Or Message.Message Is Nothing Then

                        'Message.Message = "Unable to Update Records"
                        'Message.Payload = "Internal Error"

                        Return BadRequest(ReturnJSONResults(Message))

                    Else
                        Dim rval As String
                        rval = ReturnJSONResults(Message)

                        Return Ok(rval)
                    End If


                End If
            Catch ex As Exception

            End Try



        End If

    End Function
    Public Function ProcessInstLoansApproval(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token


            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessInstLoansApproval : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            ' Get the Status ID

            Dim Status As String = json.StringOf("status")


            Dim instArray As Chilkat.JsonArray = json.ArrayOf("institutions_id")
            If (json.LastMethodSuccess = False) Then
                SavetoLog("Error From ProcessInstLoansApproval : " & "No applicants Found")
                Message.Message = "No applicants Found"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message


            End If

            Dim arraySize As Integer = instArray.Size
            Dim i As Integer
            Dim ssql As String
            For i = 0 To arraySize - 1

                ' Dim sValue As String = instArray.StringAt(i)

                '  Debug.WriteLine("[" & i & "] = " & sValue)

                Dim iValue As Integer = instArray.IntAt(i)

                ssql = "update SalaryLoan set Status='1500' WHERE id=" & iValue.ToString

                Message.mError = Not DBConnection.InstLoanApproval(ssql, username, iValue.ToString)

                ' Debug.WriteLine("[" & i & "] as integer = " & iValue)

            Next


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessInstLoansApproval : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function


    <HttpGet>
    <Route("/admin/loans/instloanapproval")>
    Public Function GetInstLoans(<FromQuery()> ByVal qval As InstLoanApprFilters) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            Try
                'validate token

                Dim glob As New Chilkat.Global
                Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                'Log("Chilkat License Status : " & succes)


                'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
                ' sSQL = "Select id, Name, Gender, DOB, MobileNo, Email, CompanyName, Location, NetSalary, SSNITNo, SalaryAccountNo, BankName, Branch, AccountNo, UserName, Status, RegisteredStatus, IDCard, Picture, Signature, IDType, IDNO, StaffID From Staffs  Where RegisteredStatus = '" & qval.status & "' "

                'WHERE  SL.Status = '1000' ORDER BY id"


                If qval.InsId = "" Then
                    sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, SL.Amount, (SL.Amount + SL.Interest)AS Total, SL.FI, SL.Status, SL.DateAcknowledged, B.BankName, F.CompanyName, (I.Name)AS IName, S.Name, count(*) OVER() AS full_count 
 FROM SalaryLoan SL
 Left Join DisbursementMode DM
 On SL.Modde = DM.id
 Left Join Banks B
ON SL.BankID = B.BankNo
 Left Join FIs F
ON SL.FI = F.CompanyNo
Left Join Institutions I
ON SL.Institution = I.InsNo
Left Join Staffs S
On SL.StaffID = S.StaffID WHERE  SL.Status = '1000'"
                Else
                    sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, SL.Amount, (SL.Amount + SL.Interest)AS Total, SL.FI, SL.Status, SL.DateAcknowledged, B.BankName, F.CompanyName, (I.Name)AS IName, S.Name, count(*) OVER() AS full_count 
 FROM SalaryLoan SL
 Left Join DisbursementMode DM
 On SL.Modde = DM.id
 Left Join Banks B
ON SL.BankID = B.BankNo
 Left Join FIs F
ON SL.FI = F.CompanyNo
Left Join Institutions I
ON SL.Institution = I.InsNo
Left Join Staffs S
On SL.StaffID = S.StaffID WHERE  SL.Status = '1000' And InsNo='" & qval.InsId & "' "
                End If


                If qval.pagination = False Then
                    sSQL = sSQL & " ORDER BY id"
                Else
                    Dim pagenum As Integer
                    pagenum = qval.pagesize * (qval.pagenumber - 1)
                    sSQL = sSQL & " ORDER BY id  LIMIT " & qval.pagesize.ToString & " OFFSET " & pagenum
                End If


                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then

                    Message.Message = "No Records found"
                    Message.Payload = "No Records Found"
                    Return BadRequest(ReturnJSONResults(Message))


                Else
                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Staff  Records Filtered"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)

                End If

            Catch ex As Exception

            End Try


            ' Return jsonconvert
        End If


    End Function

End Class
