﻿Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO

Public Class FIReport
    Inherits ControllerBase

    <HttpPost>
    <Route("/fi/report")>
    Public Function GetFIReport(<FromBody()> ByVal values As FIReportFilter, <FromQuery()> ByVal qval As GetRequestFormat) As ActionResult
        Dim sSQL As String
        Dim Dt As DataTable
        Dim Message As New GeneralResponds

        'Select * From FIs ORDER BY Contact.ContactPK  OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY

        ' Dim pval As String = hdvalues.token



        Dim myToken As String
        myToken = HttpContext.Request.Headers("token").ToString()


        If VerifyJWT(myToken) = False Then
            Message.Message = "Invalid Token/Expired Token/Expired Token"
            Message.Payload = "Invalid Token/Expired Token"
            Return BadRequest(ReturnJSONResults(Message))
        Else

            If values.FINO Is Nothing Or values.FINO.Length = 0 Then
                Message.Message = "No Financial Institution Number Provided"
                Message.Payload = "Financial Institution Number Required"
                Return BadRequest(ReturnJSONResults(Message))
            End If



            Try
                'validate token
                Dim status As String = "3000"
                Dim ttime As String = GetConfigurator(3)
                sSQL = "Select SL.StaffID, SL.Institution, SL.Amount, SL.FI, SL.Status, SL.Modde, SL.AccountNo,
 SL.MobileNo, S.Name, (I.Name)As InsName, F.CompanyName, (DM.Modde)PaymentMode,
 DateApplied::timestamp As ApplyDate,
 SL.DateProcessed::timestamp AS ProcessedDate, count(*) OVER() AS full_count 
 from SalaryLoan SL
 LEFT JOIN Staffs S
ON  SL.StaffID = S.StaffID
 LEFT JOIN Institutions I
ON  SL.Institution = I.InsNo
 LEFT JOIN FIs F
ON  SL.FI = F.CompanyNo
 LEFT JOIN DisbursementMode DM
ON  SL.Modde = DM.id
Where
DateApplied::timestamp  >= '" & values.FromDate & "'::timestamp AND
DateApplied::timestamp <= '" & values.ToDate & "' :: timestamp AND
((SL.Status = '" & values.Status & "' and '" & values.Status & "' <> '0')or (SL.Status = SL.Status and '" & values.Status & "' = '0')) AND
((SL.Institution = '" & values.InstitutionID & "' and '" & values.InstitutionID & "' <> '0')or (SL.Institution = SL.Institution and '" & values.InstitutionID & "' = '0')) AND
((SL.FI = '" & values.FINO & "' and '" & values.FINO & "' <> '0')or (SL.FI = SL.FI and '" & values.FINO & "' = '0') or SL.FI is null) AND
((SL.StaffID = '" & values.StaffID & "' and '" & values.StaffID & "' <> '0')or (SL.StaffID = SL.StaffID and '" & values.StaffID & "' = '0'))
"
                If values.pagination = False Then
                    ' sSQL = "SELECT SL.id, SL.StaffID, SL.Institution, SL.Amount, SL.FI, SL.Status, DM.Modde, (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 60) + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint AS TimeElapsed,count(*) OVER() AS full_count FROM SalaryLoan SL Left Join DisbursementMode DM On SL.Modde = DM.id WHERE SL.FI = '" & values.FINO & "' AND SL.Status = '" & status & "' AND (DATE_PART('day', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 24 + DATE_PART('hour', DateProcessed::timestamp - CURRENT_DATE::timestamp)* 60) + DATE_PART('minute', DateProcessed::timestamp - CURRENT_DATE::timestamp)::bigint <" & ttime
                Else
                    ' sSQL = "Select * From FIs ORDER BY CompanyNo OFFSET " & values.pagesize.ToString & " * (" & values.pagenumber.ToString & " - 1) ROWS FETCH NEXT " & values.pagesize & " ROWS ONLY"

                    Dim pagenum As Integer
                    pagenum = values.pagesize * (values.pagenumber - 1)
                    sSQL = sSQL & "ORDER BY applydate  LIMIT " & values.pagesize.ToString & " OFFSET " & pagenum


                End If





                Dt = DBConnection.GetTableData(sSQL)

                If Dt Is Nothing Or Dt.Rows.Count <= 0 Then


                    Message.Message = "No records Found "
                    Message.Payload = "No Records Found "
                    Return BadRequest(ReturnJSONResults(Message))

                Else

                    Dim glob As New Chilkat.Global
                    Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
                    'Log("Chilkat License Status : " & succes)




                    Message.Payload = JsonForDatatable(Dt)
                    'Message.Payload = Message.Payload.Replace("""[", "[")
                    'Message.Payload = Message.Payload.Replace("]""", "]")
                    Message.Message = "Loan Reports Details"
                    Dim jsonconvert As New JsonResult(Message)
                    ' Return Ok(ReturnJSON(Message.Message, Message.Payload))

                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If
            Catch ex As Exception
                SavetoLog("Get GetFIReport" & ex.Message)
            End Try


            ' Return jsonconvert
        End If


    End Function
End Class
