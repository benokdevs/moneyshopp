﻿Public Class UploadStaffs
    Public Property id As String
    Public Property staffid As String
    Public Property Name As String
    Public Property DOB As String
    Public Property NetSalary As String
    Public Property Bank As String
    Public Property AccountNo As String
    Public Property MobileNo As String
    Public Property DateRegistered As DateTime
    Public Property RegisteredBy As String
    Public Property InsID As String
    Public Property Status As String
    Public Property FirstLogin As Boolean
    Public Property Locked As String
    Public Property Role As String

End Class
