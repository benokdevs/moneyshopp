﻿Public Class Users
    Public Property id As Integer
    Public Property FirstName As String
    Public Property LastName As String
    Public Property UserName As String
    Public Property Password As String
    Public Property UserGuid As String
    Public Property Email As String
    Public Property Role As String
    Public Property EnteredBy As String
    Public Property DateEntered As Date
    Public Property Status As Boolean
    Public Property LastLogin As Date
    Public Property Branch As String
    Public Property Locked As Boolean
    Public Property ExpiringDate As String
    Public Property FirstTimeLogin As Boolean
    Public Property MobileNo As String
    Public Property Code As String
    Public Property CodeTime As String
    Public Property Token As String
    Public Property usertype As String


End Class
