﻿Imports Microsoft.AspNetCore.Mvc
Imports System.Collections.Generic
Imports Newtonsoft
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Hosting
Imports System.Data
Imports Microsoft.AspNetCore.Http.Internal
Imports System.IO
Public Class CallBack
    Inherits ControllerBase

    <HttpPost>
    <Route("/callback")>
    Public Function BankManulaDisbursement(ByVal values As String) As ActionResult
        If Not ModelState.IsValid Then
            Return BadRequest(ModelState)
        Else

            ' check values returned


            Dim tToken As New Token
            Dim Message As New GeneralResponds


            Try

                Dim myToken As String
                myToken = HttpContext.Request.Headers("token").ToString()


                HttpContext.Request.EnableRewind()

                Using reader = New StreamReader(Request.Body)
                    Dim body = reader.ReadToEnd()
                    Request.Body.Seek(0, SeekOrigin.Begin)
                    body = reader.ReadToEnd()
                    values = body
                End Using
                values = values.Trim
                'If VerifyJWT(myToken) = False Then
                '    Message.Message = "Invalid Token/Expired Token"
                '    Message.Payload = "Invalid Token/Expired Token"
                '    Return BadRequest(ReturnJSONResults(Message))
                'Else
                'Dim username As String

                'username = GetJWTUsername(myToken)

                'If username = "" Or username Is Nothing Then
                '    Message.Message = "Unable to Retrieve Username from token"
                '    Message.Payload = "Unable to Retrieve Username from token"
                '    Return BadRequest(ReturnJSONResults(Message))
                'End If


                If values.Length = 0 Then
                    Message.Message = "No Body Content "
                    Message.Payload = "Content Required"
                    Return BadRequest(ReturnJSONResults(Message))
                End If

                'process body json

                ' Go ahead to Update Record

                Message = ProcessCallBack(values, "callback")

                If Message.mError = True Or Message.Message Is Nothing Then

                    'Message.Message = "Unable to Update Records"
                    'Message.Payload = "Internal Error"

                    Return BadRequest(ReturnJSONResults(Message))

                Else
                    Dim rval As String
                    rval = ReturnJSONResults(Message)

                    Return Ok(rval)
                End If


                ' End If
            Catch ex As Exception

            End Try



        End If

    End Function

    Public Function ProcessCallBack(sval As String, username As String) As GeneralResponds
        Dim Message As New GeneralResponds
        Try
            Dim glob As New Chilkat.Global
            Dim succes As Boolean = glob.UnlockBundle("puBuHF.CBX0424_hrF5DeF87i3y")
            Dim json As New Chilkat.JsonObject

            Dim tToken As New Token

            '            {
            '  "responseCode": "01",
            '  "responseMessage": "Payment successful",
            '  "uniwalletTransactionId": "1568722912534",
            '  "networkTransactionId": "649150514368224",
            '  "merchantId": "{{merchantId}}",
            '  "productId": "{{productId}}",
            '  "refNo": "543245654436",
            '  "msisdn": "233209331457",
            '  "amount": "0.01"
            '}

            Dim jsonStr As String = sval
            Dim success As Boolean = json.Load(jsonStr)
            If (success <> True) Then
                SavetoLog("Error From ProcessCallBack : " & json.LastErrorText)
                Message.Message = "Error Unable to Process Request. Contact Adminitrator"
                Message.Payload = "Error Processing request"
                Message.mError = True
                Return Message
            End If


            '' Get the "employees" array.
            'Dim applicants As Chilkat.JsonArray = json.ArrayOf("applicants")
            'If (json.LastMethodSuccess = False) Then
            '    SavetoLog("Error From ProcessBank_Manual : " & "No applicants Found")
            '    Message.Message = "No applicants Found"
            '    Message.Payload = "Error Processing request"
            '    Message.mError = True
            '    Return Message

            'End If

            ' Get the Institution ID

            ' Dim InstID As String = json.StringOf("institution_id")

            ' Iterate over each employee, getting the JSON object at each index.

            Dim ssql As String
            Dim responseCode As String
            Dim responseMessage As String
            'Dim amount As String
            Dim refNo As String
            ' Dim id As Integer

            'Dim MERCHANTID, PRODUCTID, REFNUMBER, PHONENUMBER, NETWORK, NARRATION, APIKEY, URL, HOST As String

            ' Dim ssql As String

            refNo = json.StringOf("refNo")
            responseCode = json.StringOf("responseCode")
            responseMessage = json.StringOf("responseMessage")
            'id = Integer.Parse(json.StringOf("id"))
            Dim status As Boolean
            '--------------------
            If responseCode = "01" Then
                ssql = "update SalaryLoan set Status='2000',TransferCode='" & responseCode & "', TransferMessage='" & responseMessage & "', DateDisbursed=@DateDisbursed WHERE TransID= '" & refNo & "'"
                status = True
            Else
                ssql = "update SalaryLoan set TransferCode='" & responseCode & "', TransferMessage='" & responseMessage & "', DateDisbursed=@DateDisbursed WHERE TransID= '" & refNo & "'"
                status = False
            End If


            Message.mError = Not DBConnection.Callback(ssql, username, refNo, status)
            '----------------------------


            If Message.mError = False Then
                Message.Message = "Processed Successfully"
                Message.Payload = "Successful"
            Else
                Message.Message = "Unable to Complete Request"
                Message.Payload = "Error Processing request"

            End If
            '  Message.mError = False
            Return Message
        Catch ex As Exception
            SavetoLog("Error From ProcessCallBack : " & ex.Message)
            Message.Message = "Unable to Complete Request"
            Message.Payload = "Error Processing request"
            Message.mError = True
            Return Message

        End Try


    End Function
End Class
