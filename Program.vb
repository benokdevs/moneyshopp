Imports System
Imports Microsoft.AspNetCore.Hosting
Imports Microsoft.AspNetCore
Imports Microsoft.Extensions.DependencyInjection
Imports Microsoft.AspNetCore.Builder
Module Program
    Sub Main(args As String())
        Console.WriteLine("Server Started !")
        BuildWebHost(args).Run()
    End Sub
    Function BuildWebHost(args As String()) As IWebHost

        '        Public Static IWebHost BuildWebHost(String[] args)
        '{
        '    Return WebHost.CreateDefaultBuilder(args)
        '        .UseStartup()
        '        .UseUrls("http://localhost:5001")
        '        .Build();
        '}


        Return WebHost.CreateDefaultBuilder(args).UseStartup(Of Startup)().Build()
    End Function
End Module
